/**
 * Build configuration for common (isomorphic) code
 */
export default {
  build: {
    lib: {
      entry: 'src/index.js'
    },
    rollupOptions: {
      external: ["pinia"],
    },
    minify: 'eslint'
  }
}
