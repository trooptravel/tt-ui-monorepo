import gql from 'graphql-tag';
import * as VueApolloComposable from '@vue/apollo-composable';
import * as VueCompositionApi from 'vue';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type ReactiveFunction<TParam> = () => TParam;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Date: any;
  /** GeoJson Coordinates { "type": "Point", "coordinates": [ Float, Float ] }  */
  GeoJsonCoordinates: any;
  JSON: any;
  /** The JSONObject scalar type represents JSON objects as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSONObject: any;
  LngLatRange: any;
  /** Geo Location [Lng, Lat] Point data */
  Point: any;
  SsoCode: any;
  Timestamp: string;
  Upload: any;
};

export type Accommodation = {
  __typename?: 'Accommodation';
  address?: Maybe<Scalars['String']>;
  chain?: Maybe<Scalars['String']>;
  default?: Maybe<Scalars['Boolean']>;
  default_id?: Maybe<Scalars['String']>;
  default_price?: Maybe<Scalars['Float']>;
  destination?: Maybe<Destination>;
  destination_id?: Maybe<Scalars['ID']>;
  distance_from_search?: Maybe<Scalars['String']>;
  green_status?: Maybe<Scalars['Boolean']>;
  id: Scalars['ID'];
  link?: Maybe<Scalars['String']>;
  lnglat?: Maybe<Scalars['Point']>;
  meeting?: Maybe<Meeting>;
  number_of_rooms?: Maybe<Scalars['Int']>;
  organization?: Maybe<Organization>;
  price: Array<AccommodatoinProviderPrice>;
  provider?: Maybe<Scalars['String']>;
  provider_id?: Maybe<Scalars['String']>;
  recommended?: Maybe<Scalars['Boolean']>;
  selected?: Maybe<Scalars['Boolean']>;
  star?: Maybe<Scalars['Float']>;
  thumbnail?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
};

export type AccommodationListInputs = {
  allAccommodation?: InputMaybe<Scalars['Boolean']>;
  currentPage: Scalars['Int'];
  destinationId: Scalars['String'];
  filterBoolean?: InputMaybe<Array<FilterBoolean>>;
  filterRange?: InputMaybe<Array<FilterRange>>;
  perPage: Scalars['Int'];
  sortBy?: InputMaybe<SortListBy>;
};

export type AccommodationPaginationResponse = {
  __typename?: 'AccommodationPaginationResponse';
  data?: Maybe<Array<Accommodation>>;
  page?: Maybe<PageFiltering>;
};

export type AccommodationResult = {
  __typename?: 'AccommodationResult';
  cost_avg: Scalars['Float'];
  destination_id?: Maybe<Scalars['String']>;
  meeting_id?: Maybe<Scalars['String']>;
  preferred_count: Scalars['Int'];
  price_avg: Scalars['Float'];
  price_max: Scalars['Float'];
  price_min: Scalars['Float'];
  total_count: Scalars['Int'];
  traveller_count: Scalars['Int'];
  traveller_stay: Scalars['Int'];
};

export type AccommodatoinProviderPrice = {
  __typename?: 'AccommodatoinProviderPrice';
  default?: Maybe<Scalars['Boolean']>;
  price?: Maybe<Scalars['Float']>;
  property_id?: Maybe<Scalars['String']>;
  provider_id: Scalars['String'];
};

/** Type to display the analytics on active users */
export type ActiveUsersResponse = {
  __typename?: 'ActiveUsersResponse';
  activeUsers?: Maybe<Scalars['Int']>;
  percentageActive?: Maybe<Scalars['Float']>;
  registeredUsers?: Maybe<Scalars['Int']>;
};

export type AdoptTemplateInput = {
  meeting_id?: InputMaybe<Scalars['ID']>;
  template_id?: InputMaybe<Scalars['ID']>;
};

/** Type to display the analytics on analysed vs created meetings */
export type AnalysedMeetingsResponse = {
  __typename?: 'AnalysedMeetingsResponse';
  analysedMeetings?: Maybe<Scalars['Int']>;
  createdMeetings?: Maybe<Scalars['Int']>;
  percentageAnalysed?: Maybe<Scalars['Float']>;
};

export type AnalysisProgress = {
  __typename?: 'AnalysisProgress';
  analysis_status: AnalysisStatus;
  destinations: Array<AnalysisProgressDestination>;
  meeting_id: Scalars['String'];
};

export type AnalysisProgressDestination = {
  __typename?: 'AnalysisProgressDestination';
  accommodation_status: AnalysisProgressDestinationsAccommodation;
  analysis_status: AnalysisStatus;
  destination_id: Scalars['String'];
  flights_status: AnalysisProgressDestinationsFlights;
};

export type AnalysisProgressDestinationsAccommodation = {
  __typename?: 'AnalysisProgressDestinationsAccommodation';
  analysis_status: AnalysisStatus;
  data: Array<AnalysisProgressStatusItem>;
};

export type AnalysisProgressDestinationsFlights = {
  __typename?: 'AnalysisProgressDestinationsFlights';
  analysis_status: AnalysisStatus;
  data: Array<AnalysisProgressStatusItem>;
};

export type AnalysisProgressResponse = {
  __typename?: 'AnalysisProgressResponse';
  args: Scalars['String'];
  code: Scalars['Int'];
  data: AnalysisProgress;
  message: Scalars['String'];
  success: Scalars['Boolean'];
};

export type AnalysisProgressStatusItem = {
  __typename?: 'AnalysisProgressStatusItem';
  analysis_status: AnalysisStatus;
  tasks?: Maybe<Array<AnalysisProgressStatusItemTask>>;
  type: Scalars['String'];
};

export type AnalysisProgressStatusItemTask = {
  __typename?: 'AnalysisProgressStatusItemTask';
  status: Scalars['String'];
  task_id: Scalars['String'];
};

export type AnalysisProgressStatusItemTaskInput = {
  status: Scalars['String'];
  task_id: Scalars['String'];
};

export type AnalysisProgressUpdateInput = {
  analysis_status: Scalars['String'];
  destination_id: Scalars['String'];
  meeting_id: Scalars['String'];
  required_service: Scalars['String'];
  task?: InputMaybe<AnalysisProgressStatusItemTaskInput>;
  type: Scalars['String'];
};

/** ENUM continaing possible values for Destination Analysis Status */
export enum AnalysisStatus {
  AnalysisError = 'ANALYSIS_ERROR',
  Analyzed = 'ANALYZED',
  Completed = 'COMPLETED',
  NeedReanalysis = 'NEED_REANALYSIS',
  NotAnalyzed = 'NOT_ANALYZED',
  Processing = 'PROCESSING'
}

export type AnalyzeDestination = {
  __typename?: 'AnalyzeDestination';
  args?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['Int']>;
  message?: Maybe<Scalars['String']>;
  success?: Maybe<Scalars['Boolean']>;
  workflows?: Maybe<Array<Maybe<Scalars['String']>>>;
};

/** Response type for the top analyzed destinations */
export type AnalyzedDestination = {
  __typename?: 'AnalyzedDestination';
  destination?: Maybe<Destination>;
  numberOfSearches?: Maybe<Scalars['Int']>;
};

export type Assets = {
  __typename?: 'Assets';
  id: Scalars['ID'];
  logos?: Maybe<Array<Maybe<Logo>>>;
  organization?: Maybe<Organization>;
};

/** An attendee in Trooptravel */
export type Attendee = {
  __typename?: 'Attendee';
  email?: Maybe<Scalars['String']>;
  fully_vaccinated_covid?: Maybe<Scalars['Boolean']>;
  id?: Maybe<Scalars['ID']>;
  meeting?: Maybe<Meeting>;
  nationality?: Maybe<Scalars['String']>;
  organization?: Maybe<Organization>;
  origin?: Maybe<Origin>;
  status?: Maybe<AttendeeStatus>;
  type?: Maybe<AttendeeType>;
  updated?: Maybe<Scalars['Date']>;
  user?: Maybe<User>;
};

/** Add/Remove attendees from a Meeting */
export type AttendeeInput = {
  email?: InputMaybe<Scalars['String']>;
  fully_vaccinated_covid?: InputMaybe<Scalars['Boolean']>;
  id?: InputMaybe<Scalars['String']>;
  meeting_id: Scalars['String'];
  nationality?: InputMaybe<Scalars['String']>;
  organization_id?: InputMaybe<Scalars['String']>;
  origin_id?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<AttendeeStatus>;
  type?: InputMaybe<AttendeeType>;
  user_id?: InputMaybe<Scalars['String']>;
};

/** Schema for Attendee Response */
export type AttendeeResponse = {
  __typename?: 'AttendeeResponse';
  code?: Maybe<Scalars['Int']>;
  data?: Maybe<Attendee>;
  message?: Maybe<Scalars['String']>;
  success?: Maybe<Scalars['Boolean']>;
};

/** ENUM continaing possible values for Attendee Status */
export enum AttendeeStatus {
  Archived = 'ARCHIVED',
  Confirmed = 'CONFIRMED',
  Declined = 'DECLINED',
  Pending = 'PENDING'
}

/** ENUM containing possible values for Attendee.type field */
export enum AttendeeType {
  Guest = 'GUEST',
  Internal = 'INTERNAL',
  NotSet = 'NOT_SET'
}

export type AuthForm = {
  designation?: InputMaybe<Scalars['String']>;
  email: Scalars['String'];
  first_name?: InputMaybe<Scalars['String']>;
  job_role?: InputMaybe<JobRole>;
  last_name?: InputMaybe<Scalars['String']>;
  password: Scalars['String'];
};

export type AuthProviderForm = {
  created?: InputMaybe<Scalars['String']>;
  designation?: InputMaybe<Scalars['String']>;
  email: Scalars['String'];
  first_name?: InputMaybe<Scalars['String']>;
  id: Scalars['String'];
  last_name?: InputMaybe<Scalars['String']>;
  organization_id?: InputMaybe<Scalars['String']>;
  provider_payload: Scalars['JSON'];
};

/** ENUM continaing possible values for Auth Providers */
export enum AuthProviders {
  MsTeams = 'MS_TEAMS'
}

/** User Sign Up Response */
export type AuthResponse = {
  __typename?: 'AuthResponse';
  accessToken?: Maybe<Scalars['String']>;
  data?: Maybe<User>;
  refreshToken?: Maybe<Scalars['String']>;
};

/** (Create/Update) User Loggin in with SAML Provider */
export type AuthSamlForm = {
  created?: InputMaybe<Scalars['String']>;
  designation?: InputMaybe<Scalars['String']>;
  email: Scalars['String'];
  fb_payload: Scalars['JSON'];
  first_name?: InputMaybe<Scalars['String']>;
  id: Scalars['String'];
  last_name?: InputMaybe<Scalars['String']>;
  organization_id?: InputMaybe<Scalars['String']>;
  provider?: InputMaybe<Array<Scalars['String']>>;
};

/** Response type to display the average and totals for origins, destinations and attendees */
export type BasicMeetingStatisticsReponse = {
  __typename?: 'BasicMeetingStatisticsReponse';
  averageAttendees?: Maybe<Scalars['Float']>;
  averageDestinations?: Maybe<Scalars['Float']>;
  averageOrigins?: Maybe<Scalars['Float']>;
  totalAttendees?: Maybe<Scalars['Int']>;
  totalDestinations?: Maybe<Scalars['Int']>;
  totalMeetings?: Maybe<Scalars['Int']>;
  totalOrigins?: Maybe<Scalars['Int']>;
  totalTravelItineraries?: Maybe<Scalars['Int']>;
};

export enum CapLocaleType {
  City = 'CITY',
  Country = 'COUNTRY'
}

export enum CapType {
  Default = 'DEFAULT',
  External = 'EXTERNAL'
}

export type Carrier = {
  __typename?: 'Carrier';
  alliance_code?: Maybe<Scalars['String']>;
  alliance_name?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  iata?: Maybe<Scalars['String']>;
  icao?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  name: Scalars['String'];
  url?: Maybe<Scalars['String']>;
};

export type CarrierAlliance = {
  __typename?: 'CarrierAlliance';
  carriers?: Maybe<Array<Maybe<Carrier>>>;
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
};

/** Response sent back to server looking to retreive carrier data */
export type CarrierResponse = {
  __typename?: 'CarrierResponse';
  /** Name of API requested */
  api: Scalars['String'];
  /** Error from requested resource */
  error?: Maybe<TravelResponseResultError>;
  /** Result of requested resource */
  result?: Maybe<Array<Maybe<CarrierAlliance>>>;
  /** Version of API requested */
  version: Scalars['String'];
};

export type Country = {
  __typename?: 'Country';
  id: Scalars['String'];
  name: Scalars['String'];
  risk?: Maybe<CountryRisk>;
};

export type CountryRisk = {
  __typename?: 'CountryRisk';
  risk_details?: Maybe<Array<Maybe<CountryRiskDetails>>>;
  risk_score?: Maybe<Scalars['Float']>;
  risk_source?: Maybe<Scalars['String']>;
  risk_source_link?: Maybe<Scalars['String']>;
  risk_status?: Maybe<Scalars['String']>;
};

export type CountryRiskDetails = {
  __typename?: 'CountryRiskDetails';
  coverage?: Maybe<Scalars['String']>;
  score?: Maybe<Scalars['Float']>;
  source?: Maybe<Scalars['String']>;
  source_iso_alpha2?: Maybe<Scalars['String']>;
  source_link?: Maybe<Scalars['String']>;
};

export type CovidCoreSpecs = {
  __typename?: 'CovidCoreSpecs';
  code: Scalars['String'];
  requireQuarantine: Array<Maybe<RequireQuarantine>>;
  requireRestriction: Array<Maybe<RequireRestriction>>;
  requireTest: Array<Maybe<RequireTest>>;
};

/** Covid policies */
export type CovidPolicies = {
  __typename?: 'CovidPolicies';
  inbound: CovidPoliciesByDirection;
  outbound: CovidPoliciesByDirection;
};

export type CovidPoliciesByDirection = {
  __typename?: 'CovidPoliciesByDirection';
  procedures?: Maybe<Array<Procedure>>;
  restrictions?: Maybe<Array<Restriction>>;
};

export type CovidRestrictionEntry = {
  __typename?: 'CovidRestrictionEntry';
  category?: Maybe<Scalars['String']>;
  destination_country_code?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  origin_country_code?: Maybe<Scalars['String']>;
  requireRestriction?: Maybe<Scalars['Int']>;
  subcategory?: Maybe<Scalars['String']>;
  tags?: Maybe<Array<Maybe<Scalars['String']>>>;
  title?: Maybe<Scalars['String']>;
};

/** Covid Restrictions for a country */
export type CovidRestrictionItems = {
  __typename?: 'CovidRestrictionItems';
  country?: Maybe<Scalars['String']>;
  requireQuarantine?: Maybe<Array<Maybe<CovidRestrictionQuarantine>>>;
  requireRestriction?: Maybe<Array<Maybe<CovidRestrictionEntry>>>;
  requireTest?: Maybe<Array<Maybe<CovidRestrictionQuarantine>>>;
};

export type CovidRestrictionQuarantine = {
  __typename?: 'CovidRestrictionQuarantine';
  category?: Maybe<Scalars['String']>;
  destination_country_code?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  origin_country_code?: Maybe<Scalars['String']>;
  quarantine_days?: Maybe<Scalars['Int']>;
  requireQuarantine?: Maybe<Scalars['Int']>;
  requireTest?: Maybe<Scalars['Int']>;
  subcategory?: Maybe<Scalars['String']>;
  tags?: Maybe<Array<Maybe<Scalars['String']>>>;
  title?: Maybe<Scalars['String']>;
};

export type CovidSource = {
  __typename?: 'CovidSource';
  sourceType: Scalars['String'];
  title: Scalars['String'];
  url: Scalars['String'];
};

/** A Destination for Meeting */
export type Destination = {
  __typename?: 'Destination';
  accommodation?: Maybe<AccommodationResult>;
  address?: Maybe<Scalars['String']>;
  airport_id?: Maybe<Scalars['String']>;
  airport_lnglat?: Maybe<Scalars['Point']>;
  airport_name?: Maybe<Scalars['String']>;
  analysis_date?: Maybe<Scalars['Date']>;
  analysis_status?: Maybe<AnalysisStatus>;
  analysis_user?: Maybe<User>;
  baseline?: Maybe<Scalars['Boolean']>;
  city_id: Scalars['String'];
  city_lnglat?: Maybe<Scalars['Point']>;
  city_name?: Maybe<Scalars['String']>;
  city_sygic_image_id?: Maybe<Scalars['String']>;
  country_id: Scalars['String'];
  country_lnglat?: Maybe<Scalars['Point']>;
  country_name?: Maybe<Scalars['String']>;
  covid19?: Maybe<MeetingCovidInfectionRate>;
  id: Scalars['ID'];
  lnglat?: Maybe<Scalars['Point']>;
  meetingSpaces?: Maybe<MeetingSpaceList>;
  meeting_id: Scalars['String'];
  meeting_policy_override?: Maybe<MeetingPolicyOverride>;
  offices?: Maybe<OfficeList>;
  organization_id?: Maybe<Scalars['String']>;
  places_id?: Maybe<Scalars['String']>;
  risk?: Maybe<PlaceDetailsTravelAdvisoryRiskResponse>;
  status?: Maybe<DestinationStatus>;
  timeZone?: Maybe<PlaceDetailsTimeZoneResponse>;
  travel?: Maybe<MeetingTravelDetailsOrigin>;
  weather?: Maybe<PlaceDetailsWeatherResponse>;
};

/** Add/Update destination Analysis Status */
export type DestinationAnalysisInput = {
  analysis_status?: InputMaybe<AnalysisStatus>;
  analysis_user_id: Scalars['String'];
  destination_id: Scalars['String'];
  meeting_id: Scalars['String'];
};

export type DestinationDetails = {
  __typename?: 'DestinationDetails';
  count?: Maybe<Scalars['Int']>;
  destination?: Maybe<Destination>;
};

/** Destination type input */
export type DestinationIdInput = {
  id: Scalars['String'];
};

/** Add/Remove destination from Meeting */
export type DestinationInput = {
  address?: InputMaybe<Scalars['String']>;
  airport_id?: InputMaybe<Scalars['String']>;
  airport_lnglat?: InputMaybe<Scalars['Point']>;
  airport_name?: InputMaybe<Scalars['String']>;
  analysis_status?: InputMaybe<AnalysisStatus>;
  baseline?: InputMaybe<Scalars['Boolean']>;
  city_id?: InputMaybe<Scalars['String']>;
  city_lnglat?: InputMaybe<Scalars['Point']>;
  city_name?: InputMaybe<Scalars['String']>;
  city_sygic_image_id?: InputMaybe<Scalars['String']>;
  country_id?: InputMaybe<Scalars['String']>;
  country_lnglat?: InputMaybe<Scalars['Point']>;
  country_name?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  lnglat?: InputMaybe<Scalars['Point']>;
  meeting_id: Scalars['String'];
  organization_id: Scalars['String'];
  places_id?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<DestinationStatus>;
};

/** Destination Item Response */
export type DestinationItemResponse = {
  __typename?: 'DestinationItemResponse';
  code?: Maybe<Scalars['Int']>;
  data?: Maybe<Destination>;
  message?: Maybe<Scalars['String']>;
  success?: Maybe<Scalars['Boolean']>;
};

/** Destination type input */
export type DestinationMeetingIdInput = {
  meeting_id: Scalars['String'];
};

/** Add/Update destination Analysis Status */
export type DestinationMeetingPolicyOverrideInput = {
  destination_id: Scalars['String'];
  meeting_policy_override: MeetingPolicyOverrideInput;
};

/** Schema for Meeting Destination Response */
export type DestinationResponse = {
  __typename?: 'DestinationResponse';
  code?: Maybe<Scalars['Int']>;
  data?: Maybe<DestinationDetails>;
  message?: Maybe<Scalars['String']>;
  success?: Maybe<Scalars['Boolean']>;
};

/** ENUM continaing possible values for Destination Status */
export enum DestinationStatus {
  Active = 'ACTIVE',
  Archived = 'ARCHIVED',
  Pending = 'PENDING'
}

/** Data on directions and route overview for requested waypoints */
export type DirectionData = {
  __typename?: 'DirectionData';
  /** Estimated cost of trip */
  cost?: Maybe<Scalars['Float']>;
  /** Combined distance of steps in Meters */
  distance: Scalars['Float'];
  /** Estimated total duration of entire trip in Minutes */
  duration: Scalars['Int'];
  /** Total carbon emmissions released during trip */
  emissions: Scalars['Float'];
  /** An encoded representation of the combined steps into a single polyline */
  encodedPolyline: Scalars['String'];
  /** An array of route steps, each detailing a segemnt between two waypoints */
  route: Array<Maybe<RouteSegment>>;
  /** The travel mode assigned to this request, explicitly or otherwise */
  transitMode: Scalars['ID'];
  /** These are the waypoints sent in the request  */
  waypoints: Array<Maybe<Waypoint>>;
};

/** Request sent to server to retreive directions */
export type DirectionRequest = {
  /** Coords at ending point for route */
  destination: LatLngInput;
  /** Coords at starting point for route */
  origin: LatLngInput;
  /** Preferred mode of transport for request, null if no preference */
  preferredTransitMode?: InputMaybe<Scalars['String']>;
  /** Points through which the route must run */
  waypoints: Array<InputMaybe<WaypointInput>>;
};

/** Response sent back to server looking to retreive directions */
export type DirectionResponse = {
  __typename?: 'DirectionResponse';
  /** Similar to HTTP status code, represents the status of the mutation */
  code: Scalars['Int'];
  /** Directional data for requested coordinates */
  data?: Maybe<DirectionData>;
  /** Human-readable message for the UI */
  message: Scalars['String'];
  /** Indicates whether the mutation was successful */
  success: Scalars['Boolean'];
};

export type Features = {
  __typename?: 'Features';
  geometry?: Maybe<GeoJsonFeatureGeometry>;
  properties: GeoJsonFeatureProperty;
  type: Scalars['String'];
};

export type FilterBoolean = {
  type: FiltersBoolean;
  value: Scalars['Boolean'];
};

export type FilterRange = {
  max: Scalars['Float'];
  min: Scalars['Float'];
  type: FiltersRange;
};

export enum FiltersBoolean {
  GreenStatus = 'green_status',
  Recommended = 'recommended'
}

export enum FiltersRange {
  DefaultPrice = 'default_price',
  DistanceFromSearch = 'distance_from_search',
  NumberOfRooms = 'number_of_rooms',
  Star = 'star'
}

/** ENUM containing possible values for the Flight Cabin Class */
export enum FlightCabinClass {
  Business = 'BUSINESS',
  Economy = 'ECONOMY',
  First = 'FIRST',
  PremiumEconomy = 'PREMIUM_ECONOMY'
}

export type FlightData = {
  __typename?: 'FlightData';
  adult_count?: Maybe<Scalars['Int']>;
  business_count?: Maybe<Scalars['Float']>;
  cabin_class?: Maybe<Scalars['String']>;
  client_id?: Maybe<Scalars['String']>;
  cost?: Maybe<Scalars['Float']>;
  duration_cost?: Maybe<Scalars['Float']>;
  economy_count?: Maybe<Scalars['Int']>;
  feasible_status?: Maybe<Scalars['String']>;
  first_count?: Maybe<Scalars['Int']>;
  in_policy?: Maybe<Scalars['Int']>;
  inbound_arrival?: Maybe<Scalars['String']>;
  inbound_carrier_country?: Maybe<Scalars['String']>;
  inbound_carrier_img?: Maybe<Scalars['String']>;
  inbound_carrier_name?: Maybe<Scalars['String']>;
  inbound_departure?: Maybe<Scalars['String']>;
  inbound_destination_airport_code?: Maybe<Scalars['String']>;
  inbound_destination_station?: Maybe<Scalars['String']>;
  inbound_duration?: Maybe<Scalars['Int']>;
  inbound_emissions?: Maybe<Scalars['Int']>;
  inbound_flight_duration?: Maybe<Scalars['Int']>;
  inbound_in_policy?: Maybe<Scalars['Int']>;
  inbound_in_policy_details?: Maybe<Scalars['String']>;
  inbound_leg_id?: Maybe<Scalars['String']>;
  inbound_legs?: Maybe<Scalars['Int']>;
  inbound_max_stop_risk?: Maybe<Scalars['Float']>;
  inbound_origin_airport_code?: Maybe<Scalars['String']>;
  inbound_origin_station?: Maybe<Scalars['String']>;
  inbound_stops?: Maybe<Scalars['Int']>;
  is_feasible?: Maybe<Scalars['Int']>;
  itinerary_id?: Maybe<Scalars['Int']>;
  max_stop_risk?: Maybe<Scalars['Float']>;
  meeting_id?: Maybe<Scalars['String']>;
  outbound_arrival?: Maybe<Scalars['String']>;
  outbound_carrier_country?: Maybe<Scalars['String']>;
  outbound_carrier_img?: Maybe<Scalars['String']>;
  outbound_carrier_name?: Maybe<Scalars['String']>;
  outbound_departure?: Maybe<Scalars['String']>;
  outbound_destination_airport_code?: Maybe<Scalars['String']>;
  outbound_destination_station?: Maybe<Scalars['String']>;
  outbound_duration?: Maybe<Scalars['Float']>;
  outbound_emissions?: Maybe<Scalars['Int']>;
  outbound_flight_duration?: Maybe<Scalars['Float']>;
  outbound_in_policy?: Maybe<Scalars['Int']>;
  outbound_in_policy_details?: Maybe<Scalars['String']>;
  outbound_leg_id?: Maybe<Scalars['String']>;
  outbound_legs?: Maybe<Scalars['Int']>;
  outbound_max_stop_risk?: Maybe<Scalars['Float']>;
  outbound_origin_airport_city_name?: Maybe<Scalars['String']>;
  outbound_origin_airport_code?: Maybe<Scalars['String']>;
  outbound_origin_airport_name?: Maybe<Scalars['String']>;
  outbound_origin_station?: Maybe<Scalars['String']>;
  outbound_stops?: Maybe<Scalars['Int']>;
  policy_count?: Maybe<Scalars['Int']>;
  policy_status?: Maybe<Scalars['String']>;
  premium_count?: Maybe<Scalars['Int']>;
  query_id?: Maybe<Scalars['Int']>;
  rank_cost?: Maybe<Scalars['Float']>;
  service?: Maybe<Scalars['String']>;
  sub_service?: Maybe<Scalars['String']>;
  supplier?: Maybe<Scalars['String']>;
  total_duration?: Maybe<Scalars['Float']>;
  total_emissions?: Maybe<Scalars['Float']>;
  total_legs?: Maybe<Scalars['Int']>;
  total_stops?: Maybe<Scalars['Int']>;
  troop_destination_id?: Maybe<Scalars['String']>;
  troop_origin_id?: Maybe<Scalars['String']>;
};

export type FlightDataDetailsResponse = {
  __typename?: 'FlightDataDetailsResponse';
  /** Name of API requested */
  api: Scalars['String'];
  /** Error from requested resource */
  error?: Maybe<Scalars['JSON']>;
  /** Result of requested resource */
  result?: Maybe<FlightDataDetailsResponseResult>;
  /** Version of API requested */
  version: Scalars['String'];
};

export type FlightDataDetailsResponseResult = {
  __typename?: 'FlightDataDetailsResponseResult';
  inbound?: Maybe<Array<Maybe<FlightTravelDataDetails>>>;
  outbound?: Maybe<Array<Maybe<FlightTravelDataDetails>>>;
};

export type FlightDataFilters = {
  cabinClasses?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  emissions?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  inboundAirports?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  inboundArrivalTime?: InputMaybe<Scalars['String']>;
  inboundDepartureTime?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  includeNonFeasible?: InputMaybe<Scalars['Int']>;
  includeOutOfPolicy?: InputMaybe<Scalars['Int']>;
  maxDuration?: InputMaybe<Scalars['Float']>;
  maxPrice?: InputMaybe<Scalars['Float']>;
  maxStops?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  outboundAirports?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  outboundArrivalTime?: InputMaybe<Scalars['String']>;
  outboundDepartureTime?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
};

export type FlightDataResponse = {
  __typename?: 'FlightDataResponse';
  data?: Maybe<Array<Maybe<FlightData>>>;
  filterData?: Maybe<FlightDataResponseFilterData>;
  page?: Maybe<FlightPagination>;
};

export type FlightDataResponseFilterData = {
  __typename?: 'FlightDataResponseFilterData';
  cabinClasses?: Maybe<Array<Maybe<Scalars['String']>>>;
  duration?: Maybe<FlightFilterDataHistogram>;
  emissions?: Maybe<FlightFilterDataHistogram>;
  price?: Maybe<FlightFilterDataHistogram>;
  stops?: Maybe<Array<Maybe<FlightFilterDataStop>>>;
};

export type FlightFilterDataHistogram = {
  __typename?: 'FlightFilterDataHistogram';
  max?: Maybe<Scalars['Float']>;
  min?: Maybe<Scalars['Float']>;
  range?: Maybe<Array<Maybe<Scalars['Int']>>>;
};

export type FlightFilterDataStop = {
  __typename?: 'FlightFilterDataStop';
  averagePrice?: Maybe<Scalars['Float']>;
  value?: Maybe<Scalars['Int']>;
};

export type FlightPagination = {
  __typename?: 'FlightPagination';
  currentPage?: Maybe<Scalars['Float']>;
  from?: Maybe<Scalars['Float']>;
  lastPage?: Maybe<Scalars['Float']>;
  offset?: Maybe<Scalars['Int']>;
  perPage?: Maybe<Scalars['Float']>;
  to?: Maybe<Scalars['Float']>;
  total?: Maybe<Scalars['Float']>;
};

export type FlightTravelDataDetails = {
  __typename?: 'FlightTravelDataDetails';
  arrival?: Maybe<Scalars['String']>;
  carrier?: Maybe<Scalars['Int']>;
  carrier_code?: Maybe<Scalars['String']>;
  carrier_img?: Maybe<Scalars['String']>;
  carrier_name?: Maybe<Scalars['String']>;
  departure?: Maybe<Scalars['String']>;
  destinationAirport?: Maybe<Scalars['String']>;
  destinationAirportName?: Maybe<Scalars['String']>;
  destinationCityCode?: Maybe<Scalars['String']>;
  destinationCityName?: Maybe<Scalars['String']>;
  destinationCountryCode?: Maybe<Scalars['String']>;
  destinationCountryName?: Maybe<Scalars['String']>;
  destination_code?: Maybe<Scalars['String']>;
  directionality?: Maybe<Scalars['String']>;
  distance?: Maybe<Scalars['Float']>;
  duration?: Maybe<Scalars['Float']>;
  emissions?: Maybe<Scalars['Float']>;
  flight_number?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  journey_mode?: Maybe<Scalars['String']>;
  leg_id?: Maybe<Scalars['String']>;
  operating_carrier?: Maybe<Scalars['Int']>;
  operating_carrier_code?: Maybe<Scalars['String']>;
  originAirport?: Maybe<Scalars['String']>;
  originAirportName?: Maybe<Scalars['String']>;
  originCountryCode?: Maybe<Scalars['String']>;
  originCountryName?: Maybe<Scalars['String']>;
  origin_code?: Maybe<Scalars['String']>;
  query_id?: Maybe<Scalars['Int']>;
  segment_id?: Maybe<Scalars['String']>;
  segment_json?: Maybe<Scalars['String']>;
  stop_duration?: Maybe<Scalars['Float']>;
  updated?: Maybe<Scalars['String']>;
};

/** GeoJson Types for Countrie in maps */
export type GeoJson = {
  __typename?: 'GeoJson';
  features: Array<Maybe<Features>>;
  type: Scalars['String'];
};

export type GeoJsonFeatureGeometry = {
  __typename?: 'GeoJsonFeatureGeometry';
  coordinates?: Maybe<Array<Maybe<Scalars['LngLatRange']>>>;
  type: Scalars['String'];
};

export type GeoJsonFeatureProperty = {
  __typename?: 'GeoJsonFeatureProperty';
  alpha2_code: Scalars['String'];
  alpha3_code: Scalars['String'];
  code: Scalars['String'];
  flag: Scalars['String'];
  lat?: Maybe<Scalars['Float']>;
  lng?: Maybe<Scalars['Float']>;
  name: Scalars['String'];
  region?: Maybe<Scalars['String']>;
  subregion?: Maybe<Scalars['String']>;
  ta_risk?: Maybe<Scalars['Float']>;
};

/** Response type for the number of hours saved */
export type HoursSavedResponse = {
  __typename?: 'HoursSavedResponse';
  hoursPerAnalysis?: Maybe<Scalars['Float']>;
  hoursPerMeeting?: Maybe<Scalars['Float']>;
};

/** ENUM continaing possible values for Job Role */
export enum JobRole {
  Ceo = 'CEO',
  Cfo = 'CFO',
  Cto = 'CTO',
  Sales = 'SALES'
}

/** For storing Latitude and Longitude values */
export type LatLng = {
  __typename?: 'LatLng';
  /** Float number representing Latitude */
  lat: Scalars['Float'];
  /** Float number representing Longitude */
  lng: Scalars['Float'];
};

/** For storing Latitude and Longitude values */
export type LatLngInput = {
  /** Float number representing Latitude */
  lat: Scalars['Float'];
  /** Float number representing Longitude */
  lng: Scalars['Float'];
};

export type License = {
  __typename?: 'License';
  created?: Maybe<Scalars['Date']>;
  id: Scalars['ID'];
  name: LicenseName;
  status?: Maybe<LicenseStatus>;
  updated?: Maybe<Scalars['Date']>;
};

export type LicenseInput = {
  created?: InputMaybe<Scalars['Date']>;
  id?: InputMaybe<Scalars['String']>;
  name: LicenseName;
  status: LicenseStatus;
};

/** ENUM continaing possible values for Licenses Names */
export enum LicenseName {
  Business = 'BUSINESS',
  Enterprise = 'ENTERPRISE',
  Free = 'FREE'
}

/** ENUM continaing possible values for License Status */
export enum LicenseStatus {
  Active = 'ACTIVE',
  Archived = 'ARCHIVED',
  Pending = 'PENDING'
}

export enum LocationSearchSuggestionType {
  Airports = 'airports',
  Cities = 'cities',
  MeetingTemplates = 'meeting_templates',
  Offices = 'offices'
}

/** Request sent to server to retrieve gis data */
export type LocationsSearchRequest = {
  /** place search string */
  search: Scalars['String'];
};

export type LocationsSearchResponse = {
  __typename?: 'LocationsSearchResponse';
  airports?: Maybe<LocationsSearchResponseAirport>;
  cities?: Maybe<LocationsSearchResponseCity>;
  offices?: Maybe<LocationsSearchResponseOffice>;
};

export type LocationsSearchResponseAirport = {
  __typename?: 'LocationsSearchResponseAirport';
  data?: Maybe<Array<Maybe<LocationsSearchResponseItem>>>;
};

/** Response returning search suggestions for city and its airports */
export type LocationsSearchResponseCity = {
  __typename?: 'LocationsSearchResponseCity';
  data?: Maybe<Array<Maybe<LocationsSearchResponseItem>>>;
};

export type LocationsSearchResponseIcon = {
  __typename?: 'LocationsSearchResponseIcon';
  name: Scalars['String'];
  type: Scalars['String'];
};

export type LocationsSearchResponseItem = {
  __typename?: 'LocationsSearchResponseItem';
  address?: Maybe<Scalars['String']>;
  airport_id: Scalars['String'];
  airport_name: Scalars['String'];
  city_iata: Scalars['String'];
  city_id: Scalars['String'];
  city_lnglat: Scalars['LngLatRange'];
  city_location: Scalars['GeoJsonCoordinates'];
  city_name: Scalars['String'];
  city_sygic_image_id?: Maybe<Scalars['String']>;
  country_id: Scalars['String'];
  country_name: Scalars['String'];
  distance: Scalars['Float'];
  icon: LocationsSearchResponseIcon;
  id: Scalars['String'];
  location: Scalars['GeoJsonCoordinates'];
  sub_title: Scalars['String'];
  title: Scalars['String'];
  type: LocationSearchSuggestionType;
};

export type LocationsSearchResponseMeeting = {
  __typename?: 'LocationsSearchResponseMeeting';
  data?: Maybe<Array<Maybe<LocationsSearchResponseItem>>>;
};

export type LocationsSearchResponseOffice = {
  __typename?: 'LocationsSearchResponseOffice';
  data?: Maybe<Array<Maybe<LocationsSearchResponseItem>>>;
};

export type Logo = {
  __typename?: 'Logo';
  filename?: Maybe<Scalars['String']>;
  mimetype?: Maybe<Scalars['String']>;
  type?: Maybe<OrganizationLogo>;
  url?: Maybe<Scalars['String']>;
};

/** Logo Response */
export type LogoResponse = {
  __typename?: 'LogoResponse';
  code?: Maybe<Scalars['Int']>;
  data: Array<Maybe<Logo>>;
  message?: Maybe<Scalars['String']>;
  success?: Maybe<Scalars['Boolean']>;
};

export type LogoUpload = {
  file: Scalars['Upload'];
  type: OrganizationLogo;
};

/** A meeting in Trooptravel */
export type Meeting = {
  __typename?: 'Meeting';
  analysis_date?: Maybe<Scalars['Date']>;
  analysis_user?: Maybe<User>;
  attendees?: Maybe<Array<Attendee>>;
  created?: Maybe<Scalars['Timestamp']>;
  description?: Maybe<Scalars['String']>;
  destinations?: Maybe<Array<Maybe<Destination>>>;
  end_date?: Maybe<Scalars['Date']>;
  end_time?: Maybe<Scalars['Timestamp']>;
  id?: Maybe<Scalars['ID']>;
  isTemplate?: Maybe<Scalars['Boolean']>;
  meetingPolicy?: Maybe<MeetingPolicy>;
  meeting_policy_id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  offices?: Maybe<OfficeList>;
  organization_id?: Maybe<Scalars['String']>;
  origins?: Maybe<Array<Maybe<Origin>>>;
  owner?: Maybe<User>;
  required_services?: Maybe<Array<Maybe<MeetingServices>>>;
  start_date?: Maybe<Scalars['Date']>;
  start_time?: Maybe<Scalars['Timestamp']>;
  status?: Maybe<MeetingStatus>;
  tags?: Maybe<Array<MeetingTag>>;
  type?: Maybe<MeetingType>;
  updated?: Maybe<Scalars['Timestamp']>;
  updated_by?: Maybe<User>;
};

/** An analysis result for a meeting */
export type MeetingAnalysis = {
  __typename?: 'MeetingAnalysis';
  analysis_date?: Maybe<Scalars['Date']>;
  destinations?: Maybe<Array<Destination>>;
  meeting: Meeting;
  number_of_travellers: Scalars['Int'];
  origins?: Maybe<Array<Origin>>;
  user: User;
};

/** An analysis result for a meeting to a specific destination */
export type MeetingAnalysisDetail = {
  __typename?: 'MeetingAnalysisDetail';
  accommodation?: Maybe<Accommodation>;
  country_id: Scalars['String'];
  country_name: Scalars['String'];
  covid19?: Maybe<MeetingCovidInfectionRate>;
  destination?: Maybe<Destination>;
  id: Scalars['ID'];
  meeting?: Maybe<Meeting>;
  name: Scalars['String'];
  origins?: Maybe<Array<Maybe<Origin>>>;
  risk?: Maybe<PlaceDetailsTravelAdvisoryRiskResponse>;
  travel?: Maybe<MeetingTravelDetailsOriginFull>;
  visas?: Maybe<Array<Maybe<MeetingVisaRestrictions>>>;
  weather?: Maybe<PlaceDetailsWeatherResponse>;
};

export type MeetingCovidInfectionRate = {
  __typename?: 'MeetingCovidInfectionRate';
  country_id?: Maybe<Scalars['String']>;
  data?: Maybe<Array<Maybe<PlaceDetailsCovidInfectionRatesResponse>>>;
};

/** A meeting in Trooptravel for an input result */
export type MeetingInput = {
  created?: InputMaybe<Scalars['Date']>;
  description?: InputMaybe<Scalars['String']>;
  end_date?: InputMaybe<Scalars['Date']>;
  end_time?: InputMaybe<Scalars['Timestamp']>;
  id?: InputMaybe<Scalars['String']>;
  isTemplate?: InputMaybe<Scalars['Boolean']>;
  meeting_policy_id?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  organization_id: Scalars['String'];
  owner_id: Scalars['String'];
  required_services?: InputMaybe<Array<MeetingServices>>;
  start_date?: InputMaybe<Scalars['Date']>;
  start_time?: InputMaybe<Scalars['Timestamp']>;
  status: MeetingStatus;
  tags?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  travel_policy_id?: InputMaybe<Scalars['String']>;
  type?: InputMaybe<MeetingType>;
  updated_by?: InputMaybe<Scalars['String']>;
};

export type MeetingPaginationResponse = {
  __typename?: 'MeetingPaginationResponse';
  data?: Maybe<Array<Meeting>>;
  page?: Maybe<Page>;
};

export type MeetingPolicy = {
  __typename?: 'MeetingPolicy';
  flight_allowed_stops_more_than_min?: Maybe<Scalars['Int']>;
  flight_avoid_carriers?: Maybe<Array<Scalars['String']>>;
  flight_cabin_class?: Maybe<Array<FlightCabinClass>>;
  flight_low_cost_airlines: Scalars['Boolean'];
  flight_max_leg_segment_duration?: Maybe<Scalars['Int']>;
  flight_max_leg_segment_duration_business?: Maybe<Scalars['Int']>;
  flight_max_leg_segment_duration_check?: Maybe<Scalars['Int']>;
  flight_max_leg_segment_duration_classes?: Maybe<Array<Scalars['Int']>>;
  flight_max_leg_segment_duration_economy?: Maybe<Scalars['Int']>;
  flight_max_leg_segment_duration_first_class?: Maybe<Scalars['Int']>;
  flight_max_leg_segment_duration_premium_economy?: Maybe<Scalars['Int']>;
  flight_max_stops?: Maybe<Scalars['Int']>;
  flight_max_total_flight_leg_duration?: Maybe<Scalars['Int']>;
  flight_max_travel_duration?: Maybe<Scalars['Int']>;
  flight_preferred_carriers?: Maybe<Array<Scalars['String']>>;
  flight_providers: Array<Scalars['String']>;
  hotel_allow_only_green: Scalars['Boolean'];
  hotel_allow_only_preferred: Scalars['Boolean'];
  hotel_allow_room_sharing: Scalars['Boolean'];
  hotel_apply_cap: Scalars['Boolean'];
  hotel_minimum_rating: Scalars['Int'];
  hotel_number_to_suggest: Scalars['Int'];
  hotel_optimization: Array<Scalars['String']>;
  hotel_residents_require_accommodation: Scalars['Boolean'];
  hotel_star_rating: Array<Scalars['Int']>;
  hotel_suggest_radius: Scalars['Int'];
  id: Scalars['String'];
  meeting_id: Scalars['String'];
  name: Scalars['String'];
  status?: Maybe<PolicyStatus>;
  transport_consider_alternative_max_distance: Scalars['Int'];
  transport_consider_flying_min_distance: Scalars['Int'];
  transport_modes: Array<Scalars['String']>;
  travel_policy_id: Scalars['String'];
};

export type MeetingPolicyInput = {
  flight_allowed_stops_more_than_min?: InputMaybe<Scalars['Int']>;
  flight_avoid_carriers?: InputMaybe<Array<Scalars['String']>>;
  flight_cabin_class?: InputMaybe<Array<FlightCabinClass>>;
  flight_low_cost_airlines?: InputMaybe<Scalars['Boolean']>;
  flight_max_leg_segment_duration?: InputMaybe<Scalars['Int']>;
  flight_max_leg_segment_duration_business?: InputMaybe<Scalars['Int']>;
  flight_max_leg_segment_duration_check?: InputMaybe<Scalars['Int']>;
  flight_max_leg_segment_duration_classes?: InputMaybe<Array<Scalars['Int']>>;
  flight_max_leg_segment_duration_economy?: InputMaybe<Scalars['Int']>;
  flight_max_leg_segment_duration_first_class?: InputMaybe<Scalars['Int']>;
  flight_max_leg_segment_duration_premium_economy?: InputMaybe<Scalars['Int']>;
  flight_max_stops?: InputMaybe<Scalars['Int']>;
  flight_max_total_flight_leg_duration?: InputMaybe<Scalars['Int']>;
  flight_max_travel_duration?: InputMaybe<Scalars['Int']>;
  flight_preferred_carriers?: InputMaybe<Array<Scalars['String']>>;
  flight_providers?: InputMaybe<Array<Scalars['String']>>;
  hotel_allow_only_green?: InputMaybe<Scalars['Boolean']>;
  hotel_allow_only_preferred?: InputMaybe<Scalars['Boolean']>;
  hotel_allow_room_sharing?: InputMaybe<Scalars['Boolean']>;
  hotel_apply_cap?: InputMaybe<Scalars['Boolean']>;
  hotel_minimum_rating?: InputMaybe<Scalars['Int']>;
  hotel_number_to_suggest?: InputMaybe<Scalars['Int']>;
  hotel_optimization?: InputMaybe<Array<Scalars['String']>>;
  hotel_residents_require_accommodation?: InputMaybe<Scalars['Boolean']>;
  hotel_star_rating?: InputMaybe<Array<Scalars['Int']>>;
  hotel_suggest_radius?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['String']>;
  meeting_id: Scalars['String'];
  name: Scalars['String'];
  status?: InputMaybe<PolicyStatus>;
  transport_consider_alternative_max_distance?: InputMaybe<Scalars['Int']>;
  transport_consider_flying_min_distance?: InputMaybe<Scalars['Int']>;
  transport_modes?: InputMaybe<Array<Scalars['String']>>;
  travel_policy_id: Scalars['String'];
};

export type MeetingPolicyOverride = {
  __typename?: 'MeetingPolicyOverride';
  hotel_custom_spending_cap?: Maybe<Scalars['Int']>;
  hotel_optimization: Array<Scalars['String']>;
  hotel_star_rating: Array<Scalars['Int']>;
};

export type MeetingPolicyOverrideInput = {
  hotel_custom_spending_cap: Scalars['Int'];
  hotel_optimization: Array<Scalars['String']>;
  hotel_star_rating: Array<Scalars['Int']>;
};

/** Schema for Meeting Response */
export type MeetingResponse = {
  __typename?: 'MeetingResponse';
  code?: Maybe<Scalars['Int']>;
  data?: Maybe<Meeting>;
  message?: Maybe<Scalars['String']>;
  success?: Maybe<Scalars['Boolean']>;
};

/** ENUM for user selectable Meeting Services */
export enum MeetingServices {
  Accommodation = 'ACCOMMODATION',
  MeetingSpace = 'MEETING_SPACE',
  Transport = 'TRANSPORT'
}

/** Organization Meetings Space Type Schema */
export type MeetingSpace = {
  __typename?: 'MeetingSpace';
  address?: Maybe<Scalars['String']>;
  address_city?: Maybe<Scalars['String']>;
  address_country_or_region?: Maybe<Scalars['String']>;
  address_postal_code?: Maybe<Scalars['String']>;
  audio_device_name?: Maybe<Scalars['String']>;
  booking_type?: Maybe<Scalars['String']>;
  building?: Maybe<Scalars['String']>;
  capacity?: Maybe<Scalars['Int']>;
  city_code?: Maybe<Scalars['String']>;
  country_code?: Maybe<Scalars['String']>;
  display_device_name?: Maybe<Scalars['String']>;
  display_name?: Maybe<Scalars['String']>;
  email_address?: Maybe<Scalars['String']>;
  floor_label?: Maybe<Scalars['String']>;
  floor_number?: Maybe<Scalars['String']>;
  geo_coordinates?: Maybe<Scalars['JSON']>;
  graph_user_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['ID']>;
  is_wheel_chair_accessable?: Maybe<Scalars['Int']>;
  label?: Maybe<Scalars['String']>;
  lat?: Maybe<Scalars['Float']>;
  lng?: Maybe<Scalars['Float']>;
  nickname?: Maybe<Scalars['String']>;
  organization_id?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  photos?: Maybe<Scalars['JSON']>;
  roomCount?: Maybe<Scalars['Int']>;
  rooms?: Maybe<Array<Maybe<MeetingSpaceRoom>>>;
  tags?: Maybe<Scalars['JSON']>;
  video_device_name?: Maybe<Scalars['String']>;
};

export type MeetingSpaceList = {
  __typename?: 'MeetingSpaceList';
  city_code?: Maybe<Scalars['String']>;
  data?: Maybe<Array<Maybe<MeetingSpace>>>;
  organization_id?: Maybe<Scalars['String']>;
  page?: Maybe<Page>;
};

/** Organization Meetings Space Type Schema */
export type MeetingSpaceRoom = {
  __typename?: 'MeetingSpaceRoom';
  address?: Maybe<Scalars['String']>;
  address_city?: Maybe<Scalars['String']>;
  address_country_or_region?: Maybe<Scalars['String']>;
  address_postal_code?: Maybe<Scalars['String']>;
  audio_device_name?: Maybe<Scalars['String']>;
  booking_type?: Maybe<Scalars['String']>;
  building?: Maybe<Scalars['String']>;
  capacity?: Maybe<Scalars['Int']>;
  city_code?: Maybe<Scalars['String']>;
  country_code?: Maybe<Scalars['String']>;
  display_device_name?: Maybe<Scalars['String']>;
  display_name?: Maybe<Scalars['String']>;
  email_address?: Maybe<Scalars['String']>;
  floor_label?: Maybe<Scalars['String']>;
  floor_number?: Maybe<Scalars['String']>;
  geo_coordinates?: Maybe<Scalars['JSON']>;
  graph_user_id?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['ID']>;
  is_wheel_chair_accessable?: Maybe<Scalars['Int']>;
  label?: Maybe<Scalars['String']>;
  lat?: Maybe<Scalars['Float']>;
  lng?: Maybe<Scalars['Float']>;
  nickname?: Maybe<Scalars['String']>;
  organization_id?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  photos?: Maybe<Scalars['JSON']>;
  schedule?: Maybe<Scalars['JSON']>;
  tags?: Maybe<Scalars['JSON']>;
  video_device_name?: Maybe<Scalars['String']>;
};

/** ENUM containing possible values for the Meeting.status field */
export enum MeetingStatus {
  Active = 'ACTIVE',
  Archived = 'ARCHIVED',
  Cancelled = 'CANCELLED',
  Created = 'CREATED',
  Draft = 'DRAFT',
  Past = 'PAST',
  Upcoming = 'UPCOMING'
}

/** Organization Specific Tags */
export type MeetingTag = {
  __typename?: 'MeetingTag';
  id: Scalars['ID'];
  name: Scalars['String'];
  organization_id: Scalars['ID'];
};

export type MeetingTagInput = {
  id?: InputMaybe<Scalars['ID']>;
  name: Scalars['String'];
  organization_id: Scalars['ID'];
};

/** Extended to solve Meeting Analysis type in Meetings SB */
export type MeetingTravelDetailsOrigin = {
  __typename?: 'MeetingTravelDetailsOrigin';
  data?: Maybe<Array<Maybe<TravelDetailsOrigin>>>;
  destination_id: Scalars['ID'];
  meeting_id: Scalars['ID'];
  summary: MeetingTravelDetailsOriginSummary;
};

/** Extended to solve Meeting Analysis details type in Meetings SB */
export type MeetingTravelDetailsOriginFull = {
  __typename?: 'MeetingTravelDetailsOriginFull';
  data: Array<Maybe<TravelDetailsOrigin>>;
  destination_id: Scalars['ID'];
  meeting_id: Scalars['ID'];
  origins: Array<Maybe<Scalars['JSON']>>;
};

export type MeetingTravelDetailsOriginSummary = {
  __typename?: 'MeetingTravelDetailsOriginSummary';
  totalCost?: Maybe<Scalars['Float']>;
  totalDuration?: Maybe<Scalars['Float']>;
  totalEmissions?: Maybe<Scalars['Float']>;
  totalStops?: Maybe<Scalars['Float']>;
  totalTravellers?: Maybe<Scalars['Int']>;
};

/** ENUM containing possible values for the Meeting.type field */
export enum MeetingType {
  External = 'EXTERNAL',
  Internal = 'INTERNAL'
}

export type MeetingVisaRestrictions = {
  __typename?: 'MeetingVisaRestrictions';
  data?: Maybe<Array<Maybe<PlaceDetailsVisaRequirementsResponse>>>;
  from_countries?: Maybe<Array<Maybe<Scalars['String']>>>;
  origin_id?: Maybe<Scalars['String']>;
  to_country?: Maybe<Scalars['String']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  adoptTemplate: MeetingResponse;
  /** Sign In with a valid Troop Provider */
  authProvider: AuthResponse;
  /** Sign In with a valid Troop SAML Provider */
  authSaml: AuthResponse;
  /** Create a User on Initial SignUp */
  authUser: AuthResponse;
  /** Confirm Reset Password */
  confirmResetPassword: Scalars['Boolean'];
  /** Response from email to verify user email */
  confirmVerifyUser: Scalars['Boolean'];
  /** Set Default Roles and Permissions for an Organization */
  defaultOrganizationSetup?: Maybe<Organization>;
  /** Remove an existing office for an organization */
  removeOrganizationOffice?: Maybe<Scalars['Boolean']>;
  removeTravelProfileAddress?: Maybe<Scalars['Boolean']>;
  removeTravelProfilePassport?: Maybe<Scalars['Boolean']>;
  /** Remove Role from User */
  removeUserRole: UserRoleResponse;
  /** Reset Password */
  resetPassword: Scalars['Boolean'];
  /** Setting selected Accommodation for a meeting */
  setAccommodation?: Maybe<Accommodation>;
  /** Adding Analysis Progress to Destination */
  setAnalysisProgressUpdate: AnalysisProgressResponse;
  /** Setting (creating/updating) an attendee */
  setAttendee: AttendeeResponse;
  /** Adding a Destination to a meeting */
  setDestination: DestinationResponse;
  /** Adding Analysis Details to Destination */
  setDestinationAnalysis: DestinationResponse;
  setDestinationAnalysisStatus?: Maybe<Scalars['Boolean']>;
  /** Adding Analysis Details to Destination */
  setDestinationMeetingPolicyOverride: DestinationItemResponse;
  /** Setting (Create/Update) a license */
  setLicense: License;
  /** Setting (creating/updating) a meeting */
  setMeeting: MeetingResponse;
  setMeetingPolicy: MeetingPolicy;
  /** Add a custom meeting tag */
  setMeetingTag: MeetingTag;
  /** (Create/Update) Organizations */
  setOrganization: Organization;
  /** Add cap for a given organization */
  setOrganizationCap: OrganizationCap;
  /** Setting (Create/Update) a mapping between organization and license */
  setOrganizationLicense: OrganizationLicense;
  /** Set an OrganizationLicense Id */
  setOrganizationLicenseId: Organization;
  /** Adding a new office for an organization */
  setOrganizationOffice: Office;
  /** Adding a new resource to an organization */
  setOrganizationResource: OrganizationResource;
  /** (Create/Update) Organization Roles */
  setOrganizationRole: Organization;
  /** Adding an Origin to a meeting */
  setOrigin: OriginResponse;
  /** Update travel_analysis data in postgres with provided itineraryId */
  setTravelAnalysis: TravelResponse;
  /** Query to get all meetings for a client */
  setTravelPolicy: TravelPolicy;
  /** Setting (creating/updating) a travel profile */
  setTravelProfile?: Maybe<TravelProfile>;
  /** Adding an address to a travel profile */
  setTravelProfileAddress?: Maybe<TravelProfileAddress>;
  /** Adding a passport to a travel profile */
  setTravelProfilePassport?: Maybe<TravelProfilePassport>;
  /** (Create/Update) User */
  setUser: UserResponse;
  /** Link an Organization to a User */
  setUserOrganization: UserOrganizationDetails;
  /** Add Role to User */
  setUserRole: UserRoleResponse;
  /** Update travelPolicyPreferredFlightCarrier data in postgres with provided travelPolicyId */
  travelPolicyPreferredFlightCarrier: TravelResponse;
  /** Uploads logo for given organizationId */
  uploadOrganizationLogos: LogoResponse;
  /** Verify user email */
  verifyUser: Scalars['Boolean'];
};


export type MutationAdoptTemplateArgs = {
  form: AdoptTemplateInput;
};


export type MutationAuthProviderArgs = {
  provider: AuthProviders;
  user: AuthProviderForm;
};


export type MutationAuthSamlArgs = {
  user: AuthSamlForm;
};


export type MutationAuthUserArgs = {
  user: AuthForm;
};


export type MutationConfirmResetPasswordArgs = {
  form: ResetPasswordForm;
};


export type MutationConfirmVerifyUserArgs = {
  form: UserVerifyEmailForm;
};


export type MutationDefaultOrganizationSetupArgs = {
  organizationId: Scalars['String'];
};


export type MutationRemoveOrganizationOfficeArgs = {
  id: Scalars['ID'];
};


export type MutationRemoveTravelProfileAddressArgs = {
  travelAddressId: Scalars['ID'];
  travelProfileId: Scalars['String'];
};


export type MutationRemoveTravelProfilePassportArgs = {
  travelPassportId: Scalars['ID'];
  travelProfileId: Scalars['String'];
};


export type MutationRemoveUserRoleArgs = {
  user: UserRoleInput;
};


export type MutationResetPasswordArgs = {
  user: UserEmailForm;
};


export type MutationSetAccommodationArgs = {
  defaultId: Scalars['String'];
  destinationId: Scalars['String'];
};


export type MutationSetAnalysisProgressUpdateArgs = {
  progress: AnalysisProgressUpdateInput;
};


export type MutationSetAttendeeArgs = {
  attendee: AttendeeInput;
};


export type MutationSetDestinationArgs = {
  destination: DestinationInput;
};


export type MutationSetDestinationAnalysisArgs = {
  analysis: DestinationAnalysisInput;
};


export type MutationSetDestinationAnalysisStatusArgs = {
  meetingId: Scalars['ID'];
};


export type MutationSetDestinationMeetingPolicyOverrideArgs = {
  destination: DestinationMeetingPolicyOverrideInput;
};


export type MutationSetLicenseArgs = {
  license: LicenseInput;
};


export type MutationSetMeetingArgs = {
  meeting: MeetingInput;
};


export type MutationSetMeetingPolicyArgs = {
  meetingPolicy: MeetingPolicyInput;
  organizationId: Scalars['String'];
};


export type MutationSetMeetingTagArgs = {
  meetingtag: MeetingTagInput;
};


export type MutationSetOrganizationArgs = {
  organization: OrganizationInput;
};


export type MutationSetOrganizationCapArgs = {
  cap: OrganizationCapForm;
};


export type MutationSetOrganizationLicenseArgs = {
  organizationLicense: OrganizationLicenseInput;
};


export type MutationSetOrganizationLicenseIdArgs = {
  organizationId: Scalars['String'];
  organizationLicenseId: Scalars['String'];
};


export type MutationSetOrganizationOfficeArgs = {
  office: OfficeInput;
};


export type MutationSetOrganizationResourceArgs = {
  resource: OrganizationResourceInput;
};


export type MutationSetOrganizationRoleArgs = {
  organization: OrganizationRoleInput;
};


export type MutationSetOriginArgs = {
  origin: OriginInput;
};


export type MutationSetTravelAnalysisArgs = {
  itineraryId: Scalars['Int'];
};


export type MutationSetTravelPolicyArgs = {
  organizationId: Scalars['String'];
  travelPolicy: TravelPolicyInput;
};


export type MutationSetTravelProfileArgs = {
  profile: TravelProfileInput;
};


export type MutationSetTravelProfileAddressArgs = {
  address: ProfileAddressInput;
};


export type MutationSetTravelProfilePassportArgs = {
  passport: ProfilePassportInput;
};


export type MutationSetUserArgs = {
  user: UserForm;
};


export type MutationSetUserOrganizationArgs = {
  form?: InputMaybe<UserOrganizationForm>;
  organizationId: Scalars['String'];
  userId: Scalars['String'];
};


export type MutationSetUserRoleArgs = {
  user: UserRoleInput;
};


export type MutationTravelPolicyPreferredFlightCarrierArgs = {
  carriers: Array<InputMaybe<Scalars['String']>>;
  travelPolicyId: Scalars['String'];
};


export type MutationUploadOrganizationLogosArgs = {
  logos: Array<InputMaybe<LogoUpload>>;
  organizationId: Scalars['String'];
};


export type MutationVerifyUserArgs = {
  user: UserEmailForm;
};

/** Organization Office Type Schema */
export type Office = {
  __typename?: 'Office';
  address?: Maybe<Scalars['String']>;
  city_code?: Maybe<Scalars['String']>;
  client_id?: Maybe<Scalars['String']>;
  closest_int_airport?: Maybe<Scalars['String']>;
  country_code?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['ID']>;
  lnglat?: Maybe<Scalars['Point']>;
  name?: Maybe<Scalars['String']>;
  organization_id?: Maybe<Scalars['ID']>;
  selected_airport_distance?: Maybe<Scalars['Float']>;
  selected_city_distance?: Maybe<Scalars['Float']>;
  type?: Maybe<OfficeType>;
};

/** The input to insert or update an organization's office */
export type OfficeInput = {
  address?: InputMaybe<Scalars['String']>;
  city_code?: InputMaybe<Scalars['String']>;
  client_id?: InputMaybe<Scalars['String']>;
  closest_int_airport?: InputMaybe<Scalars['String']>;
  country_code?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  lnglat?: InputMaybe<Scalars['Point']>;
  name?: InputMaybe<Scalars['String']>;
  organization_id?: InputMaybe<Scalars['ID']>;
  selected_airport_distance?: InputMaybe<Scalars['Float']>;
  selected_city_distance?: InputMaybe<Scalars['Float']>;
  type?: InputMaybe<OfficeType>;
};

export type OfficeList = {
  __typename?: 'OfficeList';
  city_code?: Maybe<Scalars['String']>;
  count?: Maybe<Scalars['Int']>;
  data?: Maybe<Array<Maybe<Office>>>;
  organization_id?: Maybe<Scalars['String']>;
};

/** ENUM continaing possible values for the different Office Types */
export enum OfficeType {
  Catering = 'CATERING',
  CollabHub = 'COLLAB_HUB',
  CompanyStore = 'COMPANY_STORE',
  DataCenter = 'DATA_CENTER',
  Dc = 'DC',
  Depot = 'DEPOT',
  FlexOffice = 'FLEX_OFFICE',
  Hq = 'HQ',
  Hub = 'HUB',
  Laboratory = 'LABORATORY',
  Land = 'LAND',
  Manufacturing = 'MANUFACTURING',
  ManufacturingWarehouse = 'MANUFACTURING_WAREHOUSE',
  Office = 'OFFICE',
  OfficeOrDepot = 'OFFICE_OR_DEPOT',
  OfficeOrLab = 'OFFICE_OR_LAB',
  OfficeOrStorage = 'OFFICE_OR_STORAGE',
  Offiice = 'OFFIICE',
  OtherType = 'OTHER_TYPE',
  ParkingBuilding = 'PARKING_BUILDING',
  ProductionBuilding = 'PRODUCTION_BUILDING',
  Regional = 'REGIONAL',
  RegionalSatelliteOffice = 'REGIONAL_SATELLITE_OFFICE',
  RAndD = 'R_AND_D',
  TechnicalBuilding = 'TECHNICAL_BUILDING',
  Warehouse = 'WAREHOUSE'
}

export type Organization = {
  __typename?: 'Organization';
  app_custom_domain_address?: Maybe<Scalars['String']>;
  assets?: Maybe<Assets>;
  banned_countries?: Maybe<Array<Scalars['String']>>;
  custom_introduction_text?: Maybe<Scalars['String']>;
  global_hotel_cap?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['ID']>;
  identifier?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  organization_license_id?: Maybe<Scalars['String']>;
  resources?: Maybe<Array<Maybe<OrganizationResource>>>;
  response_property?: Maybe<Scalars['String']>;
  roles?: Maybe<Array<Maybe<OrganizationRole>>>;
  saml_code?: Maybe<Scalars['SsoCode']>;
  saml_provider?: Maybe<Array<Maybe<Scalars['String']>>>;
  signin_types?: Maybe<Array<Scalars['String']>>;
  theme_variables?: Maybe<Scalars['String']>;
  user_email_domains?: Maybe<Array<Scalars['String']>>;
  user_email_tld?: Maybe<Scalars['String']>;
};

/** Organization Cap Response */
export type OrganizationCap = {
  __typename?: 'OrganizationCap';
  cap_type: CapType;
  cap_value: Scalars['Float'];
  client_id: Scalars['String'];
  currency_code: Scalars['String'];
  id: Scalars['String'];
  organization_id: Scalars['String'];
  type: CapLocaleType;
  type_code: Scalars['String'];
};

/** Cap Form */
export type OrganizationCapForm = {
  capType: CapType;
  capValue: Scalars['Float'];
  clientId: Scalars['String'];
  currencyCode: Scalars['String'];
  organizationId: Scalars['String'];
  type: CapLocaleType;
  typeCode: Scalars['String'];
};

export type OrganizationInput = {
  app_custom_domain_address?: InputMaybe<Scalars['String']>;
  banned_countries?: InputMaybe<Array<Scalars['String']>>;
  custom_introduction_text?: InputMaybe<Scalars['String']>;
  global_hotel_cap?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  response_property: Scalars['String'];
  saml_provider?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  signin_types: Array<Scalars['String']>;
  theme_variables?: InputMaybe<Scalars['String']>;
  user_email_domains: Array<Scalars['String']>;
  user_email_tld: Scalars['String'];
};

export type OrganizationLicense = {
  __typename?: 'OrganizationLicense';
  created?: Maybe<Scalars['Date']>;
  id: Scalars['ID'];
  license?: Maybe<License>;
  license_id: Scalars['ID'];
  organization_id: Scalars['ID'];
  status?: Maybe<OrganizationLicenseStatus>;
  updated?: Maybe<Scalars['Date']>;
};

export type OrganizationLicenseInput = {
  created?: InputMaybe<Scalars['Date']>;
  id?: InputMaybe<Scalars['String']>;
  license_id: Scalars['ID'];
  organization_id: Scalars['ID'];
  status: OrganizationLicenseStatus;
};

/** ENUM continaing possible values for License Organization Status */
export enum OrganizationLicenseStatus {
  Active = 'ACTIVE',
  Archived = 'ARCHIVED',
  Inactive = 'INACTIVE',
  Pending = 'PENDING'
}

export enum OrganizationLogo {
  Dark = 'DARK',
  Light = 'LIGHT',
  Thumbnail = 'THUMBNAIL'
}

export type OrganizationProviderResponse = {
  __typename?: 'OrganizationProviderResponse';
  code?: Maybe<Scalars['Int']>;
  exist: Scalars['Boolean'];
  message?: Maybe<Scalars['String']>;
  provider?: Maybe<Array<Scalars['String']>>;
  success?: Maybe<Scalars['Boolean']>;
};

/** The Roles the user has in an Organization */
export type OrganizationResource = {
  __typename?: 'OrganizationResource';
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  organization_id?: Maybe<Scalars['ID']>;
  status?: Maybe<OrganizationResourcesStatus>;
  title: Scalars['String'];
  type?: Maybe<OrganizationResourceType>;
  value?: Maybe<Scalars['String']>;
};

export type OrganizationResourceInput = {
  description?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  organization_id?: InputMaybe<Scalars['ID']>;
  status?: InputMaybe<OrganizationResourcesStatus>;
  title: Scalars['String'];
  type?: InputMaybe<OrganizationResourceType>;
  value?: InputMaybe<Scalars['String']>;
};

/** ENUM containing possible values for Organization Resource */
export enum OrganizationResourceType {
  Hyperlink = 'HYPERLINK'
}

/** ENUM continaing possible values for Organization Resources Status */
export enum OrganizationResourcesStatus {
  Active = 'ACTIVE',
  Archived = 'ARCHIVED',
  Pending = 'PENDING'
}

/** User Response */
export type OrganizationResponse = {
  __typename?: 'OrganizationResponse';
  code?: Maybe<Scalars['Int']>;
  data?: Maybe<Organization>;
  message?: Maybe<Scalars['String']>;
  success?: Maybe<Scalars['Boolean']>;
};

export type OrganizationRole = {
  __typename?: 'OrganizationRole';
  id: Scalars['ID'];
  name: Scalars['String'];
  permissions?: Maybe<Array<OrganizationRolePermission>>;
};

export type OrganizationRoleInput = {
  id?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  permissions?: InputMaybe<Array<RolePermissions>>;
};

export type OrganizationRolePermission = {
  __typename?: 'OrganizationRolePermission';
  id: Scalars['ID'];
  name: Scalars['String'];
};

export type OrganizationSso = {
  __typename?: 'OrganizationSso';
  assets?: Maybe<Assets>;
  name?: Maybe<Scalars['String']>;
  saml_code?: Maybe<Scalars['SsoCode']>;
  saml_provider?: Maybe<Array<Maybe<Scalars['String']>>>;
  signin_types?: Maybe<Array<Scalars['String']>>;
  theme_variables?: Maybe<Scalars['String']>;
};

/** An Origin for Meeting */
export type Origin = {
  __typename?: 'Origin';
  address: Scalars['String'];
  airport_id?: Maybe<Scalars['String']>;
  airport_lnglat?: Maybe<Scalars['Point']>;
  airport_name?: Maybe<Scalars['String']>;
  attendees?: Maybe<OriginAttendees>;
  city_id?: Maybe<Scalars['String']>;
  city_lnglat?: Maybe<Scalars['Point']>;
  city_name?: Maybe<Scalars['String']>;
  city_sygic_image_id?: Maybe<Scalars['String']>;
  country_id: Scalars['String'];
  country_lnglat?: Maybe<Scalars['Point']>;
  country_name?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  lnglat?: Maybe<Scalars['Point']>;
  meeting_id: Scalars['String'];
  organization_id?: Maybe<Scalars['String']>;
  places_id?: Maybe<Scalars['String']>;
  status?: Maybe<OriginStatus>;
  template_id?: Maybe<Scalars['String']>;
  timeZone?: Maybe<PlaceDetailsTimeZoneResponse>;
};

export type OriginAttendees = {
  __typename?: 'OriginAttendees';
  count?: Maybe<Scalars['Int']>;
  list?: Maybe<Array<Attendee>>;
};

export type OriginDetails = {
  __typename?: 'OriginDetails';
  count?: Maybe<Scalars['Int']>;
  origin?: Maybe<Origin>;
};

/** Add/Remove origin from Meeting */
export type OriginInput = {
  address: Scalars['String'];
  airport_id?: InputMaybe<Scalars['String']>;
  airport_lnglat?: InputMaybe<Scalars['Point']>;
  airport_name?: InputMaybe<Scalars['String']>;
  change: Scalars['String'];
  city_id?: InputMaybe<Scalars['String']>;
  city_lnglat?: InputMaybe<Scalars['Point']>;
  city_name?: InputMaybe<Scalars['String']>;
  city_sygic_image_id?: InputMaybe<Scalars['String']>;
  country_id?: InputMaybe<Scalars['String']>;
  country_lnglat?: InputMaybe<Scalars['Point']>;
  country_name?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  lnglat?: InputMaybe<Scalars['Point']>;
  meeting_id: Scalars['String'];
  organization_id: Scalars['String'];
  places_id?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<OriginStatus>;
  template_id?: InputMaybe<Scalars['String']>;
};

/** Schema for Attendee Origin Response */
export type OriginResponse = {
  __typename?: 'OriginResponse';
  code?: Maybe<Scalars['Int']>;
  data?: Maybe<OriginDetails>;
  message?: Maybe<Scalars['String']>;
  success?: Maybe<Scalars['Boolean']>;
};

/** ENUM continaing possible values for Origin Status */
export enum OriginStatus {
  Active = 'ACTIVE',
  Archived = 'ARCHIVED'
}

/** Page Type for Pagination */
export type Page = {
  __typename?: 'Page';
  currentPage?: Maybe<Scalars['Int']>;
  from?: Maybe<Scalars['Int']>;
  lastPage?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  perPage?: Maybe<Scalars['Int']>;
  to?: Maybe<Scalars['Int']>;
  total?: Maybe<Scalars['Int']>;
};

export type PageFiltering = {
  __typename?: 'PageFiltering';
  currentPage?: Maybe<Scalars['Int']>;
  from?: Maybe<Scalars['Int']>;
  lastPage?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  perPage?: Maybe<Scalars['Int']>;
  to?: Maybe<Scalars['Int']>;
  total?: Maybe<Scalars['Int']>;
  totalAfterFiltering?: Maybe<Scalars['Int']>;
};

export type PlaceCityRequest = {
  city_id: Scalars['String'];
};

export type PlaceCovidRatesRequest = {
  country_id: Scalars['String'];
};

export type PlaceDetailsCityResponse = {
  __typename?: 'PlaceDetailsCityResponse';
  city_airports?: Maybe<Scalars['JSONObject']>;
  city_code?: Maybe<Scalars['String']>;
  city_country_code?: Maybe<Scalars['String']>;
  city_g_json?: Maybe<Scalars['JSONObject']>;
  city_g_place_id?: Maybe<Scalars['String']>;
  city_g_rating?: Maybe<Scalars['String']>;
  city_g_region_id?: Maybe<Scalars['String']>;
  city_g_region_name?: Maybe<Scalars['String']>;
  city_iata_code?: Maybe<Scalars['String']>;
  city_is_international?: Maybe<Scalars['Int']>;
  city_lat?: Maybe<Scalars['Float']>;
  city_lng?: Maybe<Scalars['Float']>;
  city_name?: Maybe<Scalars['String']>;
  city_ranking?: Maybe<Scalars['Int']>;
  city_single_airport_city?: Maybe<Scalars['Int']>;
  city_ss_id?: Maybe<Scalars['String']>;
  city_ss_region_code?: Maybe<Scalars['String']>;
  city_ss_updated_timestamp?: Maybe<Scalars['Date']>;
  city_summary_text?: Maybe<Scalars['String']>;
  city_sygic_id?: Maybe<Scalars['String']>;
  city_sygic_image_id?: Maybe<Scalars['String']>;
  city_sygic_rating?: Maybe<Scalars['Float']>;
  city_time_zone?: Maybe<Scalars['Int']>;
  city_time_zone_id?: Maybe<Scalars['String']>;
  country_cases_active?: Maybe<Scalars['String']>;
  country_cases_as_of_date?: Maybe<Scalars['String']>;
  country_cases_confirmed?: Maybe<Scalars['Int']>;
  country_cases_deaths?: Maybe<Scalars['Int']>;
  country_cases_recovered?: Maybe<Scalars['String']>;
  country_code?: Maybe<Scalars['String']>;
  country_covid_risk_level?: Maybe<Scalars['String']>;
  country_infection_as_of_date?: Maybe<Scalars['String']>;
  country_infection_level?: Maybe<Scalars['String']>;
  country_infection_rate?: Maybe<Scalars['Float']>;
  country_it_entry_and_borders_entry_ban?: Maybe<Scalars['String']>;
  country_it_entry_and_borders_entry_rules?: Maybe<Scalars['String']>;
  country_it_entry_and_borders_exemptions?: Maybe<Scalars['String']>;
  country_it_entry_and_borders_last_update?: Maybe<Scalars['String']>;
  country_it_entry_and_borders_text?: Maybe<Scalars['String']>;
  country_it_entry_and_borders_through_date?: Maybe<Scalars['String']>;
  country_it_exit_country_exit_requirements?: Maybe<Scalars['String']>;
  country_it_exit_country_last_update?: Maybe<Scalars['String']>;
  country_it_exit_country_requirement?: Maybe<Scalars['String']>;
  country_it_exit_country_text?: Maybe<Scalars['String']>;
  country_it_health_travel_doc_health_document?: Maybe<Scalars['String']>;
  country_it_health_travel_doc_last_update?: Maybe<Scalars['String']>;
  country_it_health_travel_doc_need_documentations?: Maybe<Scalars['String']>;
  country_it_health_travel_doc_text?: Maybe<Scalars['String']>;
  country_it_health_travel_doc_travel_document?: Maybe<Scalars['String']>;
  country_it_international_flights_is_banned?: Maybe<Scalars['String']>;
  country_it_international_flights_last_update?: Maybe<Scalars['String']>;
  country_it_international_flights_text?: Maybe<Scalars['String']>;
  country_it_international_flights_through_date?: Maybe<Scalars['String']>;
  country_it_masks_last_update?: Maybe<Scalars['String']>;
  country_it_masks_requirement?: Maybe<Scalars['String']>;
  country_it_masks_text?: Maybe<Scalars['String']>;
  country_it_quarantine_on_arrival_days?: Maybe<Scalars['String']>;
  country_it_quarantine_on_arrival_last_update?: Maybe<Scalars['String']>;
  country_it_quarantine_on_arrival_mandate_list?: Maybe<Scalars['String']>;
  country_it_quarantine_on_arrival_rules?: Maybe<Scalars['String']>;
  country_it_quarantine_on_arrival_text?: Maybe<Scalars['String']>;
  country_it_quarantine_on_arrival_type?: Maybe<Scalars['String']>;
  country_it_quarantine_on_arrival_who_needs?: Maybe<Scalars['String']>;
  country_it_testing_last_update?: Maybe<Scalars['String']>;
  country_it_testing_need_test?: Maybe<Scalars['String']>;
  country_it_testing_requirement?: Maybe<Scalars['String']>;
  country_it_testing_rules?: Maybe<Scalars['String']>;
  country_it_testing_text?: Maybe<Scalars['String']>;
  country_it_testing_when?: Maybe<Scalars['String']>;
  country_name?: Maybe<Scalars['String']>;
  country_policy_current_status?: Maybe<Scalars['String']>;
  country_policy_end_date?: Maybe<Scalars['String']>;
  country_policy_last_update?: Maybe<Scalars['String']>;
  country_policy_start_date?: Maybe<Scalars['String']>;
  country_policy_text?: Maybe<Scalars['String']>;
  country_risk_level?: Maybe<Scalars['String']>;
  country_summary?: Maybe<Scalars['String']>;
  country_travel_risk?: Maybe<Scalars['Float']>;
};

export type PlaceDetailsCovidInfectionRatesResponse = {
  __typename?: 'PlaceDetailsCovidInfectionRatesResponse';
  country_code?: Maybe<Scalars['String']>;
  covid_risk_level?: Maybe<Scalars['String']>;
  infection_level?: Maybe<Scalars['String']>;
  infection_rate?: Maybe<Scalars['Float']>;
  risk_level?: Maybe<Scalars['String']>;
};

/** Restrictions per vaccination status */
export type PlaceDetailsRestrictionsResponse = {
  __typename?: 'PlaceDetailsRestrictionsResponse';
  unvaccinated?: Maybe<Array<Maybe<CovidRestrictionItems>>>;
  vaccinated?: Maybe<Array<Maybe<CovidRestrictionItems>>>;
};

export type PlaceDetailsSearchRequest = {
  city?: InputMaybe<PlaceCityRequest>;
  covid_rates?: InputMaybe<PlaceCovidRatesRequest>;
  restrictions?: InputMaybe<PlaceRestrictionsRequest>;
  time_zone?: InputMaybe<PlaceTimeZoneRequest>;
  travel_risk?: InputMaybe<PlaceRiskRequest>;
  visa_requirements?: InputMaybe<PlaceVisaRequirementsRequest>;
  weather?: InputMaybe<PlaceWeatherRequest>;
};

export type PlaceDetailsSearchResponse = {
  __typename?: 'PlaceDetailsSearchResponse';
  city?: Maybe<PlaceDetailsCityResponse>;
  covid_rates?: Maybe<Array<Maybe<PlaceDetailsCovidInfectionRatesResponse>>>;
  restrictions?: Maybe<PlaceDetailsRestrictionsResponse>;
  time_zone?: Maybe<PlaceDetailsTimeZoneResponse>;
  travel_risk?: Maybe<PlaceDetailsTravelAdvisoryRiskResponse>;
  visa_requirements?: Maybe<Array<Maybe<PlaceDetailsVisaRequirementsResponse>>>;
  weather?: Maybe<PlaceDetailsWeatherResponse>;
};

export type PlaceDetailsTimeZoneResponse = {
  __typename?: 'PlaceDetailsTimeZoneResponse';
  city_id?: Maybe<Scalars['String']>;
  time_zone?: Maybe<Scalars['Int']>;
  time_zone_id?: Maybe<Scalars['String']>;
};

export type PlaceDetailsTravelAdvisoryRiskResponse = {
  __typename?: 'PlaceDetailsTravelAdvisoryRiskResponse';
  country_code?: Maybe<Scalars['String']>;
  country_id?: Maybe<Scalars['String']>;
  risk?: Maybe<Scalars['Float']>;
};

export type PlaceDetailsVisaRequirementsResponse = {
  __typename?: 'PlaceDetailsVisaRequirementsResponse';
  destination?: Maybe<Scalars['String']>;
  duration?: Maybe<Scalars['Int']>;
  from_country?: Maybe<Scalars['String']>;
  origin?: Maybe<Scalars['String']>;
  to_country?: Maybe<Scalars['String']>;
  traveller_count?: Maybe<Scalars['Int']>;
  visa_durations?: Maybe<Array<Maybe<Scalars['Int']>>>;
  visa_required?: Maybe<Scalars['Boolean']>;
  visa_requirement?: Maybe<Scalars['String']>;
  visa_titles?: Maybe<Array<Maybe<Scalars['String']>>>;
};

export type PlaceDetailsWeatherResponse = {
  __typename?: 'PlaceDetailsWeatherResponse';
  city_id?: Maybe<Scalars['String']>;
  end_date?: Maybe<Scalars['Date']>;
  end_stamp?: Maybe<Scalars['Int']>;
  start_date?: Maybe<Scalars['Date']>;
  start_stamp?: Maybe<Scalars['Int']>;
  temp?: Maybe<Scalars['Float']>;
};

export type PlaceInput = {
  address: Scalars['String'];
  airport_id: Scalars['String'];
  airport_lnglat?: InputMaybe<Scalars['Point']>;
  airport_name?: InputMaybe<Scalars['String']>;
  city_id: Scalars['String'];
  city_lnglat?: InputMaybe<Scalars['Point']>;
  city_name?: InputMaybe<Scalars['String']>;
  country_id: Scalars['String'];
  country_lnglat?: InputMaybe<Scalars['Point']>;
  country_name?: InputMaybe<Scalars['String']>;
  lnglat?: InputMaybe<Scalars['Point']>;
};

/** Get Place restrictions - user either (start_date and end_date) or (start_stamp and end_stamp) */
export type PlaceRestrictionsRequest = {
  end_date?: InputMaybe<Scalars['String']>;
  /** stamp = timestamp = new Date().getTime() / 1000 */
  end_stamp?: InputMaybe<Scalars['Int']>;
  origin_codes: Array<Scalars['String']>;
  start_date?: InputMaybe<Scalars['String']>;
  /** stamp = timestamp = new Date().getTime() / 1000 */
  start_stamp?: InputMaybe<Scalars['Int']>;
};

export type PlaceRiskRequest = {
  country_id: Scalars['String'];
};

/** Get Place suggestions for map pins - user either (start_date and end_date) or (start_stamp and end_stamp) */
export type PlaceSuggestionsRequest = {
  client_id?: InputMaybe<Scalars['String']>;
  end_date?: InputMaybe<Scalars['Date']>;
  /** stamp = timestamp = new Date().getTime() / 1000 */
  end_stamp?: InputMaybe<Scalars['Int']>;
  excluded_city_ids?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  excluded_country_ids?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  location: Scalars['Point'];
  radius?: InputMaybe<Scalars['Int']>;
  start_date?: InputMaybe<Scalars['Date']>;
  /** stamp = timestamp = new Date().getTime() / 1000 */
  start_stamp?: InputMaybe<Scalars['Int']>;
};

export type PlaceSuggestionsResponse = {
  __typename?: 'PlaceSuggestionsResponse';
  avgtemp?: Maybe<Scalars['Float']>;
  country_code?: Maybe<Scalars['String']>;
  dist?: Maybe<Scalars['Int']>;
  iata_code?: Maybe<Scalars['String']>;
  is_international?: Maybe<Scalars['Int']>;
  lat?: Maybe<Scalars['Float']>;
  lng?: Maybe<Scalars['Float']>;
  name?: Maybe<Scalars['String']>;
  offices?: Maybe<Scalars['Int']>;
  ranking?: Maybe<Scalars['Int']>;
  region?: Maybe<Scalars['String']>;
  risk?: Maybe<Scalars['Float']>;
  sygic_image_id?: Maybe<Scalars['String']>;
  sygic_rating?: Maybe<Scalars['Float']>;
  time_zone?: Maybe<Scalars['Int']>;
  uuid: Scalars['String'];
};

export type PlaceTimeZoneRequest = {
  city_id: Scalars['String'];
};

export type PlaceVisaRequirementsRequest = {
  from_countries: Array<Scalars['String']>;
  to_country: Scalars['String'];
};

/** Get Place weather temp - user either (start_date and end_date) or (start_stamp and end_stamp) */
export type PlaceWeatherRequest = {
  city_id: Scalars['String'];
  end_date?: InputMaybe<Scalars['Date']>;
  /** stamp = timestamp = new Date().getTime() / 1000 */
  end_stamp?: InputMaybe<Scalars['Int']>;
  start_date?: InputMaybe<Scalars['Date']>;
  /** stamp = timestamp = new Date().getTime() / 1000 */
  start_stamp?: InputMaybe<Scalars['Int']>;
};

/** Response type for meeting planning insights */
export type PlanningInsightsResponse = {
  __typename?: 'PlanningInsightsResponse';
  co2PerAttendee?: Maybe<Scalars['Float']>;
  co2PerMeeting?: Maybe<Scalars['Float']>;
  co2Total?: Maybe<Scalars['Float']>;
  spendPerAttendee?: Maybe<Scalars['Float']>;
  spendPerMeeting?: Maybe<Scalars['Float']>;
  spendTotal?: Maybe<Scalars['Float']>;
  timePerAttendee?: Maybe<Scalars['Float']>;
  timePerMeeting?: Maybe<Scalars['Float']>;
  timeTotal?: Maybe<Scalars['Float']>;
};

/** ENUM containing possible values for the Policy status field */
export enum PolicyStatus {
  Active = 'ACTIVE',
  Archived = 'ARCHIVED'
}

/** Procedure structure */
export type Procedure = {
  __typename?: 'Procedure';
  category: Scalars['String'];
  created_at?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  destination_country_code: Scalars['String'];
  destination_region_code: Scalars['String'];
  documentLinks?: Maybe<Array<Maybe<RestrictionDocumentLink>>>;
  end_date?: Maybe<Scalars['String']>;
  enforcement: Scalars['String'];
  id: Scalars['String'];
  included: Array<Maybe<Scalars['String']>>;
  last_checked_at?: Maybe<Scalars['String']>;
  last_updated_at?: Maybe<Scalars['String']>;
  origin_count: Scalars['Int'];
  region_included: Array<Maybe<Scalars['String']>>;
  severity: Scalars['Int'];
  source: CovidSource;
  start_date?: Maybe<Scalars['String']>;
  still_current_at?: Maybe<Scalars['String']>;
  subcategory: Scalars['String'];
  tags?: Maybe<Array<Maybe<Scalars['String']>>>;
  title: Scalars['String'];
  tt_updated_at: Scalars['String'];
};

export type ProfileAddressInput = {
  address_1?: InputMaybe<Scalars['String']>;
  address_2?: InputMaybe<Scalars['String']>;
  city_id?: InputMaybe<Scalars['String']>;
  city_lnglat?: InputMaybe<Scalars['Point']>;
  city_name?: InputMaybe<Scalars['String']>;
  country_id?: InputMaybe<Scalars['String']>;
  country_lnglat?: InputMaybe<Scalars['Point']>;
  country_name?: InputMaybe<Scalars['String']>;
  default?: InputMaybe<Scalars['Boolean']>;
  id?: InputMaybe<Scalars['ID']>;
  state_id?: InputMaybe<Scalars['String']>;
  state_name?: InputMaybe<Scalars['String']>;
  suburb_id?: InputMaybe<Scalars['String']>;
  suburb_name?: InputMaybe<Scalars['String']>;
  travel_profile_id: Scalars['String'];
};

export type ProfilePassportInput = {
  expiry_date: Scalars['Date'];
  id?: InputMaybe<Scalars['ID']>;
  issuing_authority: Scalars['String'];
  issuing_country: Scalars['String'];
  issuing_date: Scalars['Date'];
  number: Scalars['String'];
  travel_profile_id: Scalars['String'];
  type: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  accommodation: Accommodation;
  accommodationList: AccommodationPaginationResponse;
  /** Query to fetch the analytics on active users given a date interval */
  activeUsers?: Maybe<ActiveUsersResponse>;
  /** Query to fetch the analytics on the number of created vs analysed meetings */
  analysedMeetings?: Maybe<AnalysedMeetingsResponse>;
  analysisProgress: AnalysisProgress;
  analyzeDestination: AnalyzeDestination;
  analyzeMeeting?: Maybe<Array<AnalyzeDestination>>;
  analyzeOrigin: AnalyzeDestination;
  /** Query to get a single Attendee */
  attendee: Attendee;
  /** Query to get all meeting Attendees */
  attendeeList?: Maybe<Array<Attendee>>;
  /** Query to fetch the total and average number of origins, destinations and attendees over a date interval for a given organization */
  basicMeetingStatistics?: Maybe<BasicMeetingStatisticsReponse>;
  /** Query to fetch route summary data */
  calcRouteResultTravel: TravelResultResponse;
  /** Query to calc route summary data */
  calcRouteSummaryTravel: TravelResponse;
  /** Query to calc route data */
  calcRouteTravel: TravelResponse;
  /** Query to fetch carrier data */
  carrierData: CarrierResponse;
  countryGeoJson: GeoJson;
  countryList: Array<Country>;
  covidPolicyList?: Maybe<CovidPolicies>;
  covidSpecsByOrigin: Array<CovidCoreSpecs>;
  /** Query to get a single Destination */
  destination: Destination;
  /** Query to get all meeting Destinations */
  destinationList?: Maybe<Array<Destination>>;
  /** Query to get directions */
  directions: DirectionResponse;
  /** Query to fetch flight data */
  flightOptionDetail: FlightDataDetailsResponse;
  /** Query to fetch flight data */
  flightOptionList?: Maybe<FlightDataResponse>;
  /** Query to calculate the total hours saved for the organization */
  hoursSaved?: Maybe<HoursSavedResponse>;
  /** Query to determine is user exists in Troop db if not it will check Firebase and if it exits in Firebase it will create it in Troop db. */
  isUser: UserCheckResponse;
  /** Get a single license */
  license?: Maybe<License>;
  /** Get a all licenses */
  licenseList?: Maybe<Array<License>>;
  /** Fetch Suggestions for address search */
  locationSuggestionList: LocationsSearchResponse;
  /** Get a login user */
  me: UserResponse;
  /** Get a meeting for an Organization */
  meeting: MeetingResponse;
  /** Query the Analysis for a Troop Meeting */
  meetingAnalysis: MeetingAnalysis;
  /** Query the Analysis details for a Meeting Destination */
  meetingAnalysisDetail: MeetingAnalysisDetail;
  /** Query to get all meetings for an Organization */
  meetingList?: Maybe<MeetingPaginationResponse>;
  meetingPolicy: MeetingPolicy;
  /** Get a single Meeting Space */
  meetingSpace?: Maybe<MeetingSpaceRoom>;
  /** Get Organization Meeting Space List */
  meetingSpaceList?: Maybe<MeetingSpaceList>;
  /** Query a custom meeting tag */
  meetingTag: MeetingTag;
  /** Query all custom meeting tag */
  meetingTagList?: Maybe<Array<MeetingTag>>;
  /** Get a meeting template for an Organization */
  meetingTemplate: MeetingResponse;
  /** Query to get all meeting templates for an Organization */
  meetingTemplateList?: Maybe<MeetingPaginationResponse>;
  /** Get a single Office */
  office?: Maybe<Office>;
  /** Get Organization Office List */
  officeList?: Maybe<OfficeList>;
  /** Get a single organization for a user */
  organization: OrganizationResponse;
  organizationCap: OrganizationCap;
  organizationCapList?: Maybe<Array<OrganizationCap>>;
  /** Get a map between organization and license */
  organizationLicense?: Maybe<OrganizationLicense>;
  /** Get all organizations for a user */
  organizationList?: Maybe<Array<Organization>>;
  /** Query to determine login/singup options for organization */
  organizationProviderList: OrganizationProviderResponse;
  /** Get a resource to an organization */
  organizationResource: OrganizationResource;
  /** Get a resource to an organization */
  organizationResourceList?: Maybe<Array<OrganizationResource>>;
  /** Get organization from SSO Token */
  organizationSsoAuth?: Maybe<OrganizationSso>;
  /** Query to get a single Origin */
  origin: Origin;
  /** Query to get all meeting Origins */
  originList?: Maybe<Array<Origin>>;
  /** Fetch place City */
  placeCity: PlaceDetailsCityResponse;
  /** Fetch place Covid Infection Rates */
  placeCovidInfectionRateList: Array<Maybe<PlaceDetailsCovidInfectionRatesResponse>>;
  /** Fetch place details */
  placeDetail: PlaceDetailsSearchResponse;
  /** Fetch place Restrictions */
  placeRestrictionList: PlaceDetailsRestrictionsResponse;
  /** Fetch place Risk */
  placeRisk: PlaceDetailsTravelAdvisoryRiskResponse;
  /** Fetch place suggestions */
  placeSuggestionList: Array<Maybe<PlaceSuggestionsResponse>>;
  /** Fetch place Time Zones */
  placeTimeZone: PlaceDetailsTimeZoneResponse;
  /** Fetch place Visa Requirements */
  placeVisaRequirementList: Array<Maybe<PlaceDetailsVisaRequirementsResponse>>;
  /** Fetch place Weather */
  placeWeather: PlaceDetailsWeatherResponse;
  /** Query to calculate the travel time, CO2, and spend amount for the organization */
  planningInsights?: Maybe<PlanningInsightsResponse>;
  /** Generate a new Token based on Organization */
  recycleToken?: Maybe<AuthResponse>;
  /** Revoke the Token */
  revokeToken?: Maybe<Scalars['Boolean']>;
  /** Query to calcualte the maximum and minimum savings on travel time, spending and co2 for the organization */
  savingsOverview?: Maybe<SavingsOverviewResponse>;
  /** Set Default Organization */
  switchOrganization?: Maybe<AuthResponse>;
  /** Query to calculate the top analyzed destinations */
  topAnalyzedDestinations?: Maybe<TopAnalyzedDestinationsRepsonse>;
  /** Query to fetch transport data */
  transportData: TransportResponse;
  /** Query to fetch transport data for a provided destination */
  transportForDestination: Array<Maybe<TravelDetailsOrigin>>;
  travelPolicy: TravelPolicy;
  /** Query to get all meetings for a client */
  travelPolicyList: Array<TravelPolicy>;
  /** Get a travel profile */
  travelerProfile?: Maybe<TravelProfile>;
  /** Ger a single user */
  user: UserResponse;
  /** Get all users */
  userList?: Maybe<Array<User>>;
  /** Get a user employment organization details */
  userOrganizationDetail: UserOrganizationDetails;
  /** Query to fetch the user overview for an organization given a date interval */
  userOverviewList?: Maybe<UserOverviewPaginationResponse>;
};


export type QueryAccommodationArgs = {
  defaultId: Scalars['String'];
  destinationId: Scalars['String'];
};


export type QueryAccommodationListArgs = {
  data: AccommodationListInputs;
};


export type QueryActiveUsersArgs = {
  endDate?: InputMaybe<Scalars['Date']>;
  organizationId: Scalars['String'];
  startDate?: InputMaybe<Scalars['Date']>;
};


export type QueryAnalysedMeetingsArgs = {
  endDate?: InputMaybe<Scalars['Date']>;
  organizationId: Scalars['String'];
  startDate?: InputMaybe<Scalars['Date']>;
};


export type QueryAnalysisProgressArgs = {
  meetingId: Scalars['String'];
};


export type QueryAnalyzeDestinationArgs = {
  destinationId: Scalars['String'];
  meetingId: Scalars['String'];
  userId: Scalars['String'];
};


export type QueryAnalyzeMeetingArgs = {
  meetingId: Scalars['String'];
  userId: Scalars['String'];
};


export type QueryAnalyzeOriginArgs = {
  meetingId: Scalars['String'];
  originId: Scalars['String'];
  userId: Scalars['String'];
};


export type QueryAttendeeArgs = {
  id: Scalars['String'];
  meetingId?: InputMaybe<Scalars['String']>;
};


export type QueryAttendeeListArgs = {
  meetingId?: InputMaybe<Scalars['String']>;
  originId?: InputMaybe<Scalars['String']>;
};


export type QueryBasicMeetingStatisticsArgs = {
  endDate?: InputMaybe<Scalars['Date']>;
  meetingType?: InputMaybe<TypeMeeting>;
  organizationId: Scalars['String'];
  startDate?: InputMaybe<Scalars['Date']>;
};


export type QueryCalcRouteResultTravelArgs = {
  currentPage?: InputMaybe<Scalars['Int']>;
  perPage?: InputMaybe<Scalars['Int']>;
  travelRequest: TravelRequest;
};


export type QueryCalcRouteSummaryTravelArgs = {
  travelRequest: TravelRequest;
};


export type QueryCalcRouteTravelArgs = {
  travelRequest: TravelRequest;
};


export type QueryCountryListArgs = {
  codes?: InputMaybe<Scalars['String']>;
};


export type QueryCovidPolicyListArgs = {
  countries: Scalars['String'];
  endDate: Scalars['String'];
  includedOrigins?: InputMaybe<Scalars['String']>;
  startDate: Scalars['String'];
};


export type QueryCovidSpecsByOriginArgs = {
  origins: Scalars['String'];
  period: Scalars['String'];
  vaccinatedStatus: Scalars['Int'];
};


export type QueryDestinationArgs = {
  id: Scalars['String'];
};


export type QueryDestinationListArgs = {
  meetingId: Scalars['String'];
};


export type QueryDirectionsArgs = {
  directionRequest: DirectionRequest;
};


export type QueryFlightOptionDetailArgs = {
  inboundLegId: Scalars['String'];
  outboundLegId: Scalars['String'];
  queryId: Scalars['Int'];
};


export type QueryFlightOptionListArgs = {
  destinationId: Scalars['String'];
  filters?: InputMaybe<FlightDataFilters>;
  meetingId: Scalars['String'];
  originId: Scalars['String'];
  pagination?: InputMaybe<TravelPagination>;
};


export type QueryHoursSavedArgs = {
  endDate?: InputMaybe<Scalars['Date']>;
  meetingType?: InputMaybe<TypeMeeting>;
  organizationId: Scalars['String'];
  startDate?: InputMaybe<Scalars['Date']>;
};


export type QueryIsUserArgs = {
  email: Scalars['String'];
};


export type QueryLicenseArgs = {
  licenseId: Scalars['String'];
};


export type QueryLocationSuggestionListArgs = {
  locationsSearchRequest: LocationsSearchRequest;
};


export type QueryMeetingArgs = {
  meetingId: Scalars['String'];
};


export type QueryMeetingAnalysisArgs = {
  meetingId: Scalars['String'];
  organizationId?: InputMaybe<Scalars['String']>;
};


export type QueryMeetingAnalysisDetailArgs = {
  destinationId: Scalars['String'];
  meetingId: Scalars['String'];
  organizationId: Scalars['String'];
};


export type QueryMeetingListArgs = {
  currentPage?: InputMaybe<Scalars['Int']>;
  organizationId?: InputMaybe<Scalars['String']>;
  perPage?: InputMaybe<Scalars['Int']>;
  showAll?: InputMaybe<Scalars['Boolean']>;
  userId?: InputMaybe<Scalars['String']>;
};


export type QueryMeetingPolicyArgs = {
  meetingId: Scalars['String'];
  meetingPolicyId: Scalars['String'];
};


export type QueryMeetingSpaceArgs = {
  endDate?: InputMaybe<Scalars['String']>;
  endTime?: InputMaybe<Scalars['String']>;
  meetingSpaceId: Scalars['String'];
  organizationId: Scalars['String'];
  startDate?: InputMaybe<Scalars['String']>;
  startTime?: InputMaybe<Scalars['String']>;
};


export type QueryMeetingSpaceListArgs = {
  currentPage?: InputMaybe<Scalars['Int']>;
  organizationId: Scalars['String'];
  perPage?: InputMaybe<Scalars['Int']>;
  placeId?: InputMaybe<Scalars['String']>;
};


export type QueryMeetingTagArgs = {
  id: Scalars['String'];
};


export type QueryMeetingTagListArgs = {
  organizationId?: InputMaybe<Scalars['String']>;
};


export type QueryMeetingTemplateArgs = {
  templateId: Scalars['String'];
};


export type QueryMeetingTemplateListArgs = {
  currentPage?: InputMaybe<Scalars['Int']>;
  organizationId?: InputMaybe<Scalars['String']>;
  perPage?: InputMaybe<Scalars['Int']>;
  userId?: InputMaybe<Scalars['String']>;
};


export type QueryOfficeArgs = {
  officeId: Scalars['ID'];
};


export type QueryOfficeListArgs = {
  organizationId: Scalars['ID'];
  placeId?: InputMaybe<Scalars['String']>;
};


export type QueryOrganizationArgs = {
  organizationId: Scalars['String'];
};


export type QueryOrganizationCapArgs = {
  capId: Scalars['String'];
  organizationId: Scalars['String'];
};


export type QueryOrganizationCapListArgs = {
  organizationId: Scalars['String'];
};


export type QueryOrganizationLicenseArgs = {
  orgLicenseId: Scalars['String'];
};


export type QueryOrganizationListArgs = {
  userId?: InputMaybe<Scalars['String']>;
};


export type QueryOrganizationProviderListArgs = {
  email?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
};


export type QueryOrganizationResourceArgs = {
  organizationId: Scalars['String'];
  resourceId: Scalars['String'];
};


export type QueryOrganizationResourceListArgs = {
  organizationId: Scalars['String'];
  type: OrganizationResourceType;
};


export type QueryOrganizationSsoAuthArgs = {
  code: Scalars['SsoCode'];
};


export type QueryOriginArgs = {
  id: Scalars['String'];
};


export type QueryOriginListArgs = {
  meetingId: Scalars['String'];
};


export type QueryPlaceCityArgs = {
  placeCityRequest: PlaceCityRequest;
};


export type QueryPlaceCovidInfectionRateListArgs = {
  placeCovidInfectionRatesRequest: PlaceCovidRatesRequest;
};


export type QueryPlaceDetailArgs = {
  placeSearchRequest: PlaceDetailsSearchRequest;
};


export type QueryPlaceRestrictionListArgs = {
  placeRestrictionsRequest: PlaceRestrictionsRequest;
};


export type QueryPlaceRiskArgs = {
  placeRiskRequest: PlaceRiskRequest;
};


export type QueryPlaceSuggestionListArgs = {
  placeSuggestionsRequest: PlaceSuggestionsRequest;
};


export type QueryPlaceTimeZoneArgs = {
  placeTimeZoneRequest: PlaceTimeZoneRequest;
};


export type QueryPlaceVisaRequirementListArgs = {
  placeVisaRequirementsRequest: PlaceVisaRequirementsRequest;
};


export type QueryPlaceWeatherArgs = {
  placeWeatherRequest: PlaceWeatherRequest;
};


export type QueryPlanningInsightsArgs = {
  endDate?: InputMaybe<Scalars['Date']>;
  meetingType?: InputMaybe<TypeMeeting>;
  organizationId: Scalars['String'];
  startDate?: InputMaybe<Scalars['Date']>;
};


export type QueryRecycleTokenArgs = {
  refreshToken: Scalars['String'];
};


export type QueryRevokeTokenArgs = {
  userId: Scalars['String'];
};


export type QuerySavingsOverviewArgs = {
  endDate?: InputMaybe<Scalars['Date']>;
  meetingType?: InputMaybe<TypeMeeting>;
  organizationId: Scalars['String'];
  startDate?: InputMaybe<Scalars['Date']>;
};


export type QuerySwitchOrganizationArgs = {
  isDefault?: InputMaybe<Scalars['Boolean']>;
  organizationId: Scalars['String'];
};


export type QueryTopAnalyzedDestinationsArgs = {
  endDate?: InputMaybe<Scalars['Date']>;
  meetingType?: InputMaybe<TypeMeeting>;
  organizationId: Scalars['String'];
  startDate?: InputMaybe<Scalars['Date']>;
};


export type QueryTransportDataArgs = {
  transportRequest: TransportRequest;
};


export type QueryTransportForDestinationArgs = {
  travelRequestForDestination: TravelRequestForDestination;
};


export type QueryTravelPolicyArgs = {
  organizationId: Scalars['String'];
  travelPolicyId: Scalars['String'];
};


export type QueryTravelPolicyListArgs = {
  organizationId: Scalars['String'];
};


export type QueryTravelerProfileArgs = {
  profileId: Scalars['ID'];
};


export type QueryUserArgs = {
  userId: Scalars['String'];
};


export type QueryUserListArgs = {
  organizationId?: InputMaybe<Scalars['String']>;
};


export type QueryUserOrganizationDetailArgs = {
  organizationId: Scalars['String'];
  userId: Scalars['String'];
};


export type QueryUserOverviewListArgs = {
  currentPage?: InputMaybe<Scalars['Int']>;
  endDate?: InputMaybe<Scalars['Date']>;
  organizationId: Scalars['String'];
  perPage?: InputMaybe<Scalars['Int']>;
  startDate?: InputMaybe<Scalars['Date']>;
};

export type RequireQuarantine = {
  __typename?: 'RequireQuarantine';
  category: Scalars['String'];
  destination_country_code: Scalars['String'];
  document_type?: Maybe<Array<Scalars['String']>>;
  origin_country_code: Scalars['String'];
  priority?: Maybe<Scalars['Int']>;
  quarantine_days?: Maybe<Scalars['Int']>;
  requireQuarantine: Scalars['Int'];
  subcategory: Scalars['String'];
  tags: Array<Scalars['String']>;
};

export type RequireRestriction = {
  __typename?: 'RequireRestriction';
  category: Scalars['String'];
  destination_country_code: Scalars['String'];
  origin_country_code: Scalars['String'];
  priority?: Maybe<Scalars['Int']>;
  requireRestriction: Scalars['Int'];
  subcategory: Scalars['String'];
  tags: Array<Scalars['String']>;
};

export type RequireTest = {
  __typename?: 'RequireTest';
  category: Scalars['String'];
  destination_country_code: Scalars['String'];
  document_type?: Maybe<Array<Scalars['String']>>;
  origin_country_code: Scalars['String'];
  priority?: Maybe<Scalars['Int']>;
  requireTest: Scalars['Int'];
  subcategory: Scalars['String'];
  tags: Array<Scalars['String']>;
};

/** Reset Password Form */
export type ResetPasswordForm = {
  code: Scalars['String'];
  password: Scalars['String'];
};

/** Restriction Structure */
export type Restriction = {
  __typename?: 'Restriction';
  category: Scalars['String'];
  created_at?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  destination_country_code: Scalars['String'];
  destination_region_code: Scalars['String'];
  documentLinks?: Maybe<Array<Maybe<RestrictionDocumentLink>>>;
  end_date?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  included: Array<Maybe<Scalars['String']>>;
  last_checked_at?: Maybe<Scalars['String']>;
  last_updated_at?: Maybe<Scalars['String']>;
  origin_count: Scalars['Int'];
  region_included: Array<Maybe<Scalars['String']>>;
  severity: Scalars['Int'];
  source: CovidSource;
  start_date?: Maybe<Scalars['String']>;
  still_current_at?: Maybe<Scalars['String']>;
  subcategory: Scalars['String'];
  tags?: Maybe<Array<Maybe<Scalars['String']>>>;
  title: Scalars['String'];
  tt_updated_at: Scalars['String'];
};

export type RestrictionDocumentLink = {
  __typename?: 'RestrictionDocumentLink';
  title: Scalars['String'];
  type: Scalars['String'];
  url: Scalars['String'];
};

/** ENUM continaing possible values for Role Permissions */
export enum RolePermissions {
  Delete = 'DELETE',
  Read = 'READ',
  Write = 'WRITE'
}

/** ENUM continaing possible values for Roles */
export enum Roles {
  Admin = 'ADMIN',
  Guest = 'GUEST',
  User = 'USER'
}

/** Represents a segment of the route between two waypoints */
export type RouteSegment = {
  __typename?: 'RouteSegment';
  /** An array of LatLng coordinates for this segment in the route */
  coords: Array<Maybe<LatLng>>;
  /** The distance of this step in the route in Meters */
  distance: Scalars['Float'];
  /** Estimated total duration of this step in Minutes */
  duration: Scalars['Int'];
  /** An encoded representation of the combined coords in this step into a single polyline */
  encodedPolyline: Scalars['String'];
};

/** Summary of directions data */
export type RouteSummary = {
  __typename?: 'RouteSummary';
  /** Version of API requested */
  data?: Maybe<Scalars['String']>;
  /** Http status code of response */
  statusCode: Scalars['Int'];
  /** Route summary data */
  summary?: Maybe<RouteSummary>;
};

/** Response type for savings overview */
export type SavingsOverviewResponse = {
  __typename?: 'SavingsOverviewResponse';
  maxCo2Saving?: Maybe<Scalars['Float']>;
  maxCostSaving?: Maybe<Scalars['Float']>;
  maxTravelTimeSaving?: Maybe<Scalars['Float']>;
  minCo2Saving?: Maybe<Scalars['Float']>;
  minCostSaving?: Maybe<Scalars['Float']>;
  minTravelTimeSaving?: Maybe<Scalars['Float']>;
};

export enum SortListBy {
  Name = 'NAME',
  Price = 'PRICE',
  Rooms = 'ROOMS',
  Stars = 'STARS'
}

/** Authenticated Token Response */
export type TokenResponse = {
  __typename?: 'TokenResponse';
  accessToken?: Maybe<Scalars['String']>;
  refreshToken?: Maybe<Scalars['String']>;
};

/** Response type for the top analyzed destinations */
export type TopAnalyzedDestinationsRepsonse = {
  __typename?: 'TopAnalyzedDestinationsRepsonse';
  destinations?: Maybe<Array<AnalyzedDestination>>;
};

/** Response sent back to server looking to retreive transport json data */
export type TransportData = {
  __typename?: 'TransportData';
  /** Itineraries for transport */
  itineraries?: Maybe<Array<Maybe<TransportResponseItineraries>>>;
  /** Transport json legs */
  legs?: Maybe<Scalars['JSON']>;
  /** parameters provided in initial request */
  parameters?: Maybe<TransportResponseParameters>;
};

/** Request sent to server to retreive travel data for a certain destination */
export type TransportRequest = {
  /** Unique ID of client */
  clientId?: InputMaybe<Scalars['String']>;
  /** Unique ID of meeting destination */
  destinationId?: InputMaybe<Scalars['String']>;
  /** Unique ID of meeting taking place */
  meetingId?: InputMaybe<Scalars['String']>;
  /** Unique ID of meeting origin */
  originId?: InputMaybe<Scalars['String']>;
};

/** Response sent back to server looking to retreive transport data */
export type TransportResponse = {
  __typename?: 'TransportResponse';
  /** Name of API requested */
  api: Scalars['String'];
  /** Error from requested resource */
  error?: Maybe<TravelResponseResultError>;
  /** Result of requested resource */
  result?: Maybe<TransportData>;
  /** Version of API requested */
  version: Scalars['String'];
};

export type TransportResponseItineraries = {
  __typename?: 'TransportResponseItineraries';
  inboundLeg?: Maybe<Scalars['JSON']>;
  itineraryId?: Maybe<Scalars['Int']>;
  outboundLeg?: Maybe<Scalars['JSON']>;
  summary?: Maybe<TransportResponseItinerariesSummary>;
};

export type TransportResponseItinerariesSummary = {
  __typename?: 'TransportResponseItinerariesSummary';
  cabinClass?: Maybe<Scalars['String']>;
  duration?: Maybe<Scalars['Float']>;
  emissions?: Maybe<Scalars['Float']>;
  inboundArrival?: Maybe<Scalars['String']>;
  inboundDeparture?: Maybe<Scalars['String']>;
  inboundInPolicy?: Maybe<Scalars['Boolean']>;
  inboundInPolicyDetails?: Maybe<Scalars['JSON']>;
  outboundArrival?: Maybe<Scalars['String']>;
  outboundDeparture?: Maybe<Scalars['String']>;
  outboundInPolicy?: Maybe<Scalars['Boolean']>;
  outboundInPolicyDetails?: Maybe<Scalars['JSON']>;
  price?: Maybe<Scalars['Float']>;
  service?: Maybe<Scalars['String']>;
  stops?: Maybe<Scalars['Int']>;
  subService?: Maybe<Scalars['String']>;
  supplier?: Maybe<Scalars['String']>;
};

export type TransportResponseParameters = {
  __typename?: 'TransportResponseParameters';
  destination?: Maybe<Scalars['String']>;
  destinationTimezone?: Maybe<Scalars['Int']>;
  origin?: Maybe<Scalars['String']>;
  originTimezone?: Maybe<Scalars['Int']>;
  policy?: Maybe<TransportResponseParametersPolicy>;
};

export type TransportResponseParametersPolicy = {
  __typename?: 'TransportResponseParametersPolicy';
  flightMaxLegSegmentDuration?: Maybe<Scalars['Float']>;
  flightMaxTotalFlightLegDuration?: Maybe<Scalars['Float']>;
};

export type TravelDetailsOrigin = {
  __typename?: 'TravelDetailsOrigin';
  cabin_class?: Maybe<Scalars['String']>;
  cost?: Maybe<Scalars['Float']>;
  in_policy?: Maybe<Scalars['Boolean']>;
  inboundLeg?: Maybe<Array<Maybe<TravelDetailsOriginLegDetails>>>;
  inbound_arrival?: Maybe<Scalars['String']>;
  inbound_carrier_country?: Maybe<Scalars['String']>;
  inbound_carrier_img?: Maybe<Scalars['String']>;
  inbound_carrier_name?: Maybe<Scalars['String']>;
  inbound_departure?: Maybe<Scalars['String']>;
  inbound_duration?: Maybe<Scalars['Int']>;
  inbound_emissions?: Maybe<Scalars['Int']>;
  inbound_flight_duration?: Maybe<Scalars['Int']>;
  inbound_in_policy?: Maybe<Scalars['Boolean']>;
  inbound_leg_id?: Maybe<Scalars['String']>;
  inbound_legs?: Maybe<Scalars['Int']>;
  inbound_max_stop_risk?: Maybe<Scalars['Float']>;
  inbound_stops?: Maybe<Scalars['Int']>;
  itinerary_id?: Maybe<Scalars['Int']>;
  max_stop_risk?: Maybe<Scalars['Float']>;
  originCityName?: Maybe<Scalars['String']>;
  originCountryId?: Maybe<Scalars['String']>;
  originCountryName?: Maybe<Scalars['String']>;
  originCountryflag?: Maybe<Scalars['String']>;
  originId: Scalars['String'];
  outboundLeg?: Maybe<Array<Maybe<TravelDetailsOriginLegDetails>>>;
  outbound_arrival?: Maybe<Scalars['String']>;
  outbound_carrier_country?: Maybe<Scalars['String']>;
  outbound_carrier_img?: Maybe<Scalars['String']>;
  outbound_carrier_name?: Maybe<Scalars['String']>;
  outbound_departure?: Maybe<Scalars['String']>;
  outbound_duration?: Maybe<Scalars['Int']>;
  outbound_emissions?: Maybe<Scalars['Int']>;
  outbound_flight_duration?: Maybe<Scalars['Int']>;
  outbound_in_policy?: Maybe<Scalars['Boolean']>;
  outbound_leg_id?: Maybe<Scalars['String']>;
  outbound_legs?: Maybe<Scalars['Int']>;
  outbound_max_stop_risk?: Maybe<Scalars['Float']>;
  outbound_stops?: Maybe<Scalars['Int']>;
  query_id?: Maybe<Scalars['Int']>;
  service?: Maybe<Scalars['String']>;
  sub_service?: Maybe<Scalars['String']>;
  supplier?: Maybe<Scalars['String']>;
  timezoneDiff?: Maybe<Scalars['Int']>;
  total_duration?: Maybe<Scalars['Int']>;
  total_emissions?: Maybe<Scalars['Int']>;
  total_legs?: Maybe<Scalars['Int']>;
  total_stops?: Maybe<Scalars['Int']>;
  travellerCount?: Maybe<Scalars['Int']>;
};

export type TravelDetailsOriginLegDetails = {
  __typename?: 'TravelDetailsOriginLegDetails';
  arrival?: Maybe<Scalars['String']>;
  carrier?: Maybe<Scalars['Int']>;
  carrier_code?: Maybe<Scalars['String']>;
  carrier_img?: Maybe<Scalars['String']>;
  carrier_name?: Maybe<Scalars['String']>;
  departure?: Maybe<Scalars['String']>;
  destinationAirport?: Maybe<Scalars['String']>;
  destinationAirportName?: Maybe<Scalars['String']>;
  destinationCityCode?: Maybe<Scalars['String']>;
  destinationCityName?: Maybe<Scalars['String']>;
  destinationCountryCode?: Maybe<Scalars['String']>;
  destinationCountryName?: Maybe<Scalars['String']>;
  destination_code?: Maybe<Scalars['String']>;
  directionality?: Maybe<Scalars['String']>;
  distance?: Maybe<Scalars['Int']>;
  duration?: Maybe<Scalars['Int']>;
  emissions?: Maybe<Scalars['Int']>;
  flight_number?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  journey_mode?: Maybe<Scalars['String']>;
  leg_id?: Maybe<Scalars['String']>;
  operating_carrier?: Maybe<Scalars['Int']>;
  operating_carrier_code?: Maybe<Scalars['String']>;
  originAirport?: Maybe<Scalars['String']>;
  originAirportName?: Maybe<Scalars['String']>;
  originCityCode?: Maybe<Scalars['String']>;
  originCityName?: Maybe<Scalars['String']>;
  originCountryCode?: Maybe<Scalars['String']>;
  originCountryName?: Maybe<Scalars['String']>;
  origin_code?: Maybe<Scalars['String']>;
  query_id?: Maybe<Scalars['Int']>;
  segment_id?: Maybe<Scalars['String']>;
  stop_duration?: Maybe<Scalars['Int']>;
};

export type TravelPagination = {
  currentPage?: InputMaybe<Scalars['Int']>;
  perPage?: InputMaybe<Scalars['Int']>;
};

export type TravelPolicy = {
  __typename?: 'TravelPolicy';
  flight_allowed_stops_more_than_min?: Maybe<Scalars['Int']>;
  flight_avoid_carriers?: Maybe<Array<Scalars['String']>>;
  flight_cabin_class?: Maybe<Array<FlightCabinClass>>;
  flight_low_cost_airlines: Scalars['Boolean'];
  flight_max_leg_segment_duration?: Maybe<Scalars['Int']>;
  flight_max_leg_segment_duration_business?: Maybe<Scalars['Int']>;
  flight_max_leg_segment_duration_check?: Maybe<Scalars['Int']>;
  flight_max_leg_segment_duration_classes?: Maybe<Array<Scalars['Int']>>;
  flight_max_leg_segment_duration_economy?: Maybe<Scalars['Int']>;
  flight_max_leg_segment_duration_first_class?: Maybe<Scalars['Int']>;
  flight_max_leg_segment_duration_premium_economy?: Maybe<Scalars['Int']>;
  flight_max_stops?: Maybe<Scalars['Int']>;
  flight_max_total_flight_leg_duration?: Maybe<Scalars['Int']>;
  flight_max_travel_duration?: Maybe<Scalars['Int']>;
  flight_preferred_carriers?: Maybe<Array<Scalars['String']>>;
  flight_providers: Array<Scalars['String']>;
  hotel_allow_only_green: Scalars['Boolean'];
  hotel_allow_only_preferred: Scalars['Boolean'];
  hotel_allow_room_sharing: Scalars['Boolean'];
  hotel_apply_cap: Scalars['Boolean'];
  hotel_minimum_rating: Scalars['Int'];
  hotel_number_to_suggest: Scalars['Int'];
  hotel_optimization: Array<Scalars['String']>;
  hotel_residents_require_accommodation: Scalars['Boolean'];
  hotel_star_rating: Array<Scalars['Int']>;
  hotel_suggest_radius: Scalars['Int'];
  id: Scalars['String'];
  isDefault?: Maybe<Scalars['Boolean']>;
  name: Scalars['String'];
  organization_id: Scalars['String'];
  status?: Maybe<PolicyStatus>;
  transport_consider_alternative_max_distance: Scalars['Int'];
  transport_consider_flying_min_distance: Scalars['Int'];
  transport_modes: Array<Scalars['String']>;
};

export type TravelPolicyInput = {
  flight_allowed_stops_more_than_min?: InputMaybe<Scalars['Int']>;
  flight_avoid_carriers?: InputMaybe<Array<Scalars['String']>>;
  flight_cabin_class?: InputMaybe<Array<FlightCabinClass>>;
  flight_low_cost_airlines?: InputMaybe<Scalars['Boolean']>;
  flight_max_leg_segment_duration?: InputMaybe<Scalars['Int']>;
  flight_max_leg_segment_duration_business?: InputMaybe<Scalars['Int']>;
  flight_max_leg_segment_duration_check?: InputMaybe<Scalars['Int']>;
  flight_max_leg_segment_duration_classes?: InputMaybe<Array<Scalars['Int']>>;
  flight_max_leg_segment_duration_economy?: InputMaybe<Scalars['Int']>;
  flight_max_leg_segment_duration_first_class?: InputMaybe<Scalars['Int']>;
  flight_max_leg_segment_duration_premium_economy?: InputMaybe<Scalars['Int']>;
  flight_max_stops?: InputMaybe<Scalars['Int']>;
  flight_max_total_flight_leg_duration?: InputMaybe<Scalars['Int']>;
  flight_max_travel_duration?: InputMaybe<Scalars['Int']>;
  flight_preferred_carriers?: InputMaybe<Array<Scalars['String']>>;
  flight_providers?: InputMaybe<Array<Scalars['String']>>;
  hotel_allow_only_green?: InputMaybe<Scalars['Boolean']>;
  hotel_allow_only_preferred?: InputMaybe<Scalars['Boolean']>;
  hotel_allow_room_sharing?: InputMaybe<Scalars['Boolean']>;
  hotel_apply_cap?: InputMaybe<Scalars['Boolean']>;
  hotel_minimum_rating?: InputMaybe<Scalars['Int']>;
  hotel_number_to_suggest?: InputMaybe<Scalars['Int']>;
  hotel_optimization?: InputMaybe<Array<Scalars['String']>>;
  hotel_residents_require_accommodation?: InputMaybe<Scalars['Boolean']>;
  hotel_star_rating?: InputMaybe<Array<Scalars['Int']>>;
  hotel_suggest_radius?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['String']>;
  isDefault?: InputMaybe<Scalars['Boolean']>;
  name: Scalars['String'];
  organization_id: Scalars['String'];
  status?: InputMaybe<PolicyStatus>;
  transport_consider_alternative_max_distance?: InputMaybe<Scalars['Int']>;
  transport_consider_flying_min_distance?: InputMaybe<Scalars['Int']>;
  transport_modes?: InputMaybe<Array<Scalars['String']>>;
};

export type TravelProfile = {
  __typename?: 'TravelProfile';
  addresses?: Maybe<Array<Maybe<TravelProfileAddress>>>;
  birth_date?: Maybe<Scalars['Date']>;
  covid_vaccination_date?: Maybe<Scalars['Date']>;
  covid_vaccination_status?: Maybe<VaccinationStatus>;
  covid_vaccination_type?: Maybe<VaccinationType>;
  id: Scalars['ID'];
  nationality?: Maybe<Scalars['String']>;
  passports?: Maybe<Array<Maybe<TravelProfilePassport>>>;
  user?: Maybe<User>;
  user_id?: Maybe<Scalars['String']>;
};

export type TravelProfileAddress = {
  __typename?: 'TravelProfileAddress';
  address_1?: Maybe<Scalars['String']>;
  address_2?: Maybe<Scalars['String']>;
  city_id?: Maybe<Scalars['String']>;
  city_lnglat?: Maybe<Scalars['Point']>;
  city_name?: Maybe<Scalars['String']>;
  country_id?: Maybe<Scalars['String']>;
  country_lnglat?: Maybe<Scalars['Point']>;
  country_name?: Maybe<Scalars['String']>;
  default?: Maybe<Scalars['Boolean']>;
  id: Scalars['ID'];
  state_id?: Maybe<Scalars['String']>;
  state_name?: Maybe<Scalars['String']>;
  suburb_id?: Maybe<Scalars['String']>;
  suburb_name?: Maybe<Scalars['String']>;
  travel_profile_id?: Maybe<Scalars['String']>;
};

export type TravelProfileInput = {
  birth_date?: InputMaybe<Scalars['Date']>;
  covid_vaccination_date?: InputMaybe<Scalars['Date']>;
  covid_vaccination_status?: InputMaybe<VaccinationStatus>;
  covid_vaccination_type?: InputMaybe<VaccinationType>;
  id?: InputMaybe<Scalars['ID']>;
  nationality?: InputMaybe<Scalars['String']>;
  user_id: Scalars['String'];
};

export type TravelProfilePassport = {
  __typename?: 'TravelProfilePassport';
  expiry_date?: Maybe<Scalars['Date']>;
  id: Scalars['ID'];
  issuing_authority?: Maybe<Scalars['String']>;
  issuing_country?: Maybe<Scalars['String']>;
  issuing_date?: Maybe<Scalars['Date']>;
  number?: Maybe<Scalars['String']>;
  travel_profile_id?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
};

/** Provider of travel related data */
export type TravelProvider = {
  /** Services required of the travel provider */
  directionService?: InputMaybe<TravelProviderDirectionService>;
  /** Services required of the travel provider */
  flightService?: InputMaybe<TravelProviderFlightService>;
  /** Name of the service provider */
  name: Scalars['String'];
};

/** Direction service config for travel provider */
export type TravelProviderDirectionService = {
  /** Modes of transport being queried */
  mode: Array<InputMaybe<Scalars['String']>>;
  /** Public transit type if transit mode specified */
  transitMode?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

/** Flight service config for travel provider */
export type TravelProviderFlightService = {
  /** The cabin class of the requested flight */
  cabinClass: Array<InputMaybe<Scalars['String']>>;
};

/** Request sent to server to retreive travel data */
export type TravelRequest = {
  /** Unique ID of client */
  clientId: Scalars['String'];
  /** Unique ID of meeting destination */
  destinationId: Scalars['String'];
  /** Unique ID of meeting taking place */
  meetingId: Scalars['String'];
  /** Unique ID of meeting origin */
  originId: Scalars['String'];
  /** Travel providers to be called by request */
  travelProvider?: InputMaybe<Array<InputMaybe<TravelProvider>>>;
};

/** Request sent to server to retreive travel data for a certain destination */
export type TravelRequestForDestination = {
  /** Unique ID of meeting destination */
  destinationID: Scalars['String'];
  /** Unique ID of meeting taking place */
  meetingID: Scalars['String'];
  /** List of origins to loop over */
  origins: Array<InputMaybe<Scalars['JSON']>>;
};

export type TravelRequestOrigin = {
  countryId: Scalars['String'];
  countryName: Scalars['String'];
  id: Scalars['String'];
  name: Scalars['String'];
  timeZone?: InputMaybe<Scalars['Int']>;
  travellerCount: Scalars['Int'];
};

/** Response sent back to server looking to retreive directions */
export type TravelResponse = {
  __typename?: 'TravelResponse';
  /** Name of API requested */
  api: Scalars['String'];
  /** Error from requested resource */
  error?: Maybe<TravelResponseResultError>;
  /** Result of requested resource */
  result?: Maybe<TravelResponseResult>;
  /** Version of API requested */
  version: Scalars['String'];
};

/** Response sent back to server looking to retreive directions */
export type TravelResponseResult = {
  __typename?: 'TravelResponseResult';
  /** Version of API requested */
  data?: Maybe<Scalars['String']>;
  /** Http status code of response */
  statusCode: Scalars['Int'];
  /** Route summary data */
  summary?: Maybe<RouteSummary>;
};

/** Response sent back to server looking to retreive directions */
export type TravelResponseResultError = {
  __typename?: 'TravelResponseResultError';
  /** Message to provide insight into error */
  message: Scalars['String'];
  /** Stack trace of error that occured */
  stackTrace?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Status string */
  status: Scalars['String'];
  /** Http status code of response */
  statusCode: Scalars['Int'];
  /** Http error code of response */
  trace: Scalars['Int'];
};

export type TravelResult = {
  __typename?: 'TravelResult';
  data?: Maybe<TravelResultData>;
  statusCode?: Maybe<Scalars['Int']>;
};

export type TravelResultData = {
  __typename?: 'TravelResultData';
  itineraries?: Maybe<Array<Maybe<TravelResultItineraries>>>;
  legs?: Maybe<TravelResultDataLegs>;
  parameters?: Maybe<TravelResultParameters>;
};

export type TravelResultDataLegs = {
  __typename?: 'TravelResultDataLegs';
  data?: Maybe<Scalars['JSON']>;
  page?: Maybe<Page>;
};

export type TravelResultItineraries = {
  __typename?: 'TravelResultItineraries';
  inbound_leg?: Maybe<TravelResultItinerariesLeg>;
  itinerary_id?: Maybe<Scalars['Int']>;
  outbound_leg?: Maybe<TravelResultItinerariesLeg>;
  summary?: Maybe<TravelResultItinerariesSummary>;
};

export type TravelResultItinerariesLeg = {
  __typename?: 'TravelResultItinerariesLeg';
  duration?: Maybe<Scalars['Float']>;
  id?: Maybe<Scalars['String']>;
  stops?: Maybe<Scalars['Int']>;
};

export type TravelResultItinerariesSummary = {
  __typename?: 'TravelResultItinerariesSummary';
  cabin_class?: Maybe<Scalars['String']>;
  duration?: Maybe<Scalars['Float']>;
  emissions?: Maybe<Scalars['Float']>;
  inbound_arrival?: Maybe<Scalars['String']>;
  inbound_departure?: Maybe<Scalars['String']>;
  inbound_in_policy?: Maybe<Scalars['Int']>;
  inbound_in_policy_details?: Maybe<Scalars['String']>;
  outbound_arrival?: Maybe<Scalars['String']>;
  outbound_departure?: Maybe<Scalars['String']>;
  outbound_in_policy?: Maybe<Scalars['Int']>;
  outbound_in_policy_details?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  service?: Maybe<Scalars['String']>;
  stops?: Maybe<Scalars['Int']>;
  sub_service?: Maybe<Scalars['String']>;
  supplier?: Maybe<Scalars['String']>;
  troopScore?: Maybe<Scalars['Float']>;
};

export type TravelResultParameters = {
  __typename?: 'TravelResultParameters';
  destination?: Maybe<Scalars['String']>;
  destination_timezone?: Maybe<Scalars['Float']>;
  origin?: Maybe<Scalars['String']>;
  origin_timezone?: Maybe<Scalars['Float']>;
  policy?: Maybe<TravelResultParametersPolicy>;
};

export type TravelResultParametersPolicy = {
  __typename?: 'TravelResultParametersPolicy';
  flight_max_leg_segment_duration?: Maybe<Scalars['Float']>;
  flight_max_total_flight_leg_duration?: Maybe<Scalars['Float']>;
};

export type TravelResultResponse = {
  __typename?: 'TravelResultResponse';
  /** Name of API requested */
  api: Scalars['String'];
  /** Error from requested resource */
  error?: Maybe<Scalars['JSON']>;
  /** Result of requested resource */
  result?: Maybe<TravelResult>;
  /** Version of API requested */
  version: Scalars['String'];
};

/** ENUM containing possible values for the Meeting.type field */
export enum TypeMeeting {
  External = 'EXTERNAL',
  Internal = 'INTERNAL'
}

/** User Schema for Signup via Email and Password */
export type User = {
  __typename?: 'User';
  created?: Maybe<Scalars['String']>;
  designation?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  email_verified?: Maybe<Scalars['Boolean']>;
  fb_payload?: Maybe<Scalars['JSON']>;
  first_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['ID']>;
  last_name?: Maybe<Scalars['String']>;
  organizations?: Maybe<Array<Maybe<Organization>>>;
  provider?: Maybe<Array<Scalars['String']>>;
  roles?: Maybe<Array<Maybe<UserOrganizationRoles>>>;
  travelProfile?: Maybe<TravelProfile>;
  updated?: Maybe<Scalars['String']>;
};

/** User Check Response */
export type UserCheckResponse = {
  __typename?: 'UserCheckResponse';
  code?: Maybe<Scalars['Int']>;
  emailVerified?: Maybe<Scalars['Boolean']>;
  exist: Scalars['Boolean'];
  message?: Maybe<Scalars['String']>;
  organizations?: Maybe<Array<Maybe<Organization>>>;
  provider?: Maybe<Array<Maybe<Scalars['String']>>>;
  success?: Maybe<Scalars['Boolean']>;
};

/** User Email Form */
export type UserEmailForm = {
  email: Scalars['String'];
};

/** (Create/Update) User */
export type UserForm = {
  created?: InputMaybe<Scalars['String']>;
  designation?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  fb_payload?: InputMaybe<Scalars['JSON']>;
  first_name?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  last_name?: InputMaybe<Scalars['String']>;
  organization_id?: InputMaybe<Scalars['String']>;
  provider?: InputMaybe<Array<Scalars['String']>>;
};

/** User Details for Organization */
export type UserOrganizationDetails = {
  __typename?: 'UserOrganizationDetails';
  added?: Maybe<Scalars['Date']>;
  id?: Maybe<Scalars['String']>;
  job_role?: Maybe<Scalars['String']>;
  office?: Maybe<Office>;
  office_id?: Maybe<Scalars['String']>;
  organization_id: Scalars['String'];
  status?: Maybe<UserOrganizationStatus>;
  user_id: Scalars['String'];
};

/** User Details for Organization */
export type UserOrganizationForm = {
  email?: InputMaybe<Scalars['String']>;
  job_role?: InputMaybe<Scalars['String']>;
  office_id?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<UserOrganizationStatus>;
};

/** The Roles the user has in an Organization */
export type UserOrganizationRoles = {
  __typename?: 'UserOrganizationRoles';
  organizationId: Scalars['ID'];
  permissions?: Maybe<Array<Maybe<RolePermissions>>>;
  roles?: Maybe<Array<Maybe<Roles>>>;
};

/** ENUM continaing possible values for User Organization Status */
export enum UserOrganizationStatus {
  Active = 'ACTIVE',
  Archived = 'ARCHIVED',
  Pending = 'PENDING'
}

/** Type to display an organization's users and their usage statistics */
export type UserOverview = {
  __typename?: 'UserOverview';
  analyzedMeetings?: Maybe<Scalars['Int']>;
  createdMeetings?: Maybe<Scalars['Int']>;
  user?: Maybe<User>;
};

/** Add comments and descriptions to all types and queries */
export type UserOverviewPaginationResponse = {
  __typename?: 'UserOverviewPaginationResponse';
  data?: Maybe<Array<Maybe<UserOverview>>>;
  page?: Maybe<Page>;
};

/** User Response */
export type UserResponse = {
  __typename?: 'UserResponse';
  code?: Maybe<Scalars['Int']>;
  data?: Maybe<User>;
  message?: Maybe<Scalars['String']>;
  success?: Maybe<Scalars['Boolean']>;
};

export type UserRoleInput = {
  roleId: Scalars['String'];
  userId: Scalars['String'];
};

export type UserRoleResponse = {
  __typename?: 'UserRoleResponse';
  role_id?: Maybe<Scalars['String']>;
  user_id?: Maybe<Scalars['String']>;
};

/** User Verify Email Form */
export type UserVerifyEmailForm = {
  code: Scalars['String'];
};

/** ENUM continaing status of Vaccination */
export enum VaccinationStatus {
  Fully = 'FULLY',
  Partial = 'PARTIAL',
  Unvaccinated = 'UNVACCINATED'
}

/** ENUM continaing type of Vaccination */
export enum VaccinationType {
  Abdala = 'ABDALA',
  Convidecia = 'CONVIDECIA',
  Coronavac = 'CORONAVAC',
  Covaxin = 'COVAXIN',
  Epivaccorona = 'EPIVACCORONA',
  Janssen = 'JANSSEN',
  Moderna = 'MODERNA',
  Novavax = 'NOVAVAX',
  OxfordAstrazeneca = 'OXFORD_ASTRAZENECA',
  PfizerBiontech = 'PFIZER_BIONTECH',
  SinopharmBibp = 'SINOPHARM_BIBP',
  SinopharmWibp = 'SINOPHARM_WIBP',
  Soberana_02 = 'SOBERANA_02',
  SputnikLight = 'SPUTNIK_LIGHT',
  SputnikV = 'SPUTNIK_V',
  Zifivax = 'ZIFIVAX',
  ZycovD = 'ZYCOV_D'
}

/** A point of interest through which the route must pass */
export type Waypoint = {
  __typename?: 'Waypoint';
  /** Coordinates of the waypoint */
  coords: LatLng;
  /** The location type of the waypoint, e.g Hotel, Office or Airport */
  type: Scalars['String'];
};

/** A point of interest through which the route must pass */
export type WaypointInput = {
  /** Coordinates of the waypoint */
  coords: LatLngInput;
  /** The location type of the waypoint, e.g Hotel, Office or Airport */
  type: Scalars['String'];
};

export type SetAccommodationMutationVariables = Exact<{
  destinationId: Scalars['String'];
  defaultId: Scalars['String'];
}>;


export type SetAccommodationMutation = { __typename?: 'Mutation', setAccommodation?: { __typename?: 'Accommodation', id: string, selected?: boolean | null } | null };

export type AccommodationFilterListQueryVariables = Exact<{
  data: AccommodationListInputs;
}>;


export type AccommodationFilterListQuery = { __typename?: 'Query', accommodationList: { __typename?: 'AccommodationPaginationResponse', page?: { __typename?: 'PageFiltering', total?: number | null, totalAfterFiltering?: number | null, perPage?: number | null, lastPage?: number | null, currentPage?: number | null, from?: number | null, to?: number | null, offset?: number | null } | null, data?: Array<{ __typename?: 'Accommodation', title?: string | null, number_of_rooms?: number | null, star?: number | null, default_price?: number | null, green_status?: boolean | null, recommended?: boolean | null, distance_from_search?: string | null }> | null } };

export type AccommodationListQueryVariables = Exact<{
  data: AccommodationListInputs;
}>;


export type AccommodationListQuery = { __typename?: 'Query', accommodationList: { __typename?: 'AccommodationPaginationResponse', page?: { __typename?: 'PageFiltering', total?: number | null, perPage?: number | null, offset?: number | null, to?: number | null, lastPage?: number | null, currentPage?: number | null, from?: number | null, totalAfterFiltering?: number | null } | null, data?: Array<{ __typename?: 'Accommodation', id: string, title?: string | null, provider_id?: string | null, provider?: string | null, default?: boolean | null, chain?: string | null, number_of_rooms?: number | null, lnglat?: any | null, address?: string | null, link?: string | null, thumbnail?: string | null, star?: number | null, distance_from_search?: string | null, green_status?: boolean | null, recommended?: boolean | null, default_id?: string | null, default_price?: number | null, selected?: boolean | null, destination_id?: string | null, price: Array<{ __typename?: 'AccommodatoinProviderPrice', provider_id: string, property_id?: string | null, price?: number | null, default?: boolean | null }> }> | null } };

export type SetAttendeeMutationVariables = Exact<{
  attendee: AttendeeInput;
}>;


export type SetAttendeeMutation = { __typename?: 'Mutation', setAttendee: { __typename?: 'AttendeeResponse', code?: number | null, message?: string | null, success?: boolean | null } };

export type CountryListQueryVariables = Exact<{
  codes?: InputMaybe<Scalars['String']>;
}>;


export type CountryListQuery = { __typename?: 'Query', countryList: Array<{ __typename?: 'Country', id: string, name: string, risk?: { __typename?: 'CountryRisk', risk_score?: number | null, risk_source?: string | null, risk_source_link?: string | null, risk_status?: string | null, risk_details?: Array<{ __typename?: 'CountryRiskDetails', source?: string | null, source_iso_alpha2?: string | null, source_link?: string | null, score?: number | null, coverage?: string | null } | null> | null } | null }> };

export type SetDestinationMutationVariables = Exact<{
  destination: DestinationInput;
}>;


export type SetDestinationMutation = { __typename?: 'Mutation', setDestination: { __typename?: 'DestinationResponse', code?: number | null, message?: string | null, success?: boolean | null } };

export type AnalysisProgressQueryVariables = Exact<{
  meetingId: Scalars['String'];
}>;


export type AnalysisProgressQuery = { __typename?: 'Query', analysisProgress: { __typename?: 'AnalysisProgress', analysis_status: AnalysisStatus, meeting_id: string, destinations: Array<{ __typename?: 'AnalysisProgressDestination', destination_id: string, analysis_status: AnalysisStatus, accommodation_status: { __typename?: 'AnalysisProgressDestinationsAccommodation', analysis_status: AnalysisStatus, data: Array<{ __typename?: 'AnalysisProgressStatusItem', analysis_status: AnalysisStatus, type: string }> }, flights_status: { __typename?: 'AnalysisProgressDestinationsFlights', analysis_status: AnalysisStatus, data: Array<{ __typename?: 'AnalysisProgressStatusItem', analysis_status: AnalysisStatus, type: string }> } }> } };

export type AnalyzeDestinationQueryVariables = Exact<{
  meetingId: Scalars['String'];
  destinationId: Scalars['String'];
  userId: Scalars['String'];
}>;


export type AnalyzeDestinationQuery = { __typename?: 'Query', analyzeDestination: { __typename?: 'AnalyzeDestination', code?: number | null, success?: boolean | null, message?: string | null, workflows?: Array<string | null> | null, args?: string | null } };

export type AnalyzeOriginQueryVariables = Exact<{
  meetingId: Scalars['String'];
  userId: Scalars['String'];
  originId: Scalars['String'];
}>;


export type AnalyzeOriginQuery = { __typename?: 'Query', analyzeOrigin: { __typename?: 'AnalyzeDestination', args?: string | null, code?: number | null, message?: string | null, success?: boolean | null, workflows?: Array<string | null> | null } };

export type DestinationDetailQueryVariables = Exact<{
  placeSearchRequest: PlaceDetailsSearchRequest;
}>;


export type DestinationDetailQuery = { __typename?: 'Query', placeDetail: { __typename?: 'PlaceDetailsSearchResponse', visa_requirements?: Array<{ __typename?: 'PlaceDetailsVisaRequirementsResponse', visa_required?: boolean | null, visa_requirement?: string | null, traveller_count?: number | null } | null> | null, covid_rates?: Array<{ __typename?: 'PlaceDetailsCovidInfectionRatesResponse', infection_rate?: number | null } | null> | null } };

export type DestinationListQueryVariables = Exact<{
  meetingId: Scalars['String'];
}>;


export type DestinationListQuery = { __typename?: 'Query', destinationList?: Array<{ __typename?: 'Destination', address?: string | null, airport_id?: string | null, airport_lnglat?: any | null, airport_name?: string | null, analysis_date?: any | null, analysis_status?: AnalysisStatus | null, city_id: string, city_sygic_image_id?: string | null, city_lnglat?: any | null, city_name?: string | null, baseline?: boolean | null, country_id: string, country_lnglat?: any | null, country_name?: string | null, id: string, lnglat?: any | null, meeting_id: string, places_id?: string | null, analysis_user?: { __typename?: 'User', id?: string | null } | null, risk?: { __typename?: 'PlaceDetailsTravelAdvisoryRiskResponse', risk?: number | null } | null, weather?: { __typename?: 'PlaceDetailsWeatherResponse', temp?: number | null } | null }> | null };

export type LocationSuggestionListQueryVariables = Exact<{
  locationsSearchRequest: LocationsSearchRequest;
}>;


export type LocationSuggestionListQuery = { __typename?: 'Query', locationSuggestionList: { __typename?: 'LocationsSearchResponse', cities?: { __typename?: 'LocationsSearchResponseCity', data?: Array<{ __typename?: 'LocationsSearchResponseItem', id: string, type: LocationSearchSuggestionType, title: string, sub_title: string, address?: string | null, country_id: string, country_name: string, city_id: string, city_name: string, city_iata: string, city_location: any, city_lnglat: any, city_sygic_image_id?: string | null, airport_id: string, airport_name: string, location: any, distance: number, icon: { __typename?: 'LocationsSearchResponseIcon', type: string, name: string } } | null> | null } | null, airports?: { __typename?: 'LocationsSearchResponseAirport', data?: Array<{ __typename?: 'LocationsSearchResponseItem', id: string, type: LocationSearchSuggestionType, title: string, sub_title: string, address?: string | null, country_id: string, country_name: string, city_id: string, city_name: string, city_iata: string, city_location: any, city_lnglat: any, city_sygic_image_id?: string | null, airport_id: string, airport_name: string, location: any, distance: number, icon: { __typename?: 'LocationsSearchResponseIcon', type: string, name: string } } | null> | null } | null, offices?: { __typename?: 'LocationsSearchResponseOffice', data?: Array<{ __typename?: 'LocationsSearchResponseItem', id: string, type: LocationSearchSuggestionType, title: string, sub_title: string, address?: string | null, country_id: string, country_name: string, city_id: string, city_name: string, city_iata: string, city_location: any, city_lnglat: any, city_sygic_image_id?: string | null, airport_id: string, airport_name: string, location: any, distance: number, icon: { __typename?: 'LocationsSearchResponseIcon', type: string, name: string } } | null> | null } | null } };

export type MeetingAnalysisQueryVariables = Exact<{
  meetingId: Scalars['String'];
  organizationId?: InputMaybe<Scalars['String']>;
}>;


export type MeetingAnalysisQuery = { __typename?: 'Query', meetingAnalysis: { __typename?: 'MeetingAnalysis', analysis_date?: any | null, number_of_travellers: number, meeting: { __typename?: 'Meeting', analysis_date?: any | null, created?: string | null, end_date?: any | null, end_time?: string | null, id?: string | null, meeting_policy_id?: string | null, name?: string | null, organization_id?: string | null, start_date?: any | null, start_time?: string | null, updated?: string | null, analysis_user?: { __typename?: 'User', email?: string | null, id?: string | null, first_name?: string | null, last_name?: string | null } | null, owner?: { __typename?: 'User', id?: string | null, first_name?: string | null, last_name?: string | null, email?: string | null } | null }, origins?: Array<{ __typename?: 'Origin', address: string, airport_id?: string | null, airport_lnglat?: any | null, airport_name?: string | null, city_id?: string | null, city_lnglat?: any | null, city_name?: string | null, country_id: string, country_lnglat?: any | null, country_name?: string | null, id: string, lnglat?: any | null, meeting_id: string, status?: OriginStatus | null, attendees?: { __typename?: 'OriginAttendees', count?: number | null, list?: Array<{ __typename?: 'Attendee', id?: string | null, fully_vaccinated_covid?: boolean | null, nationality?: string | null }> | null } | null, timeZone?: { __typename?: 'PlaceDetailsTimeZoneResponse', time_zone?: number | null } | null }> | null, destinations?: Array<{ __typename?: 'Destination', address?: string | null, airport_id?: string | null, airport_lnglat?: any | null, airport_name?: string | null, analysis_date?: any | null, analysis_status?: AnalysisStatus | null, baseline?: boolean | null, city_id: string, city_lnglat?: any | null, city_name?: string | null, country_id: string, country_lnglat?: any | null, country_name?: string | null, id: string, lnglat?: any | null, meeting_id: string, city_sygic_image_id?: string | null, accommodation?: { __typename?: 'AccommodationResult', cost_avg: number, price_avg: number } | null, analysis_user?: { __typename?: 'User', id?: string | null, first_name?: string | null, last_name?: string | null, email?: string | null } | null, offices?: { __typename?: 'OfficeList', count?: number | null } | null, covid19?: { __typename?: 'MeetingCovidInfectionRate', data?: Array<{ __typename?: 'PlaceDetailsCovidInfectionRatesResponse', infection_rate?: number | null } | null> | null } | null, risk?: { __typename?: 'PlaceDetailsTravelAdvisoryRiskResponse', risk?: number | null } | null, timeZone?: { __typename?: 'PlaceDetailsTimeZoneResponse', time_zone?: number | null } | null, weather?: { __typename?: 'PlaceDetailsWeatherResponse', temp?: number | null } | null, travel?: { __typename?: 'MeetingTravelDetailsOrigin', summary: { __typename?: 'MeetingTravelDetailsOriginSummary', totalCost?: number | null, totalDuration?: number | null, totalEmissions?: number | null, totalStops?: number | null, totalTravellers?: number | null } } | null, meetingSpaces?: { __typename?: 'MeetingSpaceList', page?: { __typename?: 'Page', total?: number | null } | null } | null }> | null } };

export type MeetingAnalysisDetailQueryVariables = Exact<{
  meetingId: Scalars['String'];
  organizationId: Scalars['String'];
  destinationId: Scalars['String'];
}>;


export type MeetingAnalysisDetailQuery = { __typename?: 'Query', meetingAnalysisDetail: { __typename?: 'MeetingAnalysisDetail', id: string, country_id: string, country_name: string, visas?: Array<{ __typename?: 'MeetingVisaRestrictions', from_countries?: Array<string | null> | null, origin_id?: string | null, to_country?: string | null, data?: Array<{ __typename?: 'PlaceDetailsVisaRequirementsResponse', from_country?: string | null, to_country?: string | null, traveller_count?: number | null, visa_requirement?: string | null, visa_titles?: Array<string | null> | null, visa_durations?: Array<number | null> | null, visa_required?: boolean | null } | null> | null } | null> | null, travel?: { __typename?: 'MeetingTravelDetailsOriginFull', destination_id: string, data: Array<{ __typename?: 'TravelDetailsOrigin', query_id?: number | null, cabin_class?: string | null, cost?: number | null, travellerCount?: number | null, timezoneDiff?: number | null, supplier?: string | null, inbound_carrier_img?: string | null, inbound_arrival?: string | null, inbound_departure?: string | null, inbound_duration?: number | null, inbound_flight_duration?: number | null, inbound_emissions?: number | null, inbound_in_policy?: boolean | null, inbound_leg_id?: string | null, inbound_legs?: number | null, inbound_max_stop_risk?: number | null, inbound_stops?: number | null, itinerary_id?: number | null, outbound_carrier_img?: string | null, outbound_arrival?: string | null, outbound_departure?: string | null, outbound_duration?: number | null, outbound_emissions?: number | null, outbound_flight_duration?: number | null, outbound_in_policy?: boolean | null, outbound_leg_id?: string | null, outbound_legs?: number | null, outbound_max_stop_risk?: number | null, outbound_stops?: number | null, originCityName?: string | null, originCountryId?: string | null, originCountryName?: string | null, originCountryflag?: string | null, originId: string, total_stops?: number | null, total_emissions?: number | null, total_legs?: number | null, total_duration?: number | null, inboundLeg?: Array<{ __typename?: 'TravelDetailsOriginLegDetails', carrier?: number | null, arrival?: string | null, carrier_code?: string | null, carrier_img?: string | null, carrier_name?: string | null, departure?: string | null, destinationAirport?: string | null, destinationAirportName?: string | null, destinationCityCode?: string | null, destinationCityName?: string | null, destinationCountryCode?: string | null, destinationCountryName?: string | null, destination_code?: string | null, directionality?: string | null, distance?: number | null, duration?: number | null, emissions?: number | null, flight_number?: string | null, id?: number | null, journey_mode?: string | null, leg_id?: string | null, operating_carrier?: number | null, operating_carrier_code?: string | null, originAirport?: string | null, originAirportName?: string | null, originCityCode?: string | null, originCityName?: string | null, originCountryCode?: string | null, originCountryName?: string | null, origin_code?: string | null, query_id?: number | null, segment_id?: string | null, stop_duration?: number | null } | null> | null, outboundLeg?: Array<{ __typename?: 'TravelDetailsOriginLegDetails', arrival?: string | null, carrier_code?: string | null, carrier_img?: string | null, carrier_name?: string | null, departure?: string | null, destinationAirport?: string | null, destinationAirportName?: string | null, destinationCityCode?: string | null, destinationCityName?: string | null, destinationCountryCode?: string | null, destinationCountryName?: string | null, destination_code?: string | null, directionality?: string | null, distance?: number | null, duration?: number | null, emissions?: number | null, flight_number?: string | null, id?: number | null, journey_mode?: string | null, leg_id?: string | null, operating_carrier?: number | null, operating_carrier_code?: string | null, originAirport?: string | null, originAirportName?: string | null, originCityCode?: string | null, originCityName?: string | null, originCountryCode?: string | null, originCountryName?: string | null, origin_code?: string | null, query_id?: number | null, segment_id?: string | null, stop_duration?: number | null, carrier?: number | null } | null> | null } | null> } | null, origins?: Array<{ __typename?: 'Origin', id: string, airport_id?: string | null, airport_name?: string | null, city_id?: string | null, city_name?: string | null, country_id: string, country_name?: string | null, attendees?: { __typename?: 'OriginAttendees', count?: number | null, list?: Array<{ __typename?: 'Attendee', fully_vaccinated_covid?: boolean | null, id?: string | null, nationality?: string | null }> | null } | null } | null> | null, accommodation?: { __typename?: 'Accommodation', id: string, provider?: string | null, provider_id?: string | null, title?: string | null, default_price?: number | null, default?: boolean | null, chain?: string | null, number_of_rooms?: number | null, lnglat?: any | null, address?: string | null, link?: string | null, thumbnail?: string | null, star?: number | null, distance_from_search?: string | null, green_status?: boolean | null, recommended?: boolean | null, default_id?: string | null, price: Array<{ __typename?: 'AccommodatoinProviderPrice', provider_id: string, property_id?: string | null, price?: number | null, default?: boolean | null }> } | null } };

export type MeetingSpaceQueryVariables = Exact<{
  meetingSpaceId: Scalars['String'];
  organizationId: Scalars['String'];
}>;


export type MeetingSpaceQuery = { __typename?: 'Query', meetingSpace?: { __typename?: 'MeetingSpaceRoom', address?: string | null, address_city?: string | null, address_country_or_region?: string | null, address_postal_code?: string | null, audio_device_name?: string | null, booking_type?: string | null, building?: string | null, capacity?: number | null, city_code?: string | null, country_code?: string | null, organization_id?: string | null, display_device_name?: string | null, display_name?: string | null, email_address?: string | null, floor_label?: string | null, floor_number?: string | null, geo_coordinates?: any | null, graph_user_id?: string | null, id?: string | null, is_wheel_chair_accessable?: number | null, label?: string | null, lat?: number | null, lng?: number | null, nickname?: string | null, phone?: string | null, photos?: any | null, tags?: any | null, video_device_name?: string | null } | null };

export type MeetingSpaceListQueryVariables = Exact<{
  organizationId: Scalars['String'];
  placeId?: InputMaybe<Scalars['String']>;
  perPage?: InputMaybe<Scalars['Int']>;
  currentPage?: InputMaybe<Scalars['Int']>;
}>;


export type MeetingSpaceListQuery = { __typename?: 'Query', meetingSpaceList?: { __typename?: 'MeetingSpaceList', organization_id?: string | null, city_code?: string | null, data?: Array<{ __typename?: 'MeetingSpace', id?: string | null, graph_user_id?: string | null, organization_id?: string | null, display_name?: string | null, geo_coordinates?: any | null, phone?: string | null, nickname?: string | null, email_address?: string | null, building?: string | null, floor_number?: string | null, floor_label?: string | null, label?: string | null, capacity?: number | null, booking_type?: string | null, audio_device_name?: string | null, video_device_name?: string | null, display_device_name?: string | null, is_wheel_chair_accessable?: number | null, tags?: any | null, address?: string | null, address_city?: string | null, address_postal_code?: string | null, address_country_or_region?: string | null, lat?: number | null, lng?: number | null, city_code?: string | null, country_code?: string | null, photos?: any | null, roomCount?: number | null, rooms?: Array<{ __typename?: 'MeetingSpaceRoom', id?: string | null, graph_user_id?: string | null, organization_id?: string | null, display_name?: string | null, geo_coordinates?: any | null, phone?: string | null, nickname?: string | null, email_address?: string | null, building?: string | null, floor_number?: string | null, floor_label?: string | null, label?: string | null, capacity?: number | null, booking_type?: string | null, audio_device_name?: string | null, video_device_name?: string | null, display_device_name?: string | null, is_wheel_chair_accessable?: number | null, tags?: any | null, address?: string | null, address_city?: string | null, address_postal_code?: string | null, address_country_or_region?: string | null, lat?: number | null, lng?: number | null, city_code?: string | null, country_code?: string | null, photos?: any | null, schedule?: any | null } | null> | null } | null> | null, page?: { __typename?: 'Page', from?: number | null, currentPage?: number | null, lastPage?: number | null, to?: number | null, offset?: number | null, perPage?: number | null, total?: number | null } | null } | null };

export type MeetingBasicFragment = { __typename?: 'Meeting', id?: string | null, name?: string | null, organization_id?: string | null, required_services?: Array<MeetingServices | null> | null, start_date?: any | null, start_time?: string | null, status?: MeetingStatus | null, type?: MeetingType | null, updated?: string | null, end_time?: string | null, end_date?: any | null, created?: string | null, meeting_policy_id?: string | null, meetingPolicy?: { __typename?: 'MeetingPolicy', hotel_residents_require_accommodation: boolean } | null, owner?: { __typename?: 'User', id?: string | null, email?: string | null } | null };

export type SetMeetingMutationVariables = Exact<{
  meeting: MeetingInput;
}>;


export type SetMeetingMutation = { __typename?: 'Mutation', setMeeting: { __typename?: 'MeetingResponse', code?: number | null, message?: string | null, success?: boolean | null, data?: { __typename?: 'Meeting', id?: string | null, name?: string | null } | null } };

export type MeetingBasicWithSummaryQueryVariables = Exact<{
  meetingId: Scalars['String'];
}>;


export type MeetingBasicWithSummaryQuery = { __typename?: 'Query', meeting: { __typename?: 'MeetingResponse', data?: { __typename?: 'Meeting', id?: string | null, name?: string | null, organization_id?: string | null, required_services?: Array<MeetingServices | null> | null, start_date?: any | null, start_time?: string | null, status?: MeetingStatus | null, type?: MeetingType | null, updated?: string | null, end_time?: string | null, end_date?: any | null, created?: string | null, meeting_policy_id?: string | null, destinations?: Array<{ __typename?: 'Destination', id: string, lnglat?: any | null, analysis_status?: AnalysisStatus | null, baseline?: boolean | null } | null> | null, origins?: Array<{ __typename?: 'Origin', id: string, lnglat?: any | null, country_id: string, city_id?: string | null, attendees?: { __typename?: 'OriginAttendees', count?: number | null } | null } | null> | null, meetingPolicy?: { __typename?: 'MeetingPolicy', hotel_residents_require_accommodation: boolean } | null, owner?: { __typename?: 'User', id?: string | null, email?: string | null } | null } | null } };

export type MeetingListQueryVariables = Exact<{
  currentPage?: InputMaybe<Scalars['Int']>;
  organizationId?: InputMaybe<Scalars['String']>;
  perPage?: InputMaybe<Scalars['Int']>;
  userId?: InputMaybe<Scalars['String']>;
}>;


export type MeetingListQuery = { __typename?: 'Query', meetingList?: { __typename?: 'MeetingPaginationResponse', data?: Array<{ __typename?: 'Meeting', id?: string | null, name?: string | null, updated?: string | null, start_date?: any | null, start_time?: string | null, end_date?: any | null, end_time?: string | null, type?: MeetingType | null, owner?: { __typename?: 'User', id?: string | null, email?: string | null } | null, attendees?: Array<{ __typename?: 'Attendee', id?: string | null }> | null, origins?: Array<{ __typename?: 'Origin', id: string } | null> | null, destinations?: Array<{ __typename?: 'Destination', id: string } | null> | null }> | null, page?: { __typename?: 'Page', currentPage?: number | null, from?: number | null, lastPage?: number | null, offset?: number | null, perPage?: number | null, to?: number | null, total?: number | null } | null } | null };

export type OfficeQueryVariables = Exact<{
  officeId: Scalars['ID'];
}>;


export type OfficeQuery = { __typename?: 'Query', office?: { __typename?: 'Office', address?: string | null, city_code?: string | null, organization_id?: string | null, closest_int_airport?: string | null, country_code?: string | null, lnglat?: any | null, name?: string | null, selected_city_distance?: number | null, type?: OfficeType | null } | null };

export type OfficeListQueryVariables = Exact<{
  placeId?: InputMaybe<Scalars['String']>;
  organizationId: Scalars['ID'];
}>;


export type OfficeListQuery = { __typename?: 'Query', officeList?: { __typename?: 'OfficeList', city_code?: string | null, organization_id?: string | null, count?: number | null, data?: Array<{ __typename?: 'Office', address?: string | null, city_code?: string | null, organization_id?: string | null, closest_int_airport?: string | null, country_code?: string | null, lnglat?: any | null, name?: string | null, selected_city_distance?: number | null, type?: OfficeType | null } | null> | null } | null };

export type SetOriginMutationVariables = Exact<{
  origin: OriginInput;
}>;


export type SetOriginMutation = { __typename?: 'Mutation', setOrigin: { __typename?: 'OriginResponse', code?: number | null, message?: string | null, success?: boolean | null, data?: { __typename?: 'OriginDetails', count?: number | null, origin?: { __typename?: 'Origin', airport_lnglat?: any | null, airport_name?: string | null, city_id?: string | null, city_lnglat?: any | null, city_name?: string | null, country_id: string, country_lnglat?: any | null, country_name?: string | null, id: string, lnglat?: any | null, meeting_id: string, status?: OriginStatus | null, attendees?: { __typename?: 'OriginAttendees', count?: number | null, list?: Array<{ __typename?: 'Attendee', id?: string | null, nationality?: string | null, fully_vaccinated_covid?: boolean | null }> | null } | null } | null } | null } };

export type OriginListQueryVariables = Exact<{
  meetingId: Scalars['String'];
}>;


export type OriginListQuery = { __typename?: 'Query', originList?: Array<{ __typename?: 'Origin', address: string, airport_id?: string | null, airport_lnglat?: any | null, airport_name?: string | null, city_id?: string | null, city_lnglat?: any | null, city_name?: string | null, country_id: string, country_lnglat?: any | null, country_name?: string | null, id: string, lnglat?: any | null, attendees?: { __typename?: 'OriginAttendees', count?: number | null, list?: Array<{ __typename?: 'Attendee', fully_vaccinated_covid?: boolean | null, id?: string | null, nationality?: string | null, email?: string | null }> | null } | null, timeZone?: { __typename?: 'PlaceDetailsTimeZoneResponse', city_id?: string | null, time_zone?: number | null } | null }> | null };

export type CovidPolicyListQueryVariables = Exact<{
  countries: Scalars['String'];
  includedOrigins?: InputMaybe<Scalars['String']>;
  startDate: Scalars['String'];
  endDate: Scalars['String'];
}>;


export type CovidPolicyListQuery = { __typename?: 'Query', covidPolicyList?: { __typename?: 'CovidPolicies', inbound: { __typename?: 'CovidPoliciesByDirection', restrictions?: Array<{ __typename?: 'Restriction', id: string, destination_country_code: string, destination_region_code: string, origin_count: number, included: Array<string | null>, region_included: Array<string | null>, category: string, subcategory: string, severity: number, title: string, description?: string | null, tags?: Array<string | null> | null, last_checked_at?: string | null, last_updated_at?: string | null, still_current_at?: string | null, created_at?: string | null, start_date?: string | null, end_date?: string | null, tt_updated_at: string, source: { __typename?: 'CovidSource', url: string, title: string, sourceType: string } }> | null, procedures?: Array<{ __typename?: 'Procedure', id: string, destination_country_code: string, destination_region_code: string, origin_count: number, included: Array<string | null>, region_included: Array<string | null>, category: string, subcategory: string, enforcement: string, severity: number, title: string, description?: string | null, tags?: Array<string | null> | null, last_checked_at?: string | null, last_updated_at?: string | null, still_current_at?: string | null, created_at?: string | null, start_date?: string | null, end_date?: string | null, tt_updated_at: string, source: { __typename?: 'CovidSource', url: string, title: string, sourceType: string } }> | null }, outbound: { __typename?: 'CovidPoliciesByDirection', restrictions?: Array<{ __typename?: 'Restriction', id: string, destination_country_code: string, destination_region_code: string, origin_count: number, included: Array<string | null>, region_included: Array<string | null>, category: string, subcategory: string, severity: number, title: string, description?: string | null, tags?: Array<string | null> | null, last_checked_at?: string | null, last_updated_at?: string | null, still_current_at?: string | null, created_at?: string | null, start_date?: string | null, end_date?: string | null, tt_updated_at: string, source: { __typename?: 'CovidSource', url: string, title: string, sourceType: string }, documentLinks?: Array<{ __typename?: 'RestrictionDocumentLink', type: string, url: string, title: string } | null> | null }> | null, procedures?: Array<{ __typename?: 'Procedure', id: string, destination_country_code: string, destination_region_code: string, origin_count: number, included: Array<string | null>, region_included: Array<string | null>, category: string, subcategory: string, enforcement: string, severity: number, title: string, description?: string | null, tags?: Array<string | null> | null, last_checked_at?: string | null, last_updated_at?: string | null, still_current_at?: string | null, created_at?: string | null, start_date?: string | null, end_date?: string | null, tt_updated_at: string, source: { __typename?: 'CovidSource', url: string, title: string, sourceType: string }, documentLinks?: Array<{ __typename?: 'RestrictionDocumentLink', type: string, url: string, title: string } | null> | null }> | null } } | null };

export type PlaceSuggestionListQueryVariables = Exact<{
  placeSuggestionsRequest: PlaceSuggestionsRequest;
}>;


export type PlaceSuggestionListQuery = { __typename?: 'Query', placeSuggestionList: Array<{ __typename?: 'PlaceSuggestionsResponse', uuid: string, avgtemp?: number | null, country_code?: string | null, dist?: number | null, iata_code?: string | null, is_international?: number | null, lat?: number | null, lng?: number | null, name?: string | null, offices?: number | null, ranking?: number | null, region?: string | null, risk?: number | null, sygic_image_id?: string | null, sygic_rating?: number | null, time_zone?: number | null } | null> };

export type PlaceDetailQueryVariables = Exact<{
  placeSearchRequest: PlaceDetailsSearchRequest;
}>;


export type PlaceDetailQuery = { __typename?: 'Query', placeDetail: { __typename?: 'PlaceDetailsSearchResponse', city?: { __typename?: 'PlaceDetailsCityResponse', country_summary?: string | null, city_code?: string | null, city_country_code?: string | null, city_airports?: any | null, city_g_json?: any | null, city_g_place_id?: string | null, city_g_rating?: string | null, city_g_region_id?: string | null, city_g_region_name?: string | null, city_iata_code?: string | null, city_is_international?: number | null, city_lat?: number | null, city_lng?: number | null, city_name?: string | null, city_ranking?: number | null, city_summary_text?: string | null, city_single_airport_city?: number | null, city_ss_region_code?: string | null, city_ss_id?: string | null, city_ss_updated_timestamp?: any | null, city_sygic_id?: string | null, city_sygic_image_id?: string | null, city_sygic_rating?: number | null, city_time_zone?: number | null, city_time_zone_id?: string | null, country_cases_active?: string | null, country_cases_as_of_date?: string | null, country_cases_confirmed?: number | null, country_cases_deaths?: number | null, country_cases_recovered?: string | null, country_code?: string | null, country_covid_risk_level?: string | null, country_infection_as_of_date?: string | null, country_infection_level?: string | null, country_infection_rate?: number | null, country_it_entry_and_borders_entry_ban?: string | null, country_it_entry_and_borders_entry_rules?: string | null, country_it_entry_and_borders_last_update?: string | null, country_it_entry_and_borders_exemptions?: string | null, country_it_entry_and_borders_through_date?: string | null, country_it_exit_country_exit_requirements?: string | null, country_it_entry_and_borders_text?: string | null, country_it_exit_country_last_update?: string | null, country_it_exit_country_requirement?: string | null, country_it_health_travel_doc_health_document?: string | null, country_it_health_travel_doc_last_update?: string | null, country_it_exit_country_text?: string | null, country_it_health_travel_doc_need_documentations?: string | null, country_it_health_travel_doc_text?: string | null, country_it_health_travel_doc_travel_document?: string | null, country_it_international_flights_is_banned?: string | null, country_it_international_flights_last_update?: string | null, country_it_international_flights_text?: string | null, country_it_international_flights_through_date?: string | null, country_it_masks_last_update?: string | null, country_it_masks_requirement?: string | null, country_it_masks_text?: string | null, country_it_quarantine_on_arrival_days?: string | null, country_it_quarantine_on_arrival_last_update?: string | null, country_it_quarantine_on_arrival_mandate_list?: string | null, country_it_quarantine_on_arrival_rules?: string | null, country_it_quarantine_on_arrival_text?: string | null, country_it_quarantine_on_arrival_type?: string | null, country_it_quarantine_on_arrival_who_needs?: string | null, country_it_testing_last_update?: string | null, country_it_testing_need_test?: string | null, country_it_testing_rules?: string | null, country_it_testing_requirement?: string | null, country_it_testing_text?: string | null, country_it_testing_when?: string | null, country_name?: string | null, country_policy_current_status?: string | null, country_policy_end_date?: string | null, country_policy_last_update?: string | null, country_policy_start_date?: string | null, country_risk_level?: string | null, country_travel_risk?: number | null, country_policy_text?: string | null } | null } };

export type PlaceRestrictionListQueryVariables = Exact<{
  placeRestrictionsRequest: PlaceRestrictionsRequest;
}>;


export type PlaceRestrictionListQuery = { __typename?: 'Query', placeRestrictionList: { __typename?: 'PlaceDetailsRestrictionsResponse', vaccinated?: Array<{ __typename?: 'CovidRestrictionItems', country?: string | null, requireRestriction?: Array<{ __typename?: 'CovidRestrictionEntry', id?: string | null, origin_country_code?: string | null, destination_country_code?: string | null, category?: string | null, subcategory?: string | null, tags?: Array<string | null> | null, title?: string | null, requireRestriction?: number | null } | null> | null, requireQuarantine?: Array<{ __typename?: 'CovidRestrictionQuarantine', id?: string | null, origin_country_code?: string | null, destination_country_code?: string | null, category?: string | null, subcategory?: string | null, tags?: Array<string | null> | null, title?: string | null, requireQuarantine?: number | null, quarantine_days?: number | null } | null> | null, requireTest?: Array<{ __typename?: 'CovidRestrictionQuarantine', id?: string | null, origin_country_code?: string | null, destination_country_code?: string | null, category?: string | null, subcategory?: string | null, tags?: Array<string | null> | null, title?: string | null, requireTest?: number | null, quarantine_days?: number | null } | null> | null } | null> | null, unvaccinated?: Array<{ __typename?: 'CovidRestrictionItems', country?: string | null, requireRestriction?: Array<{ __typename?: 'CovidRestrictionEntry', id?: string | null, origin_country_code?: string | null, destination_country_code?: string | null, category?: string | null, subcategory?: string | null, tags?: Array<string | null> | null, title?: string | null, requireRestriction?: number | null } | null> | null, requireQuarantine?: Array<{ __typename?: 'CovidRestrictionQuarantine', id?: string | null, origin_country_code?: string | null, destination_country_code?: string | null, category?: string | null, subcategory?: string | null, tags?: Array<string | null> | null, title?: string | null, requireQuarantine?: number | null, quarantine_days?: number | null } | null> | null, requireTest?: Array<{ __typename?: 'CovidRestrictionQuarantine', id?: string | null, origin_country_code?: string | null, destination_country_code?: string | null, category?: string | null, subcategory?: string | null, tags?: Array<string | null> | null, title?: string | null, requireTest?: number | null, quarantine_days?: number | null } | null> | null } | null> | null } };

export type SetTravelAnalysisMutationVariables = Exact<{
  itineraryId: Scalars['Int'];
}>;


export type SetTravelAnalysisMutation = { __typename?: 'Mutation', setTravelAnalysis: { __typename?: 'TravelResponse', api: string, version: string, result?: { __typename?: 'TravelResponseResult', data?: string | null, statusCode: number, summary?: { __typename?: 'RouteSummary', statusCode: number, data?: string | null } | null } | null, error?: { __typename?: 'TravelResponseResultError', statusCode: number, trace: number, status: string, message: string, stackTrace?: Array<string | null> | null } | null } };

export type FlightOptionDetailQueryVariables = Exact<{
  inboundLegId: Scalars['String'];
  outboundLegId: Scalars['String'];
  queryId: Scalars['Int'];
}>;


export type FlightOptionDetailQuery = { __typename?: 'Query', flightOptionDetail: { __typename?: 'FlightDataDetailsResponse', api: string, version: string, result?: { __typename?: 'FlightDataDetailsResponseResult', inbound?: Array<{ __typename?: 'FlightTravelDataDetails', arrival?: string | null, carrier?: number | null, carrier_name?: string | null, departure?: string | null, carrier_code?: string | null, carrier_img?: string | null, destinationAirportName?: string | null, destinationCountryName?: string | null, duration?: number | null, emissions?: number | null, flight_number?: string | null, originAirportName?: string | null, originCountryName?: string | null, stop_duration?: number | null } | null> | null, outbound?: Array<{ __typename?: 'FlightTravelDataDetails', arrival?: string | null, carrier?: number | null, carrier_name?: string | null, departure?: string | null, carrier_code?: string | null, carrier_img?: string | null, destinationAirportName?: string | null, destinationCountryName?: string | null, duration?: number | null, emissions?: number | null, flight_number?: string | null, originAirportName?: string | null, originCountryName?: string | null, stop_duration?: number | null } | null> | null } | null } };

export type FlightOptionFiltersQueryVariables = Exact<{
  meetingId: Scalars['String'];
  originId: Scalars['String'];
  destinationId: Scalars['String'];
  pagination?: InputMaybe<TravelPagination>;
  filters?: InputMaybe<FlightDataFilters>;
}>;


export type FlightOptionFiltersQuery = { __typename?: 'Query', flightOptionList?: { __typename?: 'FlightDataResponse', data?: Array<{ __typename?: 'FlightData', total_stops?: number | null, total_duration?: number | null, total_emissions?: number | null, cost?: number | null, inbound_departure?: string | null, outbound_departure?: string | null, outbound_origin_airport_name?: string | null, outbound_origin_airport_code?: string | null, cabin_class?: string | null } | null> | null } | null };

export type FlightOptionListQueryVariables = Exact<{
  meetingId: Scalars['String'];
  originId: Scalars['String'];
  destinationId: Scalars['String'];
  pagination?: InputMaybe<TravelPagination>;
  filters?: InputMaybe<FlightDataFilters>;
}>;


export type FlightOptionListQuery = { __typename?: 'Query', flightOptionList?: { __typename?: 'FlightDataResponse', data?: Array<{ __typename?: 'FlightData', query_id?: number | null, cabin_class?: string | null, itinerary_id?: number | null, total_duration?: number | null, total_emissions?: number | null, total_stops?: number | null, cost?: number | null, inbound_in_policy?: number | null, inbound_leg_id?: string | null, inbound_departure?: string | null, inbound_arrival?: string | null, inbound_duration?: number | null, inbound_flight_duration?: number | null, inbound_stops?: number | null, inbound_carrier_name?: string | null, inbound_carrier_img?: string | null, outbound_in_policy?: number | null, outbound_leg_id?: string | null, outbound_duration?: number | null, outbound_departure?: string | null, outbound_arrival?: string | null, outbound_flight_duration?: number | null, outbound_stops?: number | null, outbound_carrier_name?: string | null, outbound_carrier_img?: string | null, rank_cost?: number | null, is_feasible?: number | null } | null> | null, page?: { __typename?: 'FlightPagination', total?: number | null, lastPage?: number | null, currentPage?: number | null } | null } | null };

export type SetMeetingPolicyMutationVariables = Exact<{
  meetingPolicy: MeetingPolicyInput;
  organizationId: Scalars['String'];
}>;


export type SetMeetingPolicyMutation = { __typename?: 'Mutation', setMeetingPolicy: { __typename?: 'MeetingPolicy', meeting_id: string, id: string, name: string } };

export type SetTravelPolicyMutationVariables = Exact<{
  organizationId: Scalars['String'];
  travelPolicy: TravelPolicyInput;
}>;


export type SetTravelPolicyMutation = { __typename?: 'Mutation', setTravelPolicy: { __typename?: 'TravelPolicy', flight_allowed_stops_more_than_min?: number | null, flight_cabin_class?: Array<FlightCabinClass> | null, flight_low_cost_airlines: boolean, flight_max_leg_segment_duration?: number | null, flight_max_leg_segment_duration_business?: number | null, flight_max_leg_segment_duration_check?: number | null, flight_max_leg_segment_duration_economy?: number | null, flight_max_leg_segment_duration_first_class?: number | null, flight_max_leg_segment_duration_premium_economy?: number | null, flight_max_stops?: number | null, flight_max_total_flight_leg_duration?: number | null, flight_max_travel_duration?: number | null, flight_avoid_carriers?: Array<string> | null, flight_preferred_carriers?: Array<string> | null, flight_providers: Array<string>, hotel_allow_only_green: boolean, hotel_allow_only_preferred: boolean, hotel_allow_room_sharing: boolean, hotel_apply_cap: boolean, hotel_minimum_rating: number, hotel_number_to_suggest: number, hotel_optimization: Array<string>, hotel_star_rating: Array<number>, hotel_residents_require_accommodation: boolean, hotel_suggest_radius: number, id: string, name: string, status?: PolicyStatus | null, organization_id: string, isDefault?: boolean | null, transport_consider_alternative_max_distance: number, transport_consider_flying_min_distance: number, transport_modes: Array<string>, flight_max_leg_segment_duration_classes?: Array<number> | null } };

export type CarrierDataQueryVariables = Exact<{ [key: string]: never; }>;


export type CarrierDataQuery = { __typename?: 'Query', carrierData: { __typename?: 'CarrierResponse', result?: Array<{ __typename?: 'CarrierAlliance', id?: string | null, name?: string | null, carriers?: Array<{ __typename?: 'Carrier', id: string, name: string, url?: string | null } | null> | null } | null> | null } };

export type MeetingPolicyQueryVariables = Exact<{
  meetingId: Scalars['String'];
  meetingPolicyId: Scalars['String'];
}>;


export type MeetingPolicyQuery = { __typename?: 'Query', meetingPolicy: { __typename?: 'MeetingPolicy', flight_allowed_stops_more_than_min?: number | null, flight_avoid_carriers?: Array<string> | null, flight_cabin_class?: Array<FlightCabinClass> | null, flight_low_cost_airlines: boolean, flight_max_leg_segment_duration?: number | null, flight_max_leg_segment_duration_business?: number | null, flight_max_leg_segment_duration_check?: number | null, flight_max_leg_segment_duration_economy?: number | null, flight_max_leg_segment_duration_first_class?: number | null, flight_max_leg_segment_duration_premium_economy?: number | null, flight_max_stops?: number | null, flight_max_total_flight_leg_duration?: number | null, flight_max_travel_duration?: number | null, flight_preferred_carriers?: Array<string> | null, flight_providers: Array<string>, hotel_allow_only_green: boolean, hotel_allow_only_preferred: boolean, hotel_allow_room_sharing: boolean, hotel_apply_cap: boolean, hotel_minimum_rating: number, hotel_number_to_suggest: number, hotel_optimization: Array<string>, hotel_star_rating: Array<number>, hotel_residents_require_accommodation: boolean, hotel_suggest_radius: number, id: string, meeting_id: string, name: string, transport_consider_alternative_max_distance: number, transport_consider_flying_min_distance: number, transport_modes: Array<string>, travel_policy_id: string, flight_max_leg_segment_duration_classes?: Array<number> | null } };

export type TravelPolicyQueryVariables = Exact<{
  organizationId: Scalars['String'];
  travelPolicyId: Scalars['String'];
}>;


export type TravelPolicyQuery = { __typename?: 'Query', travelPolicy: { __typename?: 'TravelPolicy', flight_allowed_stops_more_than_min?: number | null, flight_avoid_carriers?: Array<string> | null, flight_cabin_class?: Array<FlightCabinClass> | null, flight_low_cost_airlines: boolean, flight_max_leg_segment_duration?: number | null, flight_max_leg_segment_duration_business?: number | null, flight_max_leg_segment_duration_check?: number | null, flight_max_leg_segment_duration_economy?: number | null, flight_max_leg_segment_duration_first_class?: number | null, flight_max_leg_segment_duration_premium_economy?: number | null, flight_max_stops?: number | null, flight_max_total_flight_leg_duration?: number | null, flight_max_travel_duration?: number | null, flight_preferred_carriers?: Array<string> | null, flight_providers: Array<string>, hotel_allow_only_green: boolean, hotel_allow_only_preferred: boolean, hotel_allow_room_sharing: boolean, hotel_apply_cap: boolean, hotel_minimum_rating: number, hotel_number_to_suggest: number, hotel_optimization: Array<string>, hotel_residents_require_accommodation: boolean, hotel_star_rating: Array<number>, hotel_suggest_radius: number, id: string, name: string, organization_id: string, isDefault?: boolean | null, transport_consider_alternative_max_distance: number, transport_consider_flying_min_distance: number, transport_modes: Array<string>, flight_max_leg_segment_duration_classes?: Array<number> | null } };

export type TravelPolicyListQueryVariables = Exact<{
  organizationId: Scalars['String'];
}>;


export type TravelPolicyListQuery = { __typename?: 'Query', travelPolicyList: Array<{ __typename?: 'TravelPolicy', flight_allowed_stops_more_than_min?: number | null, flight_avoid_carriers?: Array<string> | null, flight_cabin_class?: Array<FlightCabinClass> | null, flight_low_cost_airlines: boolean, flight_max_leg_segment_duration?: number | null, flight_max_leg_segment_duration_business?: number | null, flight_max_leg_segment_duration_check?: number | null, flight_max_leg_segment_duration_economy?: number | null, flight_max_leg_segment_duration_first_class?: number | null, flight_max_leg_segment_duration_premium_economy?: number | null, flight_max_stops?: number | null, flight_max_total_flight_leg_duration?: number | null, flight_max_travel_duration?: number | null, flight_preferred_carriers?: Array<string> | null, flight_providers: Array<string>, hotel_allow_only_green: boolean, hotel_allow_only_preferred: boolean, hotel_allow_room_sharing: boolean, hotel_apply_cap: boolean, hotel_minimum_rating: number, hotel_number_to_suggest: number, hotel_optimization: Array<string>, hotel_residents_require_accommodation: boolean, hotel_star_rating: Array<number>, hotel_suggest_radius: number, id: string, name: string, organization_id: string, isDefault?: boolean | null, transport_consider_alternative_max_distance: number, transport_consider_flying_min_distance: number, transport_modes: Array<string>, flight_max_leg_segment_duration_classes?: Array<number> | null }> };

export type OrganizationBasicFragment = { __typename?: 'Organization', id?: string | null, name?: string | null, signin_types?: Array<string> | null, saml_provider?: Array<string | null> | null, theme_variables?: string | null, saml_code?: any | null, assets?: { __typename?: 'Assets', id: string, logos?: Array<{ __typename?: 'Logo', url?: string | null, filename?: string | null, mimetype?: string | null, type?: OrganizationLogo | null } | null> | null } | null };

export type UserBasicDataFragment = { __typename?: 'User', designation?: string | null, email?: string | null, id?: string | null, first_name?: string | null, last_name?: string | null, email_verified?: boolean | null, roles?: Array<{ __typename?: 'UserOrganizationRoles', organizationId: string, roles?: Array<Roles | null> | null } | null> | null, organizations?: Array<{ __typename?: 'Organization', id?: string | null, name?: string | null, signin_types?: Array<string> | null, saml_provider?: Array<string | null> | null, theme_variables?: string | null, saml_code?: any | null, assets?: { __typename?: 'Assets', id: string, logos?: Array<{ __typename?: 'Logo', url?: string | null, filename?: string | null, mimetype?: string | null, type?: OrganizationLogo | null } | null> | null } | null } | null> | null };

export type AuthSamlMutationVariables = Exact<{
  user: AuthSamlForm;
}>;


export type AuthSamlMutation = { __typename?: 'Mutation', authSaml: { __typename?: 'AuthResponse', refreshToken?: string | null, accessToken?: string | null, data?: { __typename?: 'User', designation?: string | null, email?: string | null, id?: string | null, first_name?: string | null, last_name?: string | null, email_verified?: boolean | null, roles?: Array<{ __typename?: 'UserOrganizationRoles', organizationId: string, roles?: Array<Roles | null> | null } | null> | null, organizations?: Array<{ __typename?: 'Organization', id?: string | null, name?: string | null, signin_types?: Array<string> | null, saml_provider?: Array<string | null> | null, theme_variables?: string | null, saml_code?: any | null, assets?: { __typename?: 'Assets', id: string, logos?: Array<{ __typename?: 'Logo', url?: string | null, filename?: string | null, mimetype?: string | null, type?: OrganizationLogo | null } | null> | null } | null } | null> | null } | null } };

export type AuthUserMutationVariables = Exact<{
  user: AuthForm;
}>;


export type AuthUserMutation = { __typename?: 'Mutation', authUser: { __typename?: 'AuthResponse', refreshToken?: string | null, accessToken?: string | null, data?: { __typename?: 'User', designation?: string | null, email?: string | null, id?: string | null, first_name?: string | null, last_name?: string | null, email_verified?: boolean | null, roles?: Array<{ __typename?: 'UserOrganizationRoles', organizationId: string, roles?: Array<Roles | null> | null } | null> | null, organizations?: Array<{ __typename?: 'Organization', id?: string | null, name?: string | null, signin_types?: Array<string> | null, saml_provider?: Array<string | null> | null, theme_variables?: string | null, saml_code?: any | null, assets?: { __typename?: 'Assets', id: string, logos?: Array<{ __typename?: 'Logo', url?: string | null, filename?: string | null, mimetype?: string | null, type?: OrganizationLogo | null } | null> | null } | null } | null> | null } | null } };

export type ConfirmResetPasswordMutationVariables = Exact<{
  form: ResetPasswordForm;
}>;


export type ConfirmResetPasswordMutation = { __typename?: 'Mutation', confirmResetPassword: boolean };

export type ConfirmVerifyUserMutationVariables = Exact<{
  form: UserVerifyEmailForm;
}>;


export type ConfirmVerifyUserMutation = { __typename?: 'Mutation', confirmVerifyUser: boolean };

export type ResetPasswordMutationVariables = Exact<{
  user: UserEmailForm;
}>;


export type ResetPasswordMutation = { __typename?: 'Mutation', resetPassword: boolean };

export type VerifyUserMutationVariables = Exact<{
  user: UserEmailForm;
}>;


export type VerifyUserMutation = { __typename?: 'Mutation', verifyUser: boolean };

export type IsUserQueryVariables = Exact<{
  email: Scalars['String'];
}>;


export type IsUserQuery = { __typename?: 'Query', isUser: { __typename?: 'UserCheckResponse', provider?: Array<string | null> | null, exist: boolean, emailVerified?: boolean | null, organizations?: Array<{ __typename?: 'Organization', id?: string | null, name?: string | null, signin_types?: Array<string> | null, saml_provider?: Array<string | null> | null, theme_variables?: string | null, saml_code?: any | null, assets?: { __typename?: 'Assets', id: string, logos?: Array<{ __typename?: 'Logo', url?: string | null, filename?: string | null, mimetype?: string | null, type?: OrganizationLogo | null } | null> | null } | null } | null> | null } };

export type OrganizationSsoAuthQueryVariables = Exact<{
  code: Scalars['SsoCode'];
}>;


export type OrganizationSsoAuthQuery = { __typename?: 'Query', organizationSsoAuth?: { __typename?: 'OrganizationSso', name?: string | null, signin_types?: Array<string> | null, saml_provider?: Array<string | null> | null, theme_variables?: string | null, assets?: { __typename?: 'Assets', id: string, logos?: Array<{ __typename?: 'Logo', url?: string | null, filename?: string | null, mimetype?: string | null, type?: OrganizationLogo | null } | null> | null } | null } | null };

export type RecycleTokenQueryVariables = Exact<{
  refreshToken: Scalars['String'];
}>;


export type RecycleTokenQuery = { __typename?: 'Query', recycleToken?: { __typename?: 'AuthResponse', accessToken?: string | null, refreshToken?: string | null, data?: { __typename?: 'User', designation?: string | null, email?: string | null, id?: string | null, first_name?: string | null, last_name?: string | null, email_verified?: boolean | null, roles?: Array<{ __typename?: 'UserOrganizationRoles', organizationId: string, roles?: Array<Roles | null> | null } | null> | null, organizations?: Array<{ __typename?: 'Organization', id?: string | null, name?: string | null, signin_types?: Array<string> | null, saml_provider?: Array<string | null> | null, theme_variables?: string | null, saml_code?: any | null, assets?: { __typename?: 'Assets', id: string, logos?: Array<{ __typename?: 'Logo', url?: string | null, filename?: string | null, mimetype?: string | null, type?: OrganizationLogo | null } | null> | null } | null } | null> | null } | null } | null };

export type RevokeTokenQueryVariables = Exact<{
  userId: Scalars['String'];
}>;


export type RevokeTokenQuery = { __typename?: 'Query', revokeToken?: boolean | null };

export type SwitchOrganizationQueryVariables = Exact<{
  organizationId: Scalars['String'];
  isDefault?: InputMaybe<Scalars['Boolean']>;
}>;


export type SwitchOrganizationQuery = { __typename?: 'Query', switchOrganization?: { __typename?: 'AuthResponse', accessToken?: string | null, refreshToken?: string | null } | null };

export type UserBasicQueryVariables = Exact<{
  userId: Scalars['String'];
}>;


export type UserBasicQuery = { __typename?: 'Query', user: { __typename?: 'UserResponse', data?: { __typename?: 'User', designation?: string | null, email?: string | null, id?: string | null, first_name?: string | null, last_name?: string | null, email_verified?: boolean | null, roles?: Array<{ __typename?: 'UserOrganizationRoles', organizationId: string, roles?: Array<Roles | null> | null } | null> | null, organizations?: Array<{ __typename?: 'Organization', id?: string | null, name?: string | null, signin_types?: Array<string> | null, saml_provider?: Array<string | null> | null, theme_variables?: string | null, saml_code?: any | null, assets?: { __typename?: 'Assets', id: string, logos?: Array<{ __typename?: 'Logo', url?: string | null, filename?: string | null, mimetype?: string | null, type?: OrganizationLogo | null } | null> | null } | null } | null> | null } | null } };

export type UserListQueryVariables = Exact<{ [key: string]: never; }>;


export type UserListQuery = { __typename?: 'Query', userList?: Array<{ __typename?: 'User', created?: string | null, designation?: string | null, email?: string | null, fb_payload?: any | null, first_name?: string | null, id?: string | null, last_name?: string | null, updated?: string | null, organizations?: Array<{ __typename?: 'Organization', user_email_domains?: Array<string> | null, user_email_tld?: string | null, theme_variables?: string | null, signin_types?: Array<string> | null, response_property?: string | null, name?: string | null, id?: string | null, global_hotel_cap?: number | null, banned_countries?: Array<string> | null, app_custom_domain_address?: string | null } | null> | null }> | null };

export const MeetingBasicFragmentDoc = gql`
    fragment MeetingBasic on Meeting {
  id
  name
  organization_id
  required_services
  start_date
  start_time
  status
  type
  updated
  end_time
  end_date
  created
  meeting_policy_id
  meetingPolicy {
    hotel_residents_require_accommodation
  }
  owner {
    id
    email
  }
}
    `;
export const OrganizationBasicFragmentDoc = gql`
    fragment OrganizationBasic on Organization {
  id
  name
  signin_types
  saml_provider
  theme_variables
  assets {
    id
    logos {
      url
      filename
      mimetype
      type
    }
  }
  saml_code
}
    `;
export const UserBasicDataFragmentDoc = gql`
    fragment UserBasicData on User {
  designation
  email
  id
  first_name
  last_name
  roles {
    organizationId
    roles
  }
  email_verified
  organizations {
    ...OrganizationBasic
  }
}
    ${OrganizationBasicFragmentDoc}`;
export const SetAccommodationDocument = gql`
    mutation SetAccommodation($destinationId: String!, $defaultId: String!) {
  setAccommodation(destinationId: $destinationId, defaultId: $defaultId) {
    id
    selected
  }
}
    `;

/**
 * __useSetAccommodationMutation__
 *
 * To run a mutation, you first call `useSetAccommodationMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useSetAccommodationMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useSetAccommodationMutation({
 *   variables: {
 *     destinationId: // value for 'destinationId'
 *     defaultId: // value for 'defaultId'
 *   },
 * });
 */
export function useSetAccommodationMutation(options: VueApolloComposable.UseMutationOptions<SetAccommodationMutation, SetAccommodationMutationVariables> | ReactiveFunction<VueApolloComposable.UseMutationOptions<SetAccommodationMutation, SetAccommodationMutationVariables>>) {
  return VueApolloComposable.useMutation<SetAccommodationMutation, SetAccommodationMutationVariables>(SetAccommodationDocument, options);
}
export type SetAccommodationMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<SetAccommodationMutation, SetAccommodationMutationVariables>;
export const AccommodationFilterListDocument = gql`
    query AccommodationFilterList($data: AccommodationListInputs!) {
  accommodationList(data: $data) {
    page {
      total
      totalAfterFiltering
      perPage
      lastPage
      currentPage
      from
      to
      offset
    }
    data {
      title
      number_of_rooms
      star
      default_price
      green_status
      recommended
      distance_from_search
    }
  }
}
    `;

/**
 * __useAccommodationFilterListQuery__
 *
 * To run a query within a Vue component, call `useAccommodationFilterListQuery` and pass it any options that fit your needs.
 * When your component renders, `useAccommodationFilterListQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useAccommodationFilterListQuery({
 *   data: // value for 'data'
 * });
 */
export function useAccommodationFilterListQuery(variables: AccommodationFilterListQueryVariables | VueCompositionApi.Ref<AccommodationFilterListQueryVariables> | ReactiveFunction<AccommodationFilterListQueryVariables>, options: VueApolloComposable.UseQueryOptions<AccommodationFilterListQuery, AccommodationFilterListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<AccommodationFilterListQuery, AccommodationFilterListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<AccommodationFilterListQuery, AccommodationFilterListQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<AccommodationFilterListQuery, AccommodationFilterListQueryVariables>(AccommodationFilterListDocument, variables, options);
}
export function useAccommodationFilterListLazyQuery(variables: AccommodationFilterListQueryVariables | VueCompositionApi.Ref<AccommodationFilterListQueryVariables> | ReactiveFunction<AccommodationFilterListQueryVariables>, options: VueApolloComposable.UseQueryOptions<AccommodationFilterListQuery, AccommodationFilterListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<AccommodationFilterListQuery, AccommodationFilterListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<AccommodationFilterListQuery, AccommodationFilterListQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<AccommodationFilterListQuery, AccommodationFilterListQueryVariables>(AccommodationFilterListDocument, variables, options);
}
export type AccommodationFilterListQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<AccommodationFilterListQuery, AccommodationFilterListQueryVariables>;
export const AccommodationListDocument = gql`
    query AccommodationList($data: AccommodationListInputs!) {
  accommodationList(data: $data) {
    page {
      total
      perPage
      offset
      to
      lastPage
      currentPage
      from
      totalAfterFiltering
    }
    data {
      id
      price {
        provider_id
        property_id
        price
        default
      }
      title
      provider_id
      provider
      default
      chain
      number_of_rooms
      lnglat
      address
      link
      thumbnail
      star
      distance_from_search
      green_status
      recommended
      default_id
      default_price
      selected
      destination_id
    }
  }
}
    `;

/**
 * __useAccommodationListQuery__
 *
 * To run a query within a Vue component, call `useAccommodationListQuery` and pass it any options that fit your needs.
 * When your component renders, `useAccommodationListQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useAccommodationListQuery({
 *   data: // value for 'data'
 * });
 */
export function useAccommodationListQuery(variables: AccommodationListQueryVariables | VueCompositionApi.Ref<AccommodationListQueryVariables> | ReactiveFunction<AccommodationListQueryVariables>, options: VueApolloComposable.UseQueryOptions<AccommodationListQuery, AccommodationListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<AccommodationListQuery, AccommodationListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<AccommodationListQuery, AccommodationListQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<AccommodationListQuery, AccommodationListQueryVariables>(AccommodationListDocument, variables, options);
}
export function useAccommodationListLazyQuery(variables: AccommodationListQueryVariables | VueCompositionApi.Ref<AccommodationListQueryVariables> | ReactiveFunction<AccommodationListQueryVariables>, options: VueApolloComposable.UseQueryOptions<AccommodationListQuery, AccommodationListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<AccommodationListQuery, AccommodationListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<AccommodationListQuery, AccommodationListQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<AccommodationListQuery, AccommodationListQueryVariables>(AccommodationListDocument, variables, options);
}
export type AccommodationListQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<AccommodationListQuery, AccommodationListQueryVariables>;
export const SetAttendeeDocument = gql`
    mutation setAttendee($attendee: AttendeeInput!) {
  setAttendee(attendee: $attendee) {
    code
    message
    success
  }
}
    `;

/**
 * __useSetAttendeeMutation__
 *
 * To run a mutation, you first call `useSetAttendeeMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useSetAttendeeMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useSetAttendeeMutation({
 *   variables: {
 *     attendee: // value for 'attendee'
 *   },
 * });
 */
export function useSetAttendeeMutation(options: VueApolloComposable.UseMutationOptions<SetAttendeeMutation, SetAttendeeMutationVariables> | ReactiveFunction<VueApolloComposable.UseMutationOptions<SetAttendeeMutation, SetAttendeeMutationVariables>>) {
  return VueApolloComposable.useMutation<SetAttendeeMutation, SetAttendeeMutationVariables>(SetAttendeeDocument, options);
}
export type SetAttendeeMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<SetAttendeeMutation, SetAttendeeMutationVariables>;
export const CountryListDocument = gql`
    query CountryList($codes: String) {
  countryList(codes: $codes) {
    id
    name
    risk {
      risk_score
      risk_source
      risk_source_link
      risk_status
      risk_details {
        source
        source_iso_alpha2
        source_link
        score
        coverage
      }
    }
  }
}
    `;

/**
 * __useCountryListQuery__
 *
 * To run a query within a Vue component, call `useCountryListQuery` and pass it any options that fit your needs.
 * When your component renders, `useCountryListQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useCountryListQuery({
 *   codes: // value for 'codes'
 * });
 */
export function useCountryListQuery(variables: CountryListQueryVariables | VueCompositionApi.Ref<CountryListQueryVariables> | ReactiveFunction<CountryListQueryVariables> = {}, options: VueApolloComposable.UseQueryOptions<CountryListQuery, CountryListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<CountryListQuery, CountryListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<CountryListQuery, CountryListQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<CountryListQuery, CountryListQueryVariables>(CountryListDocument, variables, options);
}
export function useCountryListLazyQuery(variables: CountryListQueryVariables | VueCompositionApi.Ref<CountryListQueryVariables> | ReactiveFunction<CountryListQueryVariables> = {}, options: VueApolloComposable.UseQueryOptions<CountryListQuery, CountryListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<CountryListQuery, CountryListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<CountryListQuery, CountryListQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<CountryListQuery, CountryListQueryVariables>(CountryListDocument, variables, options);
}
export type CountryListQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<CountryListQuery, CountryListQueryVariables>;
export const SetDestinationDocument = gql`
    mutation SetDestination($destination: DestinationInput!) {
  setDestination(destination: $destination) {
    code
    message
    success
  }
}
    `;

/**
 * __useSetDestinationMutation__
 *
 * To run a mutation, you first call `useSetDestinationMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useSetDestinationMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useSetDestinationMutation({
 *   variables: {
 *     destination: // value for 'destination'
 *   },
 * });
 */
export function useSetDestinationMutation(options: VueApolloComposable.UseMutationOptions<SetDestinationMutation, SetDestinationMutationVariables> | ReactiveFunction<VueApolloComposable.UseMutationOptions<SetDestinationMutation, SetDestinationMutationVariables>>) {
  return VueApolloComposable.useMutation<SetDestinationMutation, SetDestinationMutationVariables>(SetDestinationDocument, options);
}
export type SetDestinationMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<SetDestinationMutation, SetDestinationMutationVariables>;
export const AnalysisProgressDocument = gql`
    query AnalysisProgress($meetingId: String!) {
  analysisProgress(meetingId: $meetingId) {
    analysis_status
    meeting_id
    destinations {
      destination_id
      accommodation_status {
        data {
          analysis_status
          type
        }
        analysis_status
      }
      analysis_status
      destination_id
      flights_status {
        data {
          analysis_status
          type
        }
        analysis_status
      }
    }
  }
}
    `;

/**
 * __useAnalysisProgressQuery__
 *
 * To run a query within a Vue component, call `useAnalysisProgressQuery` and pass it any options that fit your needs.
 * When your component renders, `useAnalysisProgressQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useAnalysisProgressQuery({
 *   meetingId: // value for 'meetingId'
 * });
 */
export function useAnalysisProgressQuery(variables: AnalysisProgressQueryVariables | VueCompositionApi.Ref<AnalysisProgressQueryVariables> | ReactiveFunction<AnalysisProgressQueryVariables>, options: VueApolloComposable.UseQueryOptions<AnalysisProgressQuery, AnalysisProgressQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<AnalysisProgressQuery, AnalysisProgressQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<AnalysisProgressQuery, AnalysisProgressQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<AnalysisProgressQuery, AnalysisProgressQueryVariables>(AnalysisProgressDocument, variables, options);
}
export function useAnalysisProgressLazyQuery(variables: AnalysisProgressQueryVariables | VueCompositionApi.Ref<AnalysisProgressQueryVariables> | ReactiveFunction<AnalysisProgressQueryVariables>, options: VueApolloComposable.UseQueryOptions<AnalysisProgressQuery, AnalysisProgressQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<AnalysisProgressQuery, AnalysisProgressQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<AnalysisProgressQuery, AnalysisProgressQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<AnalysisProgressQuery, AnalysisProgressQueryVariables>(AnalysisProgressDocument, variables, options);
}
export type AnalysisProgressQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<AnalysisProgressQuery, AnalysisProgressQueryVariables>;
export const AnalyzeDestinationDocument = gql`
    query analyzeDestination($meetingId: String!, $destinationId: String!, $userId: String!) {
  analyzeDestination(
    meetingId: $meetingId
    destinationId: $destinationId
    userId: $userId
  ) {
    code
    success
    message
    workflows
    args
  }
}
    `;

/**
 * __useAnalyzeDestinationQuery__
 *
 * To run a query within a Vue component, call `useAnalyzeDestinationQuery` and pass it any options that fit your needs.
 * When your component renders, `useAnalyzeDestinationQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useAnalyzeDestinationQuery({
 *   meetingId: // value for 'meetingId'
 *   destinationId: // value for 'destinationId'
 *   userId: // value for 'userId'
 * });
 */
export function useAnalyzeDestinationQuery(variables: AnalyzeDestinationQueryVariables | VueCompositionApi.Ref<AnalyzeDestinationQueryVariables> | ReactiveFunction<AnalyzeDestinationQueryVariables>, options: VueApolloComposable.UseQueryOptions<AnalyzeDestinationQuery, AnalyzeDestinationQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<AnalyzeDestinationQuery, AnalyzeDestinationQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<AnalyzeDestinationQuery, AnalyzeDestinationQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<AnalyzeDestinationQuery, AnalyzeDestinationQueryVariables>(AnalyzeDestinationDocument, variables, options);
}
export function useAnalyzeDestinationLazyQuery(variables: AnalyzeDestinationQueryVariables | VueCompositionApi.Ref<AnalyzeDestinationQueryVariables> | ReactiveFunction<AnalyzeDestinationQueryVariables>, options: VueApolloComposable.UseQueryOptions<AnalyzeDestinationQuery, AnalyzeDestinationQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<AnalyzeDestinationQuery, AnalyzeDestinationQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<AnalyzeDestinationQuery, AnalyzeDestinationQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<AnalyzeDestinationQuery, AnalyzeDestinationQueryVariables>(AnalyzeDestinationDocument, variables, options);
}
export type AnalyzeDestinationQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<AnalyzeDestinationQuery, AnalyzeDestinationQueryVariables>;
export const AnalyzeOriginDocument = gql`
    query AnalyzeOrigin($meetingId: String!, $userId: String!, $originId: String!) {
  analyzeOrigin(meetingId: $meetingId, userId: $userId, originId: $originId) {
    args
    code
    message
    success
    workflows
  }
}
    `;

/**
 * __useAnalyzeOriginQuery__
 *
 * To run a query within a Vue component, call `useAnalyzeOriginQuery` and pass it any options that fit your needs.
 * When your component renders, `useAnalyzeOriginQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useAnalyzeOriginQuery({
 *   meetingId: // value for 'meetingId'
 *   userId: // value for 'userId'
 *   originId: // value for 'originId'
 * });
 */
export function useAnalyzeOriginQuery(variables: AnalyzeOriginQueryVariables | VueCompositionApi.Ref<AnalyzeOriginQueryVariables> | ReactiveFunction<AnalyzeOriginQueryVariables>, options: VueApolloComposable.UseQueryOptions<AnalyzeOriginQuery, AnalyzeOriginQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<AnalyzeOriginQuery, AnalyzeOriginQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<AnalyzeOriginQuery, AnalyzeOriginQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<AnalyzeOriginQuery, AnalyzeOriginQueryVariables>(AnalyzeOriginDocument, variables, options);
}
export function useAnalyzeOriginLazyQuery(variables: AnalyzeOriginQueryVariables | VueCompositionApi.Ref<AnalyzeOriginQueryVariables> | ReactiveFunction<AnalyzeOriginQueryVariables>, options: VueApolloComposable.UseQueryOptions<AnalyzeOriginQuery, AnalyzeOriginQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<AnalyzeOriginQuery, AnalyzeOriginQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<AnalyzeOriginQuery, AnalyzeOriginQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<AnalyzeOriginQuery, AnalyzeOriginQueryVariables>(AnalyzeOriginDocument, variables, options);
}
export type AnalyzeOriginQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<AnalyzeOriginQuery, AnalyzeOriginQueryVariables>;
export const DestinationDetailDocument = gql`
    query DestinationDetail($placeSearchRequest: PlaceDetailsSearchRequest!) {
  placeDetail(placeSearchRequest: $placeSearchRequest) {
    visa_requirements {
      visa_required
      visa_requirement
      traveller_count
    }
    covid_rates {
      infection_rate
    }
  }
}
    `;

/**
 * __useDestinationDetailQuery__
 *
 * To run a query within a Vue component, call `useDestinationDetailQuery` and pass it any options that fit your needs.
 * When your component renders, `useDestinationDetailQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useDestinationDetailQuery({
 *   placeSearchRequest: // value for 'placeSearchRequest'
 * });
 */
export function useDestinationDetailQuery(variables: DestinationDetailQueryVariables | VueCompositionApi.Ref<DestinationDetailQueryVariables> | ReactiveFunction<DestinationDetailQueryVariables>, options: VueApolloComposable.UseQueryOptions<DestinationDetailQuery, DestinationDetailQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<DestinationDetailQuery, DestinationDetailQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<DestinationDetailQuery, DestinationDetailQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<DestinationDetailQuery, DestinationDetailQueryVariables>(DestinationDetailDocument, variables, options);
}
export function useDestinationDetailLazyQuery(variables: DestinationDetailQueryVariables | VueCompositionApi.Ref<DestinationDetailQueryVariables> | ReactiveFunction<DestinationDetailQueryVariables>, options: VueApolloComposable.UseQueryOptions<DestinationDetailQuery, DestinationDetailQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<DestinationDetailQuery, DestinationDetailQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<DestinationDetailQuery, DestinationDetailQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<DestinationDetailQuery, DestinationDetailQueryVariables>(DestinationDetailDocument, variables, options);
}
export type DestinationDetailQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<DestinationDetailQuery, DestinationDetailQueryVariables>;
export const DestinationListDocument = gql`
    query DestinationList($meetingId: String!) {
  destinationList(meetingId: $meetingId) {
    address
    airport_id
    airport_lnglat
    airport_name
    analysis_date
    analysis_status
    analysis_user {
      id
    }
    city_id
    city_sygic_image_id
    city_lnglat
    city_name
    baseline
    country_id
    country_lnglat
    country_name
    id
    lnglat
    meeting_id
    places_id
    risk {
      risk
    }
    weather {
      temp
    }
  }
}
    `;

/**
 * __useDestinationListQuery__
 *
 * To run a query within a Vue component, call `useDestinationListQuery` and pass it any options that fit your needs.
 * When your component renders, `useDestinationListQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useDestinationListQuery({
 *   meetingId: // value for 'meetingId'
 * });
 */
export function useDestinationListQuery(variables: DestinationListQueryVariables | VueCompositionApi.Ref<DestinationListQueryVariables> | ReactiveFunction<DestinationListQueryVariables>, options: VueApolloComposable.UseQueryOptions<DestinationListQuery, DestinationListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<DestinationListQuery, DestinationListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<DestinationListQuery, DestinationListQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<DestinationListQuery, DestinationListQueryVariables>(DestinationListDocument, variables, options);
}
export function useDestinationListLazyQuery(variables: DestinationListQueryVariables | VueCompositionApi.Ref<DestinationListQueryVariables> | ReactiveFunction<DestinationListQueryVariables>, options: VueApolloComposable.UseQueryOptions<DestinationListQuery, DestinationListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<DestinationListQuery, DestinationListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<DestinationListQuery, DestinationListQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<DestinationListQuery, DestinationListQueryVariables>(DestinationListDocument, variables, options);
}
export type DestinationListQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<DestinationListQuery, DestinationListQueryVariables>;
export const LocationSuggestionListDocument = gql`
    query LocationSuggestionList($locationsSearchRequest: LocationsSearchRequest!) {
  locationSuggestionList(locationsSearchRequest: $locationsSearchRequest) {
    cities {
      data {
        id
        type
        title
        sub_title
        address
        country_id
        country_name
        city_id
        city_name
        city_iata
        city_location
        city_lnglat
        city_sygic_image_id
        airport_id
        airport_name
        location
        distance
        icon {
          type
          name
        }
      }
    }
    airports {
      data {
        id
        type
        title
        sub_title
        address
        country_id
        country_name
        city_id
        city_name
        city_iata
        city_location
        city_lnglat
        city_sygic_image_id
        airport_id
        airport_name
        location
        distance
        icon {
          type
          name
        }
      }
    }
    offices {
      data {
        id
        type
        title
        sub_title
        address
        country_id
        country_name
        city_id
        city_name
        city_iata
        city_location
        city_lnglat
        city_sygic_image_id
        airport_id
        airport_name
        location
        distance
        icon {
          type
          name
        }
      }
    }
  }
}
    `;

/**
 * __useLocationSuggestionListQuery__
 *
 * To run a query within a Vue component, call `useLocationSuggestionListQuery` and pass it any options that fit your needs.
 * When your component renders, `useLocationSuggestionListQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useLocationSuggestionListQuery({
 *   locationsSearchRequest: // value for 'locationsSearchRequest'
 * });
 */
export function useLocationSuggestionListQuery(variables: LocationSuggestionListQueryVariables | VueCompositionApi.Ref<LocationSuggestionListQueryVariables> | ReactiveFunction<LocationSuggestionListQueryVariables>, options: VueApolloComposable.UseQueryOptions<LocationSuggestionListQuery, LocationSuggestionListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<LocationSuggestionListQuery, LocationSuggestionListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<LocationSuggestionListQuery, LocationSuggestionListQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<LocationSuggestionListQuery, LocationSuggestionListQueryVariables>(LocationSuggestionListDocument, variables, options);
}
export function useLocationSuggestionListLazyQuery(variables: LocationSuggestionListQueryVariables | VueCompositionApi.Ref<LocationSuggestionListQueryVariables> | ReactiveFunction<LocationSuggestionListQueryVariables>, options: VueApolloComposable.UseQueryOptions<LocationSuggestionListQuery, LocationSuggestionListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<LocationSuggestionListQuery, LocationSuggestionListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<LocationSuggestionListQuery, LocationSuggestionListQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<LocationSuggestionListQuery, LocationSuggestionListQueryVariables>(LocationSuggestionListDocument, variables, options);
}
export type LocationSuggestionListQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<LocationSuggestionListQuery, LocationSuggestionListQueryVariables>;
export const MeetingAnalysisDocument = gql`
    query MeetingAnalysis($meetingId: String!, $organizationId: String) {
  meetingAnalysis(meetingId: $meetingId, organizationId: $organizationId) {
    analysis_date
    meeting {
      analysis_date
      analysis_user {
        email
        id
        first_name
        last_name
      }
      created
      end_date
      end_time
      id
      meeting_policy_id
      name
      organization_id
      owner {
        id
        first_name
        last_name
        email
      }
      start_date
      start_time
      updated
    }
    origins {
      address
      airport_id
      airport_lnglat
      airport_name
      attendees {
        list {
          id
          fully_vaccinated_covid
          nationality
        }
        count
      }
      city_id
      city_lnglat
      city_name
      country_id
      country_lnglat
      country_name
      id
      lnglat
      meeting_id
      status
      timeZone {
        time_zone
      }
    }
    destinations {
      accommodation {
        cost_avg
        price_avg
      }
      address
      airport_id
      airport_lnglat
      airport_name
      analysis_date
      analysis_status
      analysis_user {
        id
        first_name
        last_name
        email
      }
      baseline
      city_id
      city_lnglat
      city_name
      offices {
        count
      }
      country_id
      country_lnglat
      country_name
      covid19 {
        data {
          infection_rate
        }
      }
      id
      lnglat
      meeting_id
      risk {
        risk
      }
      timeZone {
        time_zone
      }
      weather {
        temp
      }
      travel {
        summary {
          totalCost
          totalDuration
          totalEmissions
          totalStops
          totalTravellers
        }
      }
      city_sygic_image_id
      meetingSpaces {
        page {
          total
        }
      }
    }
    number_of_travellers
  }
}
    `;

/**
 * __useMeetingAnalysisQuery__
 *
 * To run a query within a Vue component, call `useMeetingAnalysisQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeetingAnalysisQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useMeetingAnalysisQuery({
 *   meetingId: // value for 'meetingId'
 *   organizationId: // value for 'organizationId'
 * });
 */
export function useMeetingAnalysisQuery(variables: MeetingAnalysisQueryVariables | VueCompositionApi.Ref<MeetingAnalysisQueryVariables> | ReactiveFunction<MeetingAnalysisQueryVariables>, options: VueApolloComposable.UseQueryOptions<MeetingAnalysisQuery, MeetingAnalysisQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<MeetingAnalysisQuery, MeetingAnalysisQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<MeetingAnalysisQuery, MeetingAnalysisQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<MeetingAnalysisQuery, MeetingAnalysisQueryVariables>(MeetingAnalysisDocument, variables, options);
}
export function useMeetingAnalysisLazyQuery(variables: MeetingAnalysisQueryVariables | VueCompositionApi.Ref<MeetingAnalysisQueryVariables> | ReactiveFunction<MeetingAnalysisQueryVariables>, options: VueApolloComposable.UseQueryOptions<MeetingAnalysisQuery, MeetingAnalysisQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<MeetingAnalysisQuery, MeetingAnalysisQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<MeetingAnalysisQuery, MeetingAnalysisQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<MeetingAnalysisQuery, MeetingAnalysisQueryVariables>(MeetingAnalysisDocument, variables, options);
}
export type MeetingAnalysisQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<MeetingAnalysisQuery, MeetingAnalysisQueryVariables>;
export const MeetingAnalysisDetailDocument = gql`
    query MeetingAnalysisDetail($meetingId: String!, $organizationId: String!, $destinationId: String!) {
  meetingAnalysisDetail(
    meetingId: $meetingId
    organizationId: $organizationId
    destinationId: $destinationId
  ) {
    id
    country_id
    country_name
    visas {
      data {
        from_country
        to_country
        traveller_count
        visa_requirement
        visa_titles
        visa_durations
        visa_required
      }
      from_countries
      origin_id
      to_country
    }
    travel {
      data {
        query_id
        cabin_class
        cost
        travellerCount
        timezoneDiff
        supplier
        inboundLeg {
          carrier
          arrival
          carrier_code
          carrier_img
          carrier_name
          departure
          destinationAirport
          destinationAirportName
          destinationCityCode
          destinationCityName
          destinationCountryCode
          destinationCountryName
          destination_code
          directionality
          distance
          duration
          emissions
          flight_number
          id
          journey_mode
          leg_id
          operating_carrier
          operating_carrier_code
          originAirport
          originAirportName
          originCityCode
          originCityName
          originCountryCode
          originCountryName
          origin_code
          query_id
          segment_id
          stop_duration
        }
        inbound_carrier_img
        inbound_arrival
        inbound_departure
        inbound_duration
        inbound_flight_duration
        inbound_emissions
        inbound_in_policy
        inbound_leg_id
        inbound_legs
        inbound_max_stop_risk
        inbound_stops
        itinerary_id
        outboundLeg {
          arrival
          carrier_code
          carrier_img
          carrier_name
          departure
          destinationAirport
          destinationAirportName
          destinationCityCode
          destinationCityName
          destinationCountryCode
          destinationCountryName
          destination_code
          directionality
          distance
          duration
          emissions
          flight_number
          id
          journey_mode
          leg_id
          operating_carrier
          operating_carrier_code
          originAirport
          originAirportName
          originCityCode
          originCityName
          originCountryCode
          originCountryName
          origin_code
          query_id
          segment_id
          stop_duration
          carrier
        }
        outbound_carrier_img
        outbound_arrival
        outbound_departure
        outbound_duration
        outbound_emissions
        outbound_flight_duration
        outbound_in_policy
        outbound_leg_id
        outbound_legs
        outbound_max_stop_risk
        outbound_stops
        originCityName
        originCountryId
        originCountryName
        originCountryflag
        originId
        total_stops
        total_emissions
        total_legs
        total_duration
      }
      destination_id
    }
    origins {
      id
      airport_id
      airport_name
      city_id
      city_name
      country_id
      country_name
      attendees {
        list {
          fully_vaccinated_covid
          id
          nationality
        }
        count
      }
    }
    accommodation {
      id
      provider
      provider_id
      title
      price {
        provider_id
        property_id
        price
        default
      }
      default_price
      default
      chain
      number_of_rooms
      lnglat
      address
      link
      thumbnail
      star
      distance_from_search
      green_status
      recommended
      default_id
    }
  }
}
    `;

/**
 * __useMeetingAnalysisDetailQuery__
 *
 * To run a query within a Vue component, call `useMeetingAnalysisDetailQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeetingAnalysisDetailQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useMeetingAnalysisDetailQuery({
 *   meetingId: // value for 'meetingId'
 *   organizationId: // value for 'organizationId'
 *   destinationId: // value for 'destinationId'
 * });
 */
export function useMeetingAnalysisDetailQuery(variables: MeetingAnalysisDetailQueryVariables | VueCompositionApi.Ref<MeetingAnalysisDetailQueryVariables> | ReactiveFunction<MeetingAnalysisDetailQueryVariables>, options: VueApolloComposable.UseQueryOptions<MeetingAnalysisDetailQuery, MeetingAnalysisDetailQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<MeetingAnalysisDetailQuery, MeetingAnalysisDetailQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<MeetingAnalysisDetailQuery, MeetingAnalysisDetailQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<MeetingAnalysisDetailQuery, MeetingAnalysisDetailQueryVariables>(MeetingAnalysisDetailDocument, variables, options);
}
export function useMeetingAnalysisDetailLazyQuery(variables: MeetingAnalysisDetailQueryVariables | VueCompositionApi.Ref<MeetingAnalysisDetailQueryVariables> | ReactiveFunction<MeetingAnalysisDetailQueryVariables>, options: VueApolloComposable.UseQueryOptions<MeetingAnalysisDetailQuery, MeetingAnalysisDetailQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<MeetingAnalysisDetailQuery, MeetingAnalysisDetailQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<MeetingAnalysisDetailQuery, MeetingAnalysisDetailQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<MeetingAnalysisDetailQuery, MeetingAnalysisDetailQueryVariables>(MeetingAnalysisDetailDocument, variables, options);
}
export type MeetingAnalysisDetailQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<MeetingAnalysisDetailQuery, MeetingAnalysisDetailQueryVariables>;
export const MeetingSpaceDocument = gql`
    query MeetingSpace($meetingSpaceId: String!, $organizationId: String!) {
  meetingSpace(meetingSpaceId: $meetingSpaceId, organizationId: $organizationId) {
    address
    address_city
    address_country_or_region
    address_postal_code
    audio_device_name
    booking_type
    building
    capacity
    city_code
    country_code
    organization_id
    display_device_name
    display_name
    email_address
    floor_label
    floor_number
    geo_coordinates
    graph_user_id
    id
    is_wheel_chair_accessable
    label
    lat
    lng
    nickname
    phone
    photos
    tags
    video_device_name
  }
}
    `;

/**
 * __useMeetingSpaceQuery__
 *
 * To run a query within a Vue component, call `useMeetingSpaceQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeetingSpaceQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useMeetingSpaceQuery({
 *   meetingSpaceId: // value for 'meetingSpaceId'
 *   organizationId: // value for 'organizationId'
 * });
 */
export function useMeetingSpaceQuery(variables: MeetingSpaceQueryVariables | VueCompositionApi.Ref<MeetingSpaceQueryVariables> | ReactiveFunction<MeetingSpaceQueryVariables>, options: VueApolloComposable.UseQueryOptions<MeetingSpaceQuery, MeetingSpaceQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<MeetingSpaceQuery, MeetingSpaceQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<MeetingSpaceQuery, MeetingSpaceQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<MeetingSpaceQuery, MeetingSpaceQueryVariables>(MeetingSpaceDocument, variables, options);
}
export function useMeetingSpaceLazyQuery(variables: MeetingSpaceQueryVariables | VueCompositionApi.Ref<MeetingSpaceQueryVariables> | ReactiveFunction<MeetingSpaceQueryVariables>, options: VueApolloComposable.UseQueryOptions<MeetingSpaceQuery, MeetingSpaceQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<MeetingSpaceQuery, MeetingSpaceQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<MeetingSpaceQuery, MeetingSpaceQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<MeetingSpaceQuery, MeetingSpaceQueryVariables>(MeetingSpaceDocument, variables, options);
}
export type MeetingSpaceQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<MeetingSpaceQuery, MeetingSpaceQueryVariables>;
export const MeetingSpaceListDocument = gql`
    query MeetingSpaceList($organizationId: String!, $placeId: String, $perPage: Int, $currentPage: Int) {
  meetingSpaceList(
    organizationId: $organizationId
    placeId: $placeId
    perPage: $perPage
    currentPage: $currentPage
  ) {
    organization_id
    city_code
    data {
      id
      graph_user_id
      organization_id
      display_name
      geo_coordinates
      phone
      nickname
      email_address
      building
      floor_number
      floor_label
      label
      capacity
      booking_type
      audio_device_name
      video_device_name
      display_device_name
      is_wheel_chair_accessable
      tags
      address
      address_city
      address_postal_code
      address_country_or_region
      lat
      lng
      city_code
      country_code
      photos
      rooms {
        id
        graph_user_id
        organization_id
        display_name
        geo_coordinates
        phone
        nickname
        email_address
        building
        floor_number
        floor_label
        label
        capacity
        booking_type
        audio_device_name
        video_device_name
        display_device_name
        is_wheel_chair_accessable
        tags
        address
        address_city
        address_postal_code
        address_country_or_region
        lat
        lng
        city_code
        country_code
        photos
        schedule
      }
      roomCount
    }
    page {
      from
      currentPage
      lastPage
      to
      offset
      perPage
      total
    }
  }
}
    `;

/**
 * __useMeetingSpaceListQuery__
 *
 * To run a query within a Vue component, call `useMeetingSpaceListQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeetingSpaceListQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useMeetingSpaceListQuery({
 *   organizationId: // value for 'organizationId'
 *   placeId: // value for 'placeId'
 *   perPage: // value for 'perPage'
 *   currentPage: // value for 'currentPage'
 * });
 */
export function useMeetingSpaceListQuery(variables: MeetingSpaceListQueryVariables | VueCompositionApi.Ref<MeetingSpaceListQueryVariables> | ReactiveFunction<MeetingSpaceListQueryVariables>, options: VueApolloComposable.UseQueryOptions<MeetingSpaceListQuery, MeetingSpaceListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<MeetingSpaceListQuery, MeetingSpaceListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<MeetingSpaceListQuery, MeetingSpaceListQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<MeetingSpaceListQuery, MeetingSpaceListQueryVariables>(MeetingSpaceListDocument, variables, options);
}
export function useMeetingSpaceListLazyQuery(variables: MeetingSpaceListQueryVariables | VueCompositionApi.Ref<MeetingSpaceListQueryVariables> | ReactiveFunction<MeetingSpaceListQueryVariables>, options: VueApolloComposable.UseQueryOptions<MeetingSpaceListQuery, MeetingSpaceListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<MeetingSpaceListQuery, MeetingSpaceListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<MeetingSpaceListQuery, MeetingSpaceListQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<MeetingSpaceListQuery, MeetingSpaceListQueryVariables>(MeetingSpaceListDocument, variables, options);
}
export type MeetingSpaceListQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<MeetingSpaceListQuery, MeetingSpaceListQueryVariables>;
export const SetMeetingDocument = gql`
    mutation SetMeeting($meeting: MeetingInput!) {
  setMeeting(meeting: $meeting) {
    code
    data {
      id
      name
    }
    message
    success
  }
}
    `;

/**
 * __useSetMeetingMutation__
 *
 * To run a mutation, you first call `useSetMeetingMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useSetMeetingMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useSetMeetingMutation({
 *   variables: {
 *     meeting: // value for 'meeting'
 *   },
 * });
 */
export function useSetMeetingMutation(options: VueApolloComposable.UseMutationOptions<SetMeetingMutation, SetMeetingMutationVariables> | ReactiveFunction<VueApolloComposable.UseMutationOptions<SetMeetingMutation, SetMeetingMutationVariables>>) {
  return VueApolloComposable.useMutation<SetMeetingMutation, SetMeetingMutationVariables>(SetMeetingDocument, options);
}
export type SetMeetingMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<SetMeetingMutation, SetMeetingMutationVariables>;
export const MeetingBasicWithSummaryDocument = gql`
    query MeetingBasicWithSummary($meetingId: String!) {
  meeting(meetingId: $meetingId) {
    data {
      ...MeetingBasic
      destinations {
        id
        lnglat
        analysis_status
        baseline
      }
      origins {
        id
        lnglat
        attendees {
          count
        }
        country_id
        city_id
      }
    }
  }
}
    ${MeetingBasicFragmentDoc}`;

/**
 * __useMeetingBasicWithSummaryQuery__
 *
 * To run a query within a Vue component, call `useMeetingBasicWithSummaryQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeetingBasicWithSummaryQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useMeetingBasicWithSummaryQuery({
 *   meetingId: // value for 'meetingId'
 * });
 */
export function useMeetingBasicWithSummaryQuery(variables: MeetingBasicWithSummaryQueryVariables | VueCompositionApi.Ref<MeetingBasicWithSummaryQueryVariables> | ReactiveFunction<MeetingBasicWithSummaryQueryVariables>, options: VueApolloComposable.UseQueryOptions<MeetingBasicWithSummaryQuery, MeetingBasicWithSummaryQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<MeetingBasicWithSummaryQuery, MeetingBasicWithSummaryQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<MeetingBasicWithSummaryQuery, MeetingBasicWithSummaryQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<MeetingBasicWithSummaryQuery, MeetingBasicWithSummaryQueryVariables>(MeetingBasicWithSummaryDocument, variables, options);
}
export function useMeetingBasicWithSummaryLazyQuery(variables: MeetingBasicWithSummaryQueryVariables | VueCompositionApi.Ref<MeetingBasicWithSummaryQueryVariables> | ReactiveFunction<MeetingBasicWithSummaryQueryVariables>, options: VueApolloComposable.UseQueryOptions<MeetingBasicWithSummaryQuery, MeetingBasicWithSummaryQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<MeetingBasicWithSummaryQuery, MeetingBasicWithSummaryQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<MeetingBasicWithSummaryQuery, MeetingBasicWithSummaryQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<MeetingBasicWithSummaryQuery, MeetingBasicWithSummaryQueryVariables>(MeetingBasicWithSummaryDocument, variables, options);
}
export type MeetingBasicWithSummaryQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<MeetingBasicWithSummaryQuery, MeetingBasicWithSummaryQueryVariables>;
export const MeetingListDocument = gql`
    query MeetingList($currentPage: Int, $organizationId: String, $perPage: Int, $userId: String) {
  meetingList(
    currentPage: $currentPage
    organizationId: $organizationId
    perPage: $perPage
    userId: $userId
  ) {
    data {
      id
      name
      owner {
        id
        email
      }
      updated
      start_date
      start_time
      end_date
      end_time
      type
      attendees {
        id
      }
      origins {
        id
      }
      destinations {
        id
      }
    }
    page {
      currentPage
      from
      lastPage
      offset
      perPage
      to
      total
    }
  }
}
    `;

/**
 * __useMeetingListQuery__
 *
 * To run a query within a Vue component, call `useMeetingListQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeetingListQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useMeetingListQuery({
 *   currentPage: // value for 'currentPage'
 *   organizationId: // value for 'organizationId'
 *   perPage: // value for 'perPage'
 *   userId: // value for 'userId'
 * });
 */
export function useMeetingListQuery(variables: MeetingListQueryVariables | VueCompositionApi.Ref<MeetingListQueryVariables> | ReactiveFunction<MeetingListQueryVariables> = {}, options: VueApolloComposable.UseQueryOptions<MeetingListQuery, MeetingListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<MeetingListQuery, MeetingListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<MeetingListQuery, MeetingListQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<MeetingListQuery, MeetingListQueryVariables>(MeetingListDocument, variables, options);
}
export function useMeetingListLazyQuery(variables: MeetingListQueryVariables | VueCompositionApi.Ref<MeetingListQueryVariables> | ReactiveFunction<MeetingListQueryVariables> = {}, options: VueApolloComposable.UseQueryOptions<MeetingListQuery, MeetingListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<MeetingListQuery, MeetingListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<MeetingListQuery, MeetingListQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<MeetingListQuery, MeetingListQueryVariables>(MeetingListDocument, variables, options);
}
export type MeetingListQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<MeetingListQuery, MeetingListQueryVariables>;
export const OfficeDocument = gql`
    query Office($officeId: ID!) {
  office(officeId: $officeId) {
    address
    city_code
    organization_id
    closest_int_airport
    country_code
    lnglat
    name
    selected_city_distance
    type
  }
}
    `;

/**
 * __useOfficeQuery__
 *
 * To run a query within a Vue component, call `useOfficeQuery` and pass it any options that fit your needs.
 * When your component renders, `useOfficeQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useOfficeQuery({
 *   officeId: // value for 'officeId'
 * });
 */
export function useOfficeQuery(variables: OfficeQueryVariables | VueCompositionApi.Ref<OfficeQueryVariables> | ReactiveFunction<OfficeQueryVariables>, options: VueApolloComposable.UseQueryOptions<OfficeQuery, OfficeQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<OfficeQuery, OfficeQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<OfficeQuery, OfficeQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<OfficeQuery, OfficeQueryVariables>(OfficeDocument, variables, options);
}
export function useOfficeLazyQuery(variables: OfficeQueryVariables | VueCompositionApi.Ref<OfficeQueryVariables> | ReactiveFunction<OfficeQueryVariables>, options: VueApolloComposable.UseQueryOptions<OfficeQuery, OfficeQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<OfficeQuery, OfficeQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<OfficeQuery, OfficeQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<OfficeQuery, OfficeQueryVariables>(OfficeDocument, variables, options);
}
export type OfficeQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<OfficeQuery, OfficeQueryVariables>;
export const OfficeListDocument = gql`
    query OfficeList($placeId: String, $organizationId: ID!) {
  officeList(placeId: $placeId, organizationId: $organizationId) {
    city_code
    organization_id
    count
    data {
      address
      city_code
      organization_id
      closest_int_airport
      country_code
      lnglat
      name
      selected_city_distance
      type
    }
  }
}
    `;

/**
 * __useOfficeListQuery__
 *
 * To run a query within a Vue component, call `useOfficeListQuery` and pass it any options that fit your needs.
 * When your component renders, `useOfficeListQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useOfficeListQuery({
 *   placeId: // value for 'placeId'
 *   organizationId: // value for 'organizationId'
 * });
 */
export function useOfficeListQuery(variables: OfficeListQueryVariables | VueCompositionApi.Ref<OfficeListQueryVariables> | ReactiveFunction<OfficeListQueryVariables>, options: VueApolloComposable.UseQueryOptions<OfficeListQuery, OfficeListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<OfficeListQuery, OfficeListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<OfficeListQuery, OfficeListQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<OfficeListQuery, OfficeListQueryVariables>(OfficeListDocument, variables, options);
}
export function useOfficeListLazyQuery(variables: OfficeListQueryVariables | VueCompositionApi.Ref<OfficeListQueryVariables> | ReactiveFunction<OfficeListQueryVariables>, options: VueApolloComposable.UseQueryOptions<OfficeListQuery, OfficeListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<OfficeListQuery, OfficeListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<OfficeListQuery, OfficeListQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<OfficeListQuery, OfficeListQueryVariables>(OfficeListDocument, variables, options);
}
export type OfficeListQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<OfficeListQuery, OfficeListQueryVariables>;
export const SetOriginDocument = gql`
    mutation SetOrigin($origin: OriginInput!) {
  setOrigin(origin: $origin) {
    code
    data {
      count
      origin {
        airport_lnglat
        airport_name
        city_id
        city_lnglat
        city_name
        country_id
        country_lnglat
        country_name
        id
        lnglat
        meeting_id
        attendees {
          count
          list {
            id
            nationality
            fully_vaccinated_covid
          }
        }
        status
      }
    }
    message
    success
  }
}
    `;

/**
 * __useSetOriginMutation__
 *
 * To run a mutation, you first call `useSetOriginMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useSetOriginMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useSetOriginMutation({
 *   variables: {
 *     origin: // value for 'origin'
 *   },
 * });
 */
export function useSetOriginMutation(options: VueApolloComposable.UseMutationOptions<SetOriginMutation, SetOriginMutationVariables> | ReactiveFunction<VueApolloComposable.UseMutationOptions<SetOriginMutation, SetOriginMutationVariables>>) {
  return VueApolloComposable.useMutation<SetOriginMutation, SetOriginMutationVariables>(SetOriginDocument, options);
}
export type SetOriginMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<SetOriginMutation, SetOriginMutationVariables>;
export const OriginListDocument = gql`
    query OriginList($meetingId: String!) {
  originList(meetingId: $meetingId) {
    address
    airport_id
    airport_lnglat
    airport_name
    city_id
    city_lnglat
    city_name
    country_id
    country_lnglat
    country_name
    id
    lnglat
    attendees {
      count
      list {
        fully_vaccinated_covid
        id
        nationality
        email
      }
    }
    timeZone {
      city_id
      time_zone
    }
  }
}
    `;

/**
 * __useOriginListQuery__
 *
 * To run a query within a Vue component, call `useOriginListQuery` and pass it any options that fit your needs.
 * When your component renders, `useOriginListQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useOriginListQuery({
 *   meetingId: // value for 'meetingId'
 * });
 */
export function useOriginListQuery(variables: OriginListQueryVariables | VueCompositionApi.Ref<OriginListQueryVariables> | ReactiveFunction<OriginListQueryVariables>, options: VueApolloComposable.UseQueryOptions<OriginListQuery, OriginListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<OriginListQuery, OriginListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<OriginListQuery, OriginListQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<OriginListQuery, OriginListQueryVariables>(OriginListDocument, variables, options);
}
export function useOriginListLazyQuery(variables: OriginListQueryVariables | VueCompositionApi.Ref<OriginListQueryVariables> | ReactiveFunction<OriginListQueryVariables>, options: VueApolloComposable.UseQueryOptions<OriginListQuery, OriginListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<OriginListQuery, OriginListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<OriginListQuery, OriginListQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<OriginListQuery, OriginListQueryVariables>(OriginListDocument, variables, options);
}
export type OriginListQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<OriginListQuery, OriginListQueryVariables>;
export const CovidPolicyListDocument = gql`
    query CovidPolicyList($countries: String!, $includedOrigins: String, $startDate: String!, $endDate: String!) {
  covidPolicyList(
    countries: $countries
    includedOrigins: $includedOrigins
    startDate: $startDate
    endDate: $endDate
  ) {
    inbound {
      restrictions {
        id
        destination_country_code
        destination_region_code
        origin_count
        included
        region_included
        category
        subcategory
        severity
        title
        description
        tags
        source {
          url
          title
          sourceType
        }
        last_checked_at
        last_updated_at
        still_current_at
        created_at
        start_date
        end_date
        tt_updated_at
      }
      procedures {
        id
        destination_country_code
        destination_region_code
        origin_count
        included
        region_included
        category
        subcategory
        enforcement
        severity
        title
        description
        tags
        source {
          url
          title
          sourceType
        }
        last_checked_at
        last_updated_at
        still_current_at
        created_at
        start_date
        end_date
        tt_updated_at
      }
    }
    outbound {
      restrictions {
        id
        destination_country_code
        destination_region_code
        origin_count
        included
        region_included
        category
        subcategory
        severity
        title
        description
        tags
        source {
          url
          title
          sourceType
        }
        last_checked_at
        last_updated_at
        still_current_at
        created_at
        start_date
        end_date
        tt_updated_at
        documentLinks {
          type
          url
          title
        }
      }
      procedures {
        id
        destination_country_code
        destination_region_code
        origin_count
        included
        region_included
        category
        subcategory
        enforcement
        severity
        title
        description
        tags
        source {
          url
          title
          sourceType
        }
        last_checked_at
        last_updated_at
        still_current_at
        created_at
        start_date
        end_date
        tt_updated_at
        documentLinks {
          type
          url
          title
        }
      }
    }
  }
}
    `;

/**
 * __useCovidPolicyListQuery__
 *
 * To run a query within a Vue component, call `useCovidPolicyListQuery` and pass it any options that fit your needs.
 * When your component renders, `useCovidPolicyListQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useCovidPolicyListQuery({
 *   countries: // value for 'countries'
 *   includedOrigins: // value for 'includedOrigins'
 *   startDate: // value for 'startDate'
 *   endDate: // value for 'endDate'
 * });
 */
export function useCovidPolicyListQuery(variables: CovidPolicyListQueryVariables | VueCompositionApi.Ref<CovidPolicyListQueryVariables> | ReactiveFunction<CovidPolicyListQueryVariables>, options: VueApolloComposable.UseQueryOptions<CovidPolicyListQuery, CovidPolicyListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<CovidPolicyListQuery, CovidPolicyListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<CovidPolicyListQuery, CovidPolicyListQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<CovidPolicyListQuery, CovidPolicyListQueryVariables>(CovidPolicyListDocument, variables, options);
}
export function useCovidPolicyListLazyQuery(variables: CovidPolicyListQueryVariables | VueCompositionApi.Ref<CovidPolicyListQueryVariables> | ReactiveFunction<CovidPolicyListQueryVariables>, options: VueApolloComposable.UseQueryOptions<CovidPolicyListQuery, CovidPolicyListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<CovidPolicyListQuery, CovidPolicyListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<CovidPolicyListQuery, CovidPolicyListQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<CovidPolicyListQuery, CovidPolicyListQueryVariables>(CovidPolicyListDocument, variables, options);
}
export type CovidPolicyListQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<CovidPolicyListQuery, CovidPolicyListQueryVariables>;
export const PlaceSuggestionListDocument = gql`
    query PlaceSuggestionList($placeSuggestionsRequest: PlaceSuggestionsRequest!) {
  placeSuggestionList(placeSuggestionsRequest: $placeSuggestionsRequest) {
    uuid
    avgtemp
    country_code
    dist
    iata_code
    is_international
    lat
    lng
    name
    offices
    ranking
    region
    risk
    sygic_image_id
    sygic_rating
    time_zone
  }
}
    `;

/**
 * __usePlaceSuggestionListQuery__
 *
 * To run a query within a Vue component, call `usePlaceSuggestionListQuery` and pass it any options that fit your needs.
 * When your component renders, `usePlaceSuggestionListQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = usePlaceSuggestionListQuery({
 *   placeSuggestionsRequest: // value for 'placeSuggestionsRequest'
 * });
 */
export function usePlaceSuggestionListQuery(variables: PlaceSuggestionListQueryVariables | VueCompositionApi.Ref<PlaceSuggestionListQueryVariables> | ReactiveFunction<PlaceSuggestionListQueryVariables>, options: VueApolloComposable.UseQueryOptions<PlaceSuggestionListQuery, PlaceSuggestionListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<PlaceSuggestionListQuery, PlaceSuggestionListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<PlaceSuggestionListQuery, PlaceSuggestionListQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<PlaceSuggestionListQuery, PlaceSuggestionListQueryVariables>(PlaceSuggestionListDocument, variables, options);
}
export function usePlaceSuggestionListLazyQuery(variables: PlaceSuggestionListQueryVariables | VueCompositionApi.Ref<PlaceSuggestionListQueryVariables> | ReactiveFunction<PlaceSuggestionListQueryVariables>, options: VueApolloComposable.UseQueryOptions<PlaceSuggestionListQuery, PlaceSuggestionListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<PlaceSuggestionListQuery, PlaceSuggestionListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<PlaceSuggestionListQuery, PlaceSuggestionListQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<PlaceSuggestionListQuery, PlaceSuggestionListQueryVariables>(PlaceSuggestionListDocument, variables, options);
}
export type PlaceSuggestionListQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<PlaceSuggestionListQuery, PlaceSuggestionListQueryVariables>;
export const PlaceDetailDocument = gql`
    query PlaceDetail($placeSearchRequest: PlaceDetailsSearchRequest!) {
  placeDetail(placeSearchRequest: $placeSearchRequest) {
    city {
      country_summary
      city_code
      city_country_code
      city_airports
      city_g_json
      city_g_place_id
      city_g_rating
      city_g_region_id
      city_g_region_name
      city_iata_code
      city_is_international
      city_lat
      city_lng
      city_name
      city_ranking
      city_summary_text
      city_single_airport_city
      city_ss_region_code
      city_ss_id
      city_ss_updated_timestamp
      city_sygic_id
      city_sygic_image_id
      city_sygic_rating
      city_time_zone
      city_time_zone_id
      country_cases_active
      country_cases_as_of_date
      country_cases_confirmed
      country_cases_deaths
      country_cases_recovered
      country_code
      country_covid_risk_level
      country_infection_as_of_date
      country_infection_level
      country_infection_rate
      country_it_entry_and_borders_entry_ban
      country_it_entry_and_borders_entry_rules
      country_it_entry_and_borders_last_update
      country_it_entry_and_borders_exemptions
      country_it_entry_and_borders_through_date
      country_it_exit_country_exit_requirements
      country_it_entry_and_borders_text
      country_it_exit_country_last_update
      country_it_exit_country_requirement
      country_it_health_travel_doc_health_document
      country_it_health_travel_doc_last_update
      country_it_exit_country_text
      country_it_health_travel_doc_need_documentations
      country_it_health_travel_doc_text
      country_it_health_travel_doc_travel_document
      country_it_international_flights_is_banned
      country_it_international_flights_last_update
      country_it_international_flights_text
      country_it_international_flights_through_date
      country_it_masks_last_update
      country_it_masks_requirement
      country_it_masks_text
      country_it_quarantine_on_arrival_days
      country_it_quarantine_on_arrival_last_update
      country_it_quarantine_on_arrival_mandate_list
      country_it_quarantine_on_arrival_rules
      country_it_quarantine_on_arrival_text
      country_it_quarantine_on_arrival_type
      country_it_quarantine_on_arrival_who_needs
      country_it_testing_last_update
      country_it_testing_need_test
      country_it_testing_rules
      country_it_testing_requirement
      country_it_testing_text
      country_it_testing_when
      country_name
      country_policy_current_status
      country_policy_end_date
      country_policy_last_update
      country_policy_start_date
      country_risk_level
      country_travel_risk
      country_policy_text
    }
  }
}
    `;

/**
 * __usePlaceDetailQuery__
 *
 * To run a query within a Vue component, call `usePlaceDetailQuery` and pass it any options that fit your needs.
 * When your component renders, `usePlaceDetailQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = usePlaceDetailQuery({
 *   placeSearchRequest: // value for 'placeSearchRequest'
 * });
 */
export function usePlaceDetailQuery(variables: PlaceDetailQueryVariables | VueCompositionApi.Ref<PlaceDetailQueryVariables> | ReactiveFunction<PlaceDetailQueryVariables>, options: VueApolloComposable.UseQueryOptions<PlaceDetailQuery, PlaceDetailQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<PlaceDetailQuery, PlaceDetailQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<PlaceDetailQuery, PlaceDetailQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<PlaceDetailQuery, PlaceDetailQueryVariables>(PlaceDetailDocument, variables, options);
}
export function usePlaceDetailLazyQuery(variables: PlaceDetailQueryVariables | VueCompositionApi.Ref<PlaceDetailQueryVariables> | ReactiveFunction<PlaceDetailQueryVariables>, options: VueApolloComposable.UseQueryOptions<PlaceDetailQuery, PlaceDetailQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<PlaceDetailQuery, PlaceDetailQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<PlaceDetailQuery, PlaceDetailQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<PlaceDetailQuery, PlaceDetailQueryVariables>(PlaceDetailDocument, variables, options);
}
export type PlaceDetailQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<PlaceDetailQuery, PlaceDetailQueryVariables>;
export const PlaceRestrictionListDocument = gql`
    query PlaceRestrictionList($placeRestrictionsRequest: PlaceRestrictionsRequest!) {
  placeRestrictionList(placeRestrictionsRequest: $placeRestrictionsRequest) {
    vaccinated {
      country
      requireRestriction {
        id
        origin_country_code
        destination_country_code
        category
        subcategory
        tags
        title
        requireRestriction
      }
      requireQuarantine {
        id
        origin_country_code
        destination_country_code
        category
        subcategory
        tags
        title
        requireQuarantine
        quarantine_days
      }
      requireTest {
        id
        origin_country_code
        destination_country_code
        category
        subcategory
        tags
        title
        requireTest
        quarantine_days
      }
    }
    unvaccinated {
      country
      requireRestriction {
        id
        origin_country_code
        destination_country_code
        category
        subcategory
        tags
        title
        requireRestriction
      }
      requireQuarantine {
        id
        origin_country_code
        destination_country_code
        category
        subcategory
        tags
        title
        requireQuarantine
        quarantine_days
      }
      requireTest {
        id
        origin_country_code
        destination_country_code
        category
        subcategory
        tags
        title
        requireTest
        quarantine_days
      }
    }
  }
}
    `;

/**
 * __usePlaceRestrictionListQuery__
 *
 * To run a query within a Vue component, call `usePlaceRestrictionListQuery` and pass it any options that fit your needs.
 * When your component renders, `usePlaceRestrictionListQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = usePlaceRestrictionListQuery({
 *   placeRestrictionsRequest: // value for 'placeRestrictionsRequest'
 * });
 */
export function usePlaceRestrictionListQuery(variables: PlaceRestrictionListQueryVariables | VueCompositionApi.Ref<PlaceRestrictionListQueryVariables> | ReactiveFunction<PlaceRestrictionListQueryVariables>, options: VueApolloComposable.UseQueryOptions<PlaceRestrictionListQuery, PlaceRestrictionListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<PlaceRestrictionListQuery, PlaceRestrictionListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<PlaceRestrictionListQuery, PlaceRestrictionListQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<PlaceRestrictionListQuery, PlaceRestrictionListQueryVariables>(PlaceRestrictionListDocument, variables, options);
}
export function usePlaceRestrictionListLazyQuery(variables: PlaceRestrictionListQueryVariables | VueCompositionApi.Ref<PlaceRestrictionListQueryVariables> | ReactiveFunction<PlaceRestrictionListQueryVariables>, options: VueApolloComposable.UseQueryOptions<PlaceRestrictionListQuery, PlaceRestrictionListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<PlaceRestrictionListQuery, PlaceRestrictionListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<PlaceRestrictionListQuery, PlaceRestrictionListQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<PlaceRestrictionListQuery, PlaceRestrictionListQueryVariables>(PlaceRestrictionListDocument, variables, options);
}
export type PlaceRestrictionListQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<PlaceRestrictionListQuery, PlaceRestrictionListQueryVariables>;
export const SetTravelAnalysisDocument = gql`
    mutation SetTravelAnalysis($itineraryId: Int!) {
  setTravelAnalysis(itineraryId: $itineraryId) {
    api
    version
    result {
      data
      statusCode
      summary {
        statusCode
        data
      }
    }
    error {
      statusCode
      trace
      status
      message
      stackTrace
    }
  }
}
    `;

/**
 * __useSetTravelAnalysisMutation__
 *
 * To run a mutation, you first call `useSetTravelAnalysisMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useSetTravelAnalysisMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useSetTravelAnalysisMutation({
 *   variables: {
 *     itineraryId: // value for 'itineraryId'
 *   },
 * });
 */
export function useSetTravelAnalysisMutation(options: VueApolloComposable.UseMutationOptions<SetTravelAnalysisMutation, SetTravelAnalysisMutationVariables> | ReactiveFunction<VueApolloComposable.UseMutationOptions<SetTravelAnalysisMutation, SetTravelAnalysisMutationVariables>>) {
  return VueApolloComposable.useMutation<SetTravelAnalysisMutation, SetTravelAnalysisMutationVariables>(SetTravelAnalysisDocument, options);
}
export type SetTravelAnalysisMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<SetTravelAnalysisMutation, SetTravelAnalysisMutationVariables>;
export const FlightOptionDetailDocument = gql`
    query FlightOptionDetail($inboundLegId: String!, $outboundLegId: String!, $queryId: Int!) {
  flightOptionDetail(
    inboundLegId: $inboundLegId
    outboundLegId: $outboundLegId
    queryId: $queryId
  ) {
    api
    version
    result {
      inbound {
        arrival
        carrier
        carrier_name
        departure
        carrier_code
        carrier_img
        destinationAirportName
        destinationCountryName
        duration
        emissions
        flight_number
        originAirportName
        originCountryName
        stop_duration
      }
      outbound {
        arrival
        carrier
        carrier_name
        departure
        carrier_code
        carrier_img
        destinationAirportName
        destinationCountryName
        duration
        emissions
        flight_number
        originAirportName
        originCountryName
        stop_duration
      }
    }
  }
}
    `;

/**
 * __useFlightOptionDetailQuery__
 *
 * To run a query within a Vue component, call `useFlightOptionDetailQuery` and pass it any options that fit your needs.
 * When your component renders, `useFlightOptionDetailQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useFlightOptionDetailQuery({
 *   inboundLegId: // value for 'inboundLegId'
 *   outboundLegId: // value for 'outboundLegId'
 *   queryId: // value for 'queryId'
 * });
 */
export function useFlightOptionDetailQuery(variables: FlightOptionDetailQueryVariables | VueCompositionApi.Ref<FlightOptionDetailQueryVariables> | ReactiveFunction<FlightOptionDetailQueryVariables>, options: VueApolloComposable.UseQueryOptions<FlightOptionDetailQuery, FlightOptionDetailQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<FlightOptionDetailQuery, FlightOptionDetailQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<FlightOptionDetailQuery, FlightOptionDetailQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<FlightOptionDetailQuery, FlightOptionDetailQueryVariables>(FlightOptionDetailDocument, variables, options);
}
export function useFlightOptionDetailLazyQuery(variables: FlightOptionDetailQueryVariables | VueCompositionApi.Ref<FlightOptionDetailQueryVariables> | ReactiveFunction<FlightOptionDetailQueryVariables>, options: VueApolloComposable.UseQueryOptions<FlightOptionDetailQuery, FlightOptionDetailQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<FlightOptionDetailQuery, FlightOptionDetailQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<FlightOptionDetailQuery, FlightOptionDetailQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<FlightOptionDetailQuery, FlightOptionDetailQueryVariables>(FlightOptionDetailDocument, variables, options);
}
export type FlightOptionDetailQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<FlightOptionDetailQuery, FlightOptionDetailQueryVariables>;
export const FlightOptionFiltersDocument = gql`
    query FlightOptionFilters($meetingId: String!, $originId: String!, $destinationId: String!, $pagination: TravelPagination, $filters: FlightDataFilters) {
  flightOptionList(
    meetingId: $meetingId
    originId: $originId
    destinationId: $destinationId
    pagination: $pagination
    filters: $filters
  ) {
    data {
      total_stops
      total_duration
      total_emissions
      cost
      inbound_departure
      outbound_departure
      outbound_origin_airport_name
      outbound_origin_airport_code
      cabin_class
    }
  }
}
    `;

/**
 * __useFlightOptionFiltersQuery__
 *
 * To run a query within a Vue component, call `useFlightOptionFiltersQuery` and pass it any options that fit your needs.
 * When your component renders, `useFlightOptionFiltersQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useFlightOptionFiltersQuery({
 *   meetingId: // value for 'meetingId'
 *   originId: // value for 'originId'
 *   destinationId: // value for 'destinationId'
 *   pagination: // value for 'pagination'
 *   filters: // value for 'filters'
 * });
 */
export function useFlightOptionFiltersQuery(variables: FlightOptionFiltersQueryVariables | VueCompositionApi.Ref<FlightOptionFiltersQueryVariables> | ReactiveFunction<FlightOptionFiltersQueryVariables>, options: VueApolloComposable.UseQueryOptions<FlightOptionFiltersQuery, FlightOptionFiltersQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<FlightOptionFiltersQuery, FlightOptionFiltersQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<FlightOptionFiltersQuery, FlightOptionFiltersQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<FlightOptionFiltersQuery, FlightOptionFiltersQueryVariables>(FlightOptionFiltersDocument, variables, options);
}
export function useFlightOptionFiltersLazyQuery(variables: FlightOptionFiltersQueryVariables | VueCompositionApi.Ref<FlightOptionFiltersQueryVariables> | ReactiveFunction<FlightOptionFiltersQueryVariables>, options: VueApolloComposable.UseQueryOptions<FlightOptionFiltersQuery, FlightOptionFiltersQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<FlightOptionFiltersQuery, FlightOptionFiltersQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<FlightOptionFiltersQuery, FlightOptionFiltersQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<FlightOptionFiltersQuery, FlightOptionFiltersQueryVariables>(FlightOptionFiltersDocument, variables, options);
}
export type FlightOptionFiltersQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<FlightOptionFiltersQuery, FlightOptionFiltersQueryVariables>;
export const FlightOptionListDocument = gql`
    query FlightOptionList($meetingId: String!, $originId: String!, $destinationId: String!, $pagination: TravelPagination, $filters: FlightDataFilters) {
  flightOptionList(
    meetingId: $meetingId
    originId: $originId
    destinationId: $destinationId
    pagination: $pagination
    filters: $filters
  ) {
    data {
      query_id
      cabin_class
      itinerary_id
      total_duration
      total_emissions
      total_stops
      cost
      inbound_in_policy
      inbound_leg_id
      inbound_departure
      inbound_arrival
      inbound_duration
      inbound_flight_duration
      inbound_stops
      inbound_carrier_name
      inbound_carrier_img
      outbound_in_policy
      outbound_leg_id
      outbound_duration
      outbound_departure
      outbound_arrival
      outbound_flight_duration
      outbound_stops
      outbound_carrier_name
      outbound_carrier_img
      rank_cost
      is_feasible
    }
    page {
      total
      lastPage
      currentPage
    }
  }
}
    `;

/**
 * __useFlightOptionListQuery__
 *
 * To run a query within a Vue component, call `useFlightOptionListQuery` and pass it any options that fit your needs.
 * When your component renders, `useFlightOptionListQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useFlightOptionListQuery({
 *   meetingId: // value for 'meetingId'
 *   originId: // value for 'originId'
 *   destinationId: // value for 'destinationId'
 *   pagination: // value for 'pagination'
 *   filters: // value for 'filters'
 * });
 */
export function useFlightOptionListQuery(variables: FlightOptionListQueryVariables | VueCompositionApi.Ref<FlightOptionListQueryVariables> | ReactiveFunction<FlightOptionListQueryVariables>, options: VueApolloComposable.UseQueryOptions<FlightOptionListQuery, FlightOptionListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<FlightOptionListQuery, FlightOptionListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<FlightOptionListQuery, FlightOptionListQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<FlightOptionListQuery, FlightOptionListQueryVariables>(FlightOptionListDocument, variables, options);
}
export function useFlightOptionListLazyQuery(variables: FlightOptionListQueryVariables | VueCompositionApi.Ref<FlightOptionListQueryVariables> | ReactiveFunction<FlightOptionListQueryVariables>, options: VueApolloComposable.UseQueryOptions<FlightOptionListQuery, FlightOptionListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<FlightOptionListQuery, FlightOptionListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<FlightOptionListQuery, FlightOptionListQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<FlightOptionListQuery, FlightOptionListQueryVariables>(FlightOptionListDocument, variables, options);
}
export type FlightOptionListQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<FlightOptionListQuery, FlightOptionListQueryVariables>;
export const SetMeetingPolicyDocument = gql`
    mutation SetMeetingPolicy($meetingPolicy: MeetingPolicyInput!, $organizationId: String!) {
  setMeetingPolicy(meetingPolicy: $meetingPolicy, organizationId: $organizationId) {
    meeting_id
    id
    name
  }
}
    `;

/**
 * __useSetMeetingPolicyMutation__
 *
 * To run a mutation, you first call `useSetMeetingPolicyMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useSetMeetingPolicyMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useSetMeetingPolicyMutation({
 *   variables: {
 *     meetingPolicy: // value for 'meetingPolicy'
 *     organizationId: // value for 'organizationId'
 *   },
 * });
 */
export function useSetMeetingPolicyMutation(options: VueApolloComposable.UseMutationOptions<SetMeetingPolicyMutation, SetMeetingPolicyMutationVariables> | ReactiveFunction<VueApolloComposable.UseMutationOptions<SetMeetingPolicyMutation, SetMeetingPolicyMutationVariables>>) {
  return VueApolloComposable.useMutation<SetMeetingPolicyMutation, SetMeetingPolicyMutationVariables>(SetMeetingPolicyDocument, options);
}
export type SetMeetingPolicyMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<SetMeetingPolicyMutation, SetMeetingPolicyMutationVariables>;
export const SetTravelPolicyDocument = gql`
    mutation SetTravelPolicy($organizationId: String!, $travelPolicy: TravelPolicyInput!) {
  setTravelPolicy(organizationId: $organizationId, travelPolicy: $travelPolicy) {
    flight_allowed_stops_more_than_min
    flight_cabin_class
    flight_low_cost_airlines
    flight_max_leg_segment_duration
    flight_max_leg_segment_duration_business
    flight_max_leg_segment_duration_check
    flight_max_leg_segment_duration_economy
    flight_max_leg_segment_duration_first_class
    flight_max_leg_segment_duration_premium_economy
    flight_max_stops
    flight_max_total_flight_leg_duration
    flight_max_travel_duration
    flight_avoid_carriers
    flight_preferred_carriers
    flight_providers
    hotel_allow_only_green
    hotel_allow_only_preferred
    hotel_allow_room_sharing
    hotel_apply_cap
    hotel_minimum_rating
    hotel_number_to_suggest
    hotel_optimization
    hotel_star_rating
    hotel_residents_require_accommodation
    hotel_suggest_radius
    id
    name
    status
    organization_id
    isDefault
    transport_consider_alternative_max_distance
    transport_consider_flying_min_distance
    transport_modes
    flight_max_leg_segment_duration_classes
  }
}
    `;

/**
 * __useSetTravelPolicyMutation__
 *
 * To run a mutation, you first call `useSetTravelPolicyMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useSetTravelPolicyMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useSetTravelPolicyMutation({
 *   variables: {
 *     organizationId: // value for 'organizationId'
 *     travelPolicy: // value for 'travelPolicy'
 *   },
 * });
 */
export function useSetTravelPolicyMutation(options: VueApolloComposable.UseMutationOptions<SetTravelPolicyMutation, SetTravelPolicyMutationVariables> | ReactiveFunction<VueApolloComposable.UseMutationOptions<SetTravelPolicyMutation, SetTravelPolicyMutationVariables>>) {
  return VueApolloComposable.useMutation<SetTravelPolicyMutation, SetTravelPolicyMutationVariables>(SetTravelPolicyDocument, options);
}
export type SetTravelPolicyMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<SetTravelPolicyMutation, SetTravelPolicyMutationVariables>;
export const CarrierDataDocument = gql`
    query CarrierData {
  carrierData {
    result {
      id
      name
      carriers {
        id
        name
        url
      }
    }
  }
}
    `;

/**
 * __useCarrierDataQuery__
 *
 * To run a query within a Vue component, call `useCarrierDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useCarrierDataQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useCarrierDataQuery();
 */
export function useCarrierDataQuery(options: VueApolloComposable.UseQueryOptions<CarrierDataQuery, CarrierDataQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<CarrierDataQuery, CarrierDataQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<CarrierDataQuery, CarrierDataQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<CarrierDataQuery, CarrierDataQueryVariables>(CarrierDataDocument, {}, options);
}
export function useCarrierDataLazyQuery(options: VueApolloComposable.UseQueryOptions<CarrierDataQuery, CarrierDataQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<CarrierDataQuery, CarrierDataQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<CarrierDataQuery, CarrierDataQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<CarrierDataQuery, CarrierDataQueryVariables>(CarrierDataDocument, {}, options);
}
export type CarrierDataQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<CarrierDataQuery, CarrierDataQueryVariables>;
export const MeetingPolicyDocument = gql`
    query meetingPolicy($meetingId: String!, $meetingPolicyId: String!) {
  meetingPolicy(meetingId: $meetingId, meetingPolicyId: $meetingPolicyId) {
    flight_allowed_stops_more_than_min
    flight_avoid_carriers
    flight_cabin_class
    flight_low_cost_airlines
    flight_max_leg_segment_duration
    flight_max_leg_segment_duration_business
    flight_max_leg_segment_duration_check
    flight_max_leg_segment_duration_economy
    flight_max_leg_segment_duration_first_class
    flight_max_leg_segment_duration_premium_economy
    flight_max_stops
    flight_max_total_flight_leg_duration
    flight_max_travel_duration
    flight_preferred_carriers
    flight_providers
    hotel_allow_only_green
    hotel_allow_only_preferred
    hotel_allow_room_sharing
    hotel_apply_cap
    hotel_minimum_rating
    hotel_number_to_suggest
    hotel_optimization
    hotel_star_rating
    hotel_residents_require_accommodation
    hotel_suggest_radius
    id
    meeting_id
    name
    transport_consider_alternative_max_distance
    transport_consider_flying_min_distance
    transport_modes
    travel_policy_id
    flight_max_leg_segment_duration_classes
  }
}
    `;

/**
 * __useMeetingPolicyQuery__
 *
 * To run a query within a Vue component, call `useMeetingPolicyQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeetingPolicyQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useMeetingPolicyQuery({
 *   meetingId: // value for 'meetingId'
 *   meetingPolicyId: // value for 'meetingPolicyId'
 * });
 */
export function useMeetingPolicyQuery(variables: MeetingPolicyQueryVariables | VueCompositionApi.Ref<MeetingPolicyQueryVariables> | ReactiveFunction<MeetingPolicyQueryVariables>, options: VueApolloComposable.UseQueryOptions<MeetingPolicyQuery, MeetingPolicyQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<MeetingPolicyQuery, MeetingPolicyQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<MeetingPolicyQuery, MeetingPolicyQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<MeetingPolicyQuery, MeetingPolicyQueryVariables>(MeetingPolicyDocument, variables, options);
}
export function useMeetingPolicyLazyQuery(variables: MeetingPolicyQueryVariables | VueCompositionApi.Ref<MeetingPolicyQueryVariables> | ReactiveFunction<MeetingPolicyQueryVariables>, options: VueApolloComposable.UseQueryOptions<MeetingPolicyQuery, MeetingPolicyQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<MeetingPolicyQuery, MeetingPolicyQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<MeetingPolicyQuery, MeetingPolicyQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<MeetingPolicyQuery, MeetingPolicyQueryVariables>(MeetingPolicyDocument, variables, options);
}
export type MeetingPolicyQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<MeetingPolicyQuery, MeetingPolicyQueryVariables>;
export const TravelPolicyDocument = gql`
    query TravelPolicy($organizationId: String!, $travelPolicyId: String!) {
  travelPolicy(organizationId: $organizationId, travelPolicyId: $travelPolicyId) {
    flight_allowed_stops_more_than_min
    flight_avoid_carriers
    flight_cabin_class
    flight_low_cost_airlines
    flight_max_leg_segment_duration
    flight_max_leg_segment_duration_business
    flight_max_leg_segment_duration_check
    flight_max_leg_segment_duration_economy
    flight_max_leg_segment_duration_first_class
    flight_max_leg_segment_duration_premium_economy
    flight_max_stops
    flight_max_total_flight_leg_duration
    flight_max_travel_duration
    flight_preferred_carriers
    flight_providers
    hotel_allow_only_green
    hotel_allow_only_preferred
    hotel_allow_room_sharing
    hotel_apply_cap
    hotel_minimum_rating
    hotel_number_to_suggest
    hotel_optimization
    hotel_residents_require_accommodation
    hotel_star_rating
    hotel_suggest_radius
    id
    name
    organization_id
    isDefault
    transport_consider_alternative_max_distance
    transport_consider_flying_min_distance
    transport_modes
    flight_max_leg_segment_duration_classes
  }
}
    `;

/**
 * __useTravelPolicyQuery__
 *
 * To run a query within a Vue component, call `useTravelPolicyQuery` and pass it any options that fit your needs.
 * When your component renders, `useTravelPolicyQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useTravelPolicyQuery({
 *   organizationId: // value for 'organizationId'
 *   travelPolicyId: // value for 'travelPolicyId'
 * });
 */
export function useTravelPolicyQuery(variables: TravelPolicyQueryVariables | VueCompositionApi.Ref<TravelPolicyQueryVariables> | ReactiveFunction<TravelPolicyQueryVariables>, options: VueApolloComposable.UseQueryOptions<TravelPolicyQuery, TravelPolicyQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<TravelPolicyQuery, TravelPolicyQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<TravelPolicyQuery, TravelPolicyQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<TravelPolicyQuery, TravelPolicyQueryVariables>(TravelPolicyDocument, variables, options);
}
export function useTravelPolicyLazyQuery(variables: TravelPolicyQueryVariables | VueCompositionApi.Ref<TravelPolicyQueryVariables> | ReactiveFunction<TravelPolicyQueryVariables>, options: VueApolloComposable.UseQueryOptions<TravelPolicyQuery, TravelPolicyQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<TravelPolicyQuery, TravelPolicyQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<TravelPolicyQuery, TravelPolicyQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<TravelPolicyQuery, TravelPolicyQueryVariables>(TravelPolicyDocument, variables, options);
}
export type TravelPolicyQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<TravelPolicyQuery, TravelPolicyQueryVariables>;
export const TravelPolicyListDocument = gql`
    query TravelPolicyList($organizationId: String!) {
  travelPolicyList(organizationId: $organizationId) {
    flight_allowed_stops_more_than_min
    flight_avoid_carriers
    flight_cabin_class
    flight_low_cost_airlines
    flight_max_leg_segment_duration
    flight_max_leg_segment_duration_business
    flight_max_leg_segment_duration_check
    flight_max_leg_segment_duration_economy
    flight_max_leg_segment_duration_first_class
    flight_max_leg_segment_duration_premium_economy
    flight_max_stops
    flight_max_total_flight_leg_duration
    flight_max_travel_duration
    flight_preferred_carriers
    flight_providers
    hotel_allow_only_green
    hotel_allow_only_preferred
    hotel_allow_room_sharing
    hotel_apply_cap
    hotel_minimum_rating
    hotel_number_to_suggest
    hotel_optimization
    hotel_residents_require_accommodation
    hotel_star_rating
    hotel_suggest_radius
    id
    name
    organization_id
    isDefault
    transport_consider_alternative_max_distance
    transport_consider_flying_min_distance
    transport_modes
    flight_max_leg_segment_duration_classes
  }
}
    `;

/**
 * __useTravelPolicyListQuery__
 *
 * To run a query within a Vue component, call `useTravelPolicyListQuery` and pass it any options that fit your needs.
 * When your component renders, `useTravelPolicyListQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useTravelPolicyListQuery({
 *   organizationId: // value for 'organizationId'
 * });
 */
export function useTravelPolicyListQuery(variables: TravelPolicyListQueryVariables | VueCompositionApi.Ref<TravelPolicyListQueryVariables> | ReactiveFunction<TravelPolicyListQueryVariables>, options: VueApolloComposable.UseQueryOptions<TravelPolicyListQuery, TravelPolicyListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<TravelPolicyListQuery, TravelPolicyListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<TravelPolicyListQuery, TravelPolicyListQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<TravelPolicyListQuery, TravelPolicyListQueryVariables>(TravelPolicyListDocument, variables, options);
}
export function useTravelPolicyListLazyQuery(variables: TravelPolicyListQueryVariables | VueCompositionApi.Ref<TravelPolicyListQueryVariables> | ReactiveFunction<TravelPolicyListQueryVariables>, options: VueApolloComposable.UseQueryOptions<TravelPolicyListQuery, TravelPolicyListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<TravelPolicyListQuery, TravelPolicyListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<TravelPolicyListQuery, TravelPolicyListQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<TravelPolicyListQuery, TravelPolicyListQueryVariables>(TravelPolicyListDocument, variables, options);
}
export type TravelPolicyListQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<TravelPolicyListQuery, TravelPolicyListQueryVariables>;
export const AuthSamlDocument = gql`
    mutation AuthSAML($user: AuthSamlForm!) {
  authSaml(user: $user) {
    refreshToken
    accessToken
    data {
      ...UserBasicData
    }
  }
}
    ${UserBasicDataFragmentDoc}`;

/**
 * __useAuthSamlMutation__
 *
 * To run a mutation, you first call `useAuthSamlMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useAuthSamlMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useAuthSamlMutation({
 *   variables: {
 *     user: // value for 'user'
 *   },
 * });
 */
export function useAuthSamlMutation(options: VueApolloComposable.UseMutationOptions<AuthSamlMutation, AuthSamlMutationVariables> | ReactiveFunction<VueApolloComposable.UseMutationOptions<AuthSamlMutation, AuthSamlMutationVariables>>) {
  return VueApolloComposable.useMutation<AuthSamlMutation, AuthSamlMutationVariables>(AuthSamlDocument, options);
}
export type AuthSamlMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<AuthSamlMutation, AuthSamlMutationVariables>;
export const AuthUserDocument = gql`
    mutation AuthUser($user: AuthForm!) {
  authUser(user: $user) {
    data {
      ...UserBasicData
    }
    refreshToken
    accessToken
  }
}
    ${UserBasicDataFragmentDoc}`;

/**
 * __useAuthUserMutation__
 *
 * To run a mutation, you first call `useAuthUserMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useAuthUserMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useAuthUserMutation({
 *   variables: {
 *     user: // value for 'user'
 *   },
 * });
 */
export function useAuthUserMutation(options: VueApolloComposable.UseMutationOptions<AuthUserMutation, AuthUserMutationVariables> | ReactiveFunction<VueApolloComposable.UseMutationOptions<AuthUserMutation, AuthUserMutationVariables>>) {
  return VueApolloComposable.useMutation<AuthUserMutation, AuthUserMutationVariables>(AuthUserDocument, options);
}
export type AuthUserMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<AuthUserMutation, AuthUserMutationVariables>;
export const ConfirmResetPasswordDocument = gql`
    mutation confirmResetPassword($form: ResetPasswordForm!) {
  confirmResetPassword(form: $form)
}
    `;

/**
 * __useConfirmResetPasswordMutation__
 *
 * To run a mutation, you first call `useConfirmResetPasswordMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useConfirmResetPasswordMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useConfirmResetPasswordMutation({
 *   variables: {
 *     form: // value for 'form'
 *   },
 * });
 */
export function useConfirmResetPasswordMutation(options: VueApolloComposable.UseMutationOptions<ConfirmResetPasswordMutation, ConfirmResetPasswordMutationVariables> | ReactiveFunction<VueApolloComposable.UseMutationOptions<ConfirmResetPasswordMutation, ConfirmResetPasswordMutationVariables>>) {
  return VueApolloComposable.useMutation<ConfirmResetPasswordMutation, ConfirmResetPasswordMutationVariables>(ConfirmResetPasswordDocument, options);
}
export type ConfirmResetPasswordMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<ConfirmResetPasswordMutation, ConfirmResetPasswordMutationVariables>;
export const ConfirmVerifyUserDocument = gql`
    mutation ConfirmVerifyUser($form: UserVerifyEmailForm!) {
  confirmVerifyUser(form: $form)
}
    `;

/**
 * __useConfirmVerifyUserMutation__
 *
 * To run a mutation, you first call `useConfirmVerifyUserMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useConfirmVerifyUserMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useConfirmVerifyUserMutation({
 *   variables: {
 *     form: // value for 'form'
 *   },
 * });
 */
export function useConfirmVerifyUserMutation(options: VueApolloComposable.UseMutationOptions<ConfirmVerifyUserMutation, ConfirmVerifyUserMutationVariables> | ReactiveFunction<VueApolloComposable.UseMutationOptions<ConfirmVerifyUserMutation, ConfirmVerifyUserMutationVariables>>) {
  return VueApolloComposable.useMutation<ConfirmVerifyUserMutation, ConfirmVerifyUserMutationVariables>(ConfirmVerifyUserDocument, options);
}
export type ConfirmVerifyUserMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<ConfirmVerifyUserMutation, ConfirmVerifyUserMutationVariables>;
export const ResetPasswordDocument = gql`
    mutation ResetPassword($user: UserEmailForm!) {
  resetPassword(user: $user)
}
    `;

/**
 * __useResetPasswordMutation__
 *
 * To run a mutation, you first call `useResetPasswordMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useResetPasswordMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useResetPasswordMutation({
 *   variables: {
 *     user: // value for 'user'
 *   },
 * });
 */
export function useResetPasswordMutation(options: VueApolloComposable.UseMutationOptions<ResetPasswordMutation, ResetPasswordMutationVariables> | ReactiveFunction<VueApolloComposable.UseMutationOptions<ResetPasswordMutation, ResetPasswordMutationVariables>>) {
  return VueApolloComposable.useMutation<ResetPasswordMutation, ResetPasswordMutationVariables>(ResetPasswordDocument, options);
}
export type ResetPasswordMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<ResetPasswordMutation, ResetPasswordMutationVariables>;
export const VerifyUserDocument = gql`
    mutation VerifyUser($user: UserEmailForm!) {
  verifyUser(user: $user)
}
    `;

/**
 * __useVerifyUserMutation__
 *
 * To run a mutation, you first call `useVerifyUserMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useVerifyUserMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useVerifyUserMutation({
 *   variables: {
 *     user: // value for 'user'
 *   },
 * });
 */
export function useVerifyUserMutation(options: VueApolloComposable.UseMutationOptions<VerifyUserMutation, VerifyUserMutationVariables> | ReactiveFunction<VueApolloComposable.UseMutationOptions<VerifyUserMutation, VerifyUserMutationVariables>>) {
  return VueApolloComposable.useMutation<VerifyUserMutation, VerifyUserMutationVariables>(VerifyUserDocument, options);
}
export type VerifyUserMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<VerifyUserMutation, VerifyUserMutationVariables>;
export const IsUserDocument = gql`
    query IsUser($email: String!) {
  isUser(email: $email) {
    provider
    exist
    organizations {
      ...OrganizationBasic
    }
    emailVerified
  }
}
    ${OrganizationBasicFragmentDoc}`;

/**
 * __useIsUserQuery__
 *
 * To run a query within a Vue component, call `useIsUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useIsUserQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useIsUserQuery({
 *   email: // value for 'email'
 * });
 */
export function useIsUserQuery(variables: IsUserQueryVariables | VueCompositionApi.Ref<IsUserQueryVariables> | ReactiveFunction<IsUserQueryVariables>, options: VueApolloComposable.UseQueryOptions<IsUserQuery, IsUserQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<IsUserQuery, IsUserQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<IsUserQuery, IsUserQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<IsUserQuery, IsUserQueryVariables>(IsUserDocument, variables, options);
}
export function useIsUserLazyQuery(variables: IsUserQueryVariables | VueCompositionApi.Ref<IsUserQueryVariables> | ReactiveFunction<IsUserQueryVariables>, options: VueApolloComposable.UseQueryOptions<IsUserQuery, IsUserQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<IsUserQuery, IsUserQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<IsUserQuery, IsUserQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<IsUserQuery, IsUserQueryVariables>(IsUserDocument, variables, options);
}
export type IsUserQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<IsUserQuery, IsUserQueryVariables>;
export const OrganizationSsoAuthDocument = gql`
    query OrganizationSsoAuth($code: SsoCode!) {
  organizationSsoAuth(code: $code) {
    name
    signin_types
    saml_provider
    theme_variables
    assets {
      id
      logos {
        url
        filename
        mimetype
        type
      }
    }
  }
}
    `;

/**
 * __useOrganizationSsoAuthQuery__
 *
 * To run a query within a Vue component, call `useOrganizationSsoAuthQuery` and pass it any options that fit your needs.
 * When your component renders, `useOrganizationSsoAuthQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useOrganizationSsoAuthQuery({
 *   code: // value for 'code'
 * });
 */
export function useOrganizationSsoAuthQuery(variables: OrganizationSsoAuthQueryVariables | VueCompositionApi.Ref<OrganizationSsoAuthQueryVariables> | ReactiveFunction<OrganizationSsoAuthQueryVariables>, options: VueApolloComposable.UseQueryOptions<OrganizationSsoAuthQuery, OrganizationSsoAuthQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<OrganizationSsoAuthQuery, OrganizationSsoAuthQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<OrganizationSsoAuthQuery, OrganizationSsoAuthQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<OrganizationSsoAuthQuery, OrganizationSsoAuthQueryVariables>(OrganizationSsoAuthDocument, variables, options);
}
export function useOrganizationSsoAuthLazyQuery(variables: OrganizationSsoAuthQueryVariables | VueCompositionApi.Ref<OrganizationSsoAuthQueryVariables> | ReactiveFunction<OrganizationSsoAuthQueryVariables>, options: VueApolloComposable.UseQueryOptions<OrganizationSsoAuthQuery, OrganizationSsoAuthQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<OrganizationSsoAuthQuery, OrganizationSsoAuthQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<OrganizationSsoAuthQuery, OrganizationSsoAuthQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<OrganizationSsoAuthQuery, OrganizationSsoAuthQueryVariables>(OrganizationSsoAuthDocument, variables, options);
}
export type OrganizationSsoAuthQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<OrganizationSsoAuthQuery, OrganizationSsoAuthQueryVariables>;
export const RecycleTokenDocument = gql`
    query RecycleToken($refreshToken: String!) {
  recycleToken(refreshToken: $refreshToken) {
    accessToken
    refreshToken
    data {
      ...UserBasicData
    }
  }
}
    ${UserBasicDataFragmentDoc}`;

/**
 * __useRecycleTokenQuery__
 *
 * To run a query within a Vue component, call `useRecycleTokenQuery` and pass it any options that fit your needs.
 * When your component renders, `useRecycleTokenQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useRecycleTokenQuery({
 *   refreshToken: // value for 'refreshToken'
 * });
 */
export function useRecycleTokenQuery(variables: RecycleTokenQueryVariables | VueCompositionApi.Ref<RecycleTokenQueryVariables> | ReactiveFunction<RecycleTokenQueryVariables>, options: VueApolloComposable.UseQueryOptions<RecycleTokenQuery, RecycleTokenQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<RecycleTokenQuery, RecycleTokenQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<RecycleTokenQuery, RecycleTokenQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<RecycleTokenQuery, RecycleTokenQueryVariables>(RecycleTokenDocument, variables, options);
}
export function useRecycleTokenLazyQuery(variables: RecycleTokenQueryVariables | VueCompositionApi.Ref<RecycleTokenQueryVariables> | ReactiveFunction<RecycleTokenQueryVariables>, options: VueApolloComposable.UseQueryOptions<RecycleTokenQuery, RecycleTokenQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<RecycleTokenQuery, RecycleTokenQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<RecycleTokenQuery, RecycleTokenQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<RecycleTokenQuery, RecycleTokenQueryVariables>(RecycleTokenDocument, variables, options);
}
export type RecycleTokenQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<RecycleTokenQuery, RecycleTokenQueryVariables>;
export const RevokeTokenDocument = gql`
    query RevokeToken($userId: String!) {
  revokeToken(userId: $userId)
}
    `;

/**
 * __useRevokeTokenQuery__
 *
 * To run a query within a Vue component, call `useRevokeTokenQuery` and pass it any options that fit your needs.
 * When your component renders, `useRevokeTokenQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useRevokeTokenQuery({
 *   userId: // value for 'userId'
 * });
 */
export function useRevokeTokenQuery(variables: RevokeTokenQueryVariables | VueCompositionApi.Ref<RevokeTokenQueryVariables> | ReactiveFunction<RevokeTokenQueryVariables>, options: VueApolloComposable.UseQueryOptions<RevokeTokenQuery, RevokeTokenQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<RevokeTokenQuery, RevokeTokenQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<RevokeTokenQuery, RevokeTokenQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<RevokeTokenQuery, RevokeTokenQueryVariables>(RevokeTokenDocument, variables, options);
}
export function useRevokeTokenLazyQuery(variables: RevokeTokenQueryVariables | VueCompositionApi.Ref<RevokeTokenQueryVariables> | ReactiveFunction<RevokeTokenQueryVariables>, options: VueApolloComposable.UseQueryOptions<RevokeTokenQuery, RevokeTokenQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<RevokeTokenQuery, RevokeTokenQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<RevokeTokenQuery, RevokeTokenQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<RevokeTokenQuery, RevokeTokenQueryVariables>(RevokeTokenDocument, variables, options);
}
export type RevokeTokenQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<RevokeTokenQuery, RevokeTokenQueryVariables>;
export const SwitchOrganizationDocument = gql`
    query SwitchOrganization($organizationId: String!, $isDefault: Boolean) {
  switchOrganization(organizationId: $organizationId, isDefault: $isDefault) {
    accessToken
    refreshToken
  }
}
    `;

/**
 * __useSwitchOrganizationQuery__
 *
 * To run a query within a Vue component, call `useSwitchOrganizationQuery` and pass it any options that fit your needs.
 * When your component renders, `useSwitchOrganizationQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useSwitchOrganizationQuery({
 *   organizationId: // value for 'organizationId'
 *   isDefault: // value for 'isDefault'
 * });
 */
export function useSwitchOrganizationQuery(variables: SwitchOrganizationQueryVariables | VueCompositionApi.Ref<SwitchOrganizationQueryVariables> | ReactiveFunction<SwitchOrganizationQueryVariables>, options: VueApolloComposable.UseQueryOptions<SwitchOrganizationQuery, SwitchOrganizationQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<SwitchOrganizationQuery, SwitchOrganizationQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<SwitchOrganizationQuery, SwitchOrganizationQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<SwitchOrganizationQuery, SwitchOrganizationQueryVariables>(SwitchOrganizationDocument, variables, options);
}
export function useSwitchOrganizationLazyQuery(variables: SwitchOrganizationQueryVariables | VueCompositionApi.Ref<SwitchOrganizationQueryVariables> | ReactiveFunction<SwitchOrganizationQueryVariables>, options: VueApolloComposable.UseQueryOptions<SwitchOrganizationQuery, SwitchOrganizationQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<SwitchOrganizationQuery, SwitchOrganizationQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<SwitchOrganizationQuery, SwitchOrganizationQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<SwitchOrganizationQuery, SwitchOrganizationQueryVariables>(SwitchOrganizationDocument, variables, options);
}
export type SwitchOrganizationQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<SwitchOrganizationQuery, SwitchOrganizationQueryVariables>;
export const UserBasicDocument = gql`
    query UserBasic($userId: String!) {
  user(userId: $userId) {
    data {
      ...UserBasicData
    }
  }
}
    ${UserBasicDataFragmentDoc}`;

/**
 * __useUserBasicQuery__
 *
 * To run a query within a Vue component, call `useUserBasicQuery` and pass it any options that fit your needs.
 * When your component renders, `useUserBasicQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param variables that will be passed into the query
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useUserBasicQuery({
 *   userId: // value for 'userId'
 * });
 */
export function useUserBasicQuery(variables: UserBasicQueryVariables | VueCompositionApi.Ref<UserBasicQueryVariables> | ReactiveFunction<UserBasicQueryVariables>, options: VueApolloComposable.UseQueryOptions<UserBasicQuery, UserBasicQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<UserBasicQuery, UserBasicQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<UserBasicQuery, UserBasicQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<UserBasicQuery, UserBasicQueryVariables>(UserBasicDocument, variables, options);
}
export function useUserBasicLazyQuery(variables: UserBasicQueryVariables | VueCompositionApi.Ref<UserBasicQueryVariables> | ReactiveFunction<UserBasicQueryVariables>, options: VueApolloComposable.UseQueryOptions<UserBasicQuery, UserBasicQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<UserBasicQuery, UserBasicQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<UserBasicQuery, UserBasicQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<UserBasicQuery, UserBasicQueryVariables>(UserBasicDocument, variables, options);
}
export type UserBasicQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<UserBasicQuery, UserBasicQueryVariables>;
export const UserListDocument = gql`
    query UserList {
  userList {
    created
    designation
    email
    fb_payload
    first_name
    id
    last_name
    organizations {
      user_email_domains
      user_email_tld
      theme_variables
      signin_types
      response_property
      name
      id
      global_hotel_cap
      banned_countries
      app_custom_domain_address
    }
    updated
  }
}
    `;

/**
 * __useUserListQuery__
 *
 * To run a query within a Vue component, call `useUserListQuery` and pass it any options that fit your needs.
 * When your component renders, `useUserListQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useUserListQuery();
 */
export function useUserListQuery(options: VueApolloComposable.UseQueryOptions<UserListQuery, UserListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<UserListQuery, UserListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<UserListQuery, UserListQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<UserListQuery, UserListQueryVariables>(UserListDocument, {}, options);
}
export function useUserListLazyQuery(options: VueApolloComposable.UseQueryOptions<UserListQuery, UserListQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<UserListQuery, UserListQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<UserListQuery, UserListQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<UserListQuery, UserListQueryVariables>(UserListDocument, {}, options);
}
export type UserListQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<UserListQuery, UserListQueryVariables>;