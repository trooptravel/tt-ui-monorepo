import gql from 'graphql-tag';
import * as VueApolloComposable from '@vue/apollo-composable';
import * as VueCompositionApi from 'vue';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type ReactiveFunction<TParam> = () => TParam;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSON: any;
  /** The `JSONObject` scalar type represents JSON objects as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSONObject: any;
};

export type I18N = {
  __typename?: 'I18N';
  content?: Maybe<Scalars['JSONObject']>;
  createdBy?: Maybe<Scalars['String']>;
  createdDate?: Maybe<Scalars['Int']>;
  data?: Maybe<I18NData>;
  endDate?: Maybe<Scalars['Int']>;
  everything?: Maybe<Scalars['JSONObject']>;
  id?: Maybe<Scalars['ID']>;
  lastUpdated?: Maybe<Scalars['Int']>;
  lastUpdatedBy?: Maybe<Scalars['String']>;
  modelId?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['ID']>;
  ownerId?: Maybe<Scalars['ID']>;
  published?: Maybe<Scalars['String']>;
  query?: Maybe<Array<Maybe<Query>>>;
  screenshot?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['Int']>;
  testRatio?: Maybe<Scalars['Int']>;
  variations?: Maybe<Scalars['JSONObject']>;
};

export type I18NData = {
  __typename?: 'I18NData';
  _none?: Maybe<Scalars['String']>;
};

export type Page = {
  __typename?: 'Page';
  content?: Maybe<Scalars['JSONObject']>;
  createdBy?: Maybe<Scalars['String']>;
  createdDate?: Maybe<Scalars['Int']>;
  data?: Maybe<PageData>;
  endDate?: Maybe<Scalars['Int']>;
  everything?: Maybe<Scalars['JSONObject']>;
  id?: Maybe<Scalars['ID']>;
  lastUpdated?: Maybe<Scalars['Int']>;
  lastUpdatedBy?: Maybe<Scalars['String']>;
  modelId?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['ID']>;
  ownerId?: Maybe<Scalars['ID']>;
  published?: Maybe<Scalars['String']>;
  query?: Maybe<Array<Maybe<Query>>>;
  screenshot?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['Int']>;
  testRatio?: Maybe<Scalars['Int']>;
  variations?: Maybe<Scalars['JSONObject']>;
};

export type PageData = {
  __typename?: 'PageData';
  blocks?: Maybe<Scalars['JSON']>;
  css?: Maybe<Scalars['String']>;
  cssCode?: Maybe<Scalars['String']>;
  customFonts?: Maybe<Scalars['JSON']>;
  /** SEO page description */
  description?: Maybe<Scalars['String']>;
  html?: Maybe<Scalars['String']>;
  httpRequests?: Maybe<Scalars['JSON']>;
  inputs?: Maybe<Scalars['JSON']>;
  jsCode?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['JSON']>;
  /** SEO page title */
  title?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
};

export type PolicyItem = {
  __typename?: 'PolicyItem';
  content?: Maybe<Scalars['JSONObject']>;
  createdBy?: Maybe<Scalars['String']>;
  createdDate?: Maybe<Scalars['Int']>;
  data?: Maybe<PolicyItemData>;
  endDate?: Maybe<Scalars['Int']>;
  everything?: Maybe<Scalars['JSONObject']>;
  id?: Maybe<Scalars['ID']>;
  lastUpdated?: Maybe<Scalars['Int']>;
  lastUpdatedBy?: Maybe<Scalars['String']>;
  modelId?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['ID']>;
  ownerId?: Maybe<Scalars['ID']>;
  published?: Maybe<Scalars['String']>;
  query?: Maybe<Array<Maybe<Query>>>;
  screenshot?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['Int']>;
  testRatio?: Maybe<Scalars['Int']>;
  variations?: Maybe<Scalars['JSONObject']>;
};

export type PolicyItemData = {
  __typename?: 'PolicyItemData';
  blocks?: Maybe<Scalars['JSON']>;
  css?: Maybe<Scalars['String']>;
  cssCode?: Maybe<Scalars['String']>;
  customFonts?: Maybe<Scalars['JSON']>;
  details?: Maybe<Scalars['String']>;
  heading?: Maybe<Scalars['String']>;
  html?: Maybe<Scalars['String']>;
  httpRequests?: Maybe<Scalars['JSON']>;
  inputs?: Maybe<Scalars['JSON']>;
  jsCode?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['JSON']>;
  url?: Maybe<Scalars['String']>;
};

export type Query = {
  __typename?: 'Query';
  operator?: Maybe<Scalars['String']>;
  property?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['JSON']>;
};

export type RootQueryType = {
  __typename?: 'RootQueryType';
  /** Fetch i-18-n results */
  i18N?: Maybe<Array<Maybe<I18N>>>;
  /** Fetch just one i-18-n */
  oneI18N?: Maybe<I18N>;
  /** Fetch just one page */
  onePage?: Maybe<Page>;
  /** Fetch just one policy-item */
  onePolicyItem?: Maybe<PolicyItem>;
  /** Fetch just one test */
  oneTest?: Maybe<Test>;
  /** Fetch just one tooltip */
  oneTooltip?: Maybe<Tooltip>;
  /** Fetch page results */
  page?: Maybe<Array<Maybe<Page>>>;
  /** Fetch policy-item results */
  policyItem?: Maybe<Array<Maybe<PolicyItem>>>;
  /** Fetch test results */
  test?: Maybe<Array<Maybe<Test>>>;
  /** Fetch tooltip results */
  tooltip?: Maybe<Array<Maybe<Tooltip>>>;
};


export type RootQueryTypeI18NArgs = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  options?: InputMaybe<Scalars['JSONObject']>;
  query?: InputMaybe<Scalars['JSONObject']>;
  sort?: InputMaybe<Scalars['JSONObject']>;
  target?: InputMaybe<Scalars['JSONObject']>;
};


export type RootQueryTypeOneI18NArgs = {
  offset?: InputMaybe<Scalars['Int']>;
  options?: InputMaybe<Scalars['JSONObject']>;
  query?: InputMaybe<Scalars['JSONObject']>;
  sort?: InputMaybe<Scalars['JSONObject']>;
  target?: InputMaybe<Scalars['JSONObject']>;
};


export type RootQueryTypeOnePageArgs = {
  offset?: InputMaybe<Scalars['Int']>;
  options?: InputMaybe<Scalars['JSONObject']>;
  query?: InputMaybe<Scalars['JSONObject']>;
  sort?: InputMaybe<Scalars['JSONObject']>;
  target?: InputMaybe<Scalars['JSONObject']>;
};


export type RootQueryTypeOnePolicyItemArgs = {
  offset?: InputMaybe<Scalars['Int']>;
  options?: InputMaybe<Scalars['JSONObject']>;
  query?: InputMaybe<Scalars['JSONObject']>;
  sort?: InputMaybe<Scalars['JSONObject']>;
  target?: InputMaybe<Scalars['JSONObject']>;
};


export type RootQueryTypeOneTestArgs = {
  offset?: InputMaybe<Scalars['Int']>;
  options?: InputMaybe<Scalars['JSONObject']>;
  query?: InputMaybe<Scalars['JSONObject']>;
  sort?: InputMaybe<Scalars['JSONObject']>;
  target?: InputMaybe<Scalars['JSONObject']>;
};


export type RootQueryTypeOneTooltipArgs = {
  offset?: InputMaybe<Scalars['Int']>;
  options?: InputMaybe<Scalars['JSONObject']>;
  query?: InputMaybe<Scalars['JSONObject']>;
  sort?: InputMaybe<Scalars['JSONObject']>;
  target?: InputMaybe<Scalars['JSONObject']>;
};


export type RootQueryTypePageArgs = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  options?: InputMaybe<Scalars['JSONObject']>;
  query?: InputMaybe<Scalars['JSONObject']>;
  sort?: InputMaybe<Scalars['JSONObject']>;
  target?: InputMaybe<Scalars['JSONObject']>;
};


export type RootQueryTypePolicyItemArgs = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  options?: InputMaybe<Scalars['JSONObject']>;
  query?: InputMaybe<Scalars['JSONObject']>;
  sort?: InputMaybe<Scalars['JSONObject']>;
  target?: InputMaybe<Scalars['JSONObject']>;
};


export type RootQueryTypeTestArgs = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  options?: InputMaybe<Scalars['JSONObject']>;
  query?: InputMaybe<Scalars['JSONObject']>;
  sort?: InputMaybe<Scalars['JSONObject']>;
  target?: InputMaybe<Scalars['JSONObject']>;
};


export type RootQueryTypeTooltipArgs = {
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  options?: InputMaybe<Scalars['JSONObject']>;
  query?: InputMaybe<Scalars['JSONObject']>;
  sort?: InputMaybe<Scalars['JSONObject']>;
  target?: InputMaybe<Scalars['JSONObject']>;
};

export type Test = {
  __typename?: 'Test';
  content?: Maybe<Scalars['JSONObject']>;
  createdBy?: Maybe<Scalars['String']>;
  createdDate?: Maybe<Scalars['Int']>;
  data?: Maybe<TestData>;
  endDate?: Maybe<Scalars['Int']>;
  everything?: Maybe<Scalars['JSONObject']>;
  id?: Maybe<Scalars['ID']>;
  lastUpdated?: Maybe<Scalars['Int']>;
  lastUpdatedBy?: Maybe<Scalars['String']>;
  modelId?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['ID']>;
  ownerId?: Maybe<Scalars['ID']>;
  published?: Maybe<Scalars['String']>;
  query?: Maybe<Array<Maybe<Query>>>;
  screenshot?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['Int']>;
  testRatio?: Maybe<Scalars['Int']>;
  variations?: Maybe<Scalars['JSONObject']>;
};

export type TestData = {
  __typename?: 'TestData';
  _none?: Maybe<Scalars['String']>;
};

export type Tooltip = {
  __typename?: 'Tooltip';
  content?: Maybe<Scalars['JSONObject']>;
  createdBy?: Maybe<Scalars['String']>;
  createdDate?: Maybe<Scalars['Int']>;
  data?: Maybe<TooltipData>;
  endDate?: Maybe<Scalars['Int']>;
  everything?: Maybe<Scalars['JSONObject']>;
  id?: Maybe<Scalars['ID']>;
  lastUpdated?: Maybe<Scalars['Int']>;
  lastUpdatedBy?: Maybe<Scalars['String']>;
  modelId?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['ID']>;
  ownerId?: Maybe<Scalars['ID']>;
  published?: Maybe<Scalars['String']>;
  query?: Maybe<Array<Maybe<Query>>>;
  screenshot?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['Int']>;
  testRatio?: Maybe<Scalars['Int']>;
  variations?: Maybe<Scalars['JSONObject']>;
};

export type TooltipData = {
  __typename?: 'TooltipData';
  heading?: Maybe<Scalars['String']>;
  text?: Maybe<Scalars['String']>;
};

export type TooltipsQueryVariables = Exact<{ [key: string]: never; }>;


export type TooltipsQuery = { __typename?: 'RootQueryType', tooltip?: Array<{ __typename?: 'Tooltip', name?: string | null, data?: { __typename?: 'TooltipData', heading?: string | null, text?: string | null } | null } | null> | null };


export const TooltipsDocument = gql`
    query Tooltips {
  tooltip(query: {}, limit: 1000) {
    name
    data {
      heading
      text
    }
  }
}
    `;

/**
 * __useTooltipsQuery__
 *
 * To run a query within a Vue component, call `useTooltipsQuery` and pass it any options that fit your needs.
 * When your component renders, `useTooltipsQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useTooltipsQuery();
 */
export function useTooltipsQuery(options: VueApolloComposable.UseQueryOptions<TooltipsQuery, TooltipsQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<TooltipsQuery, TooltipsQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<TooltipsQuery, TooltipsQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<TooltipsQuery, TooltipsQueryVariables>(TooltipsDocument, {}, options);
}
export function useTooltipsLazyQuery(options: VueApolloComposable.UseQueryOptions<TooltipsQuery, TooltipsQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<TooltipsQuery, TooltipsQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<TooltipsQuery, TooltipsQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<TooltipsQuery, TooltipsQueryVariables>(TooltipsDocument, {}, options);
}
export type TooltipsQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<TooltipsQuery, TooltipsQueryVariables>;