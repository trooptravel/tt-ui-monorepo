import { UseQueryReturn, useResult, UseQueryOptions, UseMutationOptions, UseMutationReturn } from "@vue/apollo-composable";

import { ReactiveFunction } from "../generated";
import { computed, isRef, nextTick, ref, Ref, shallowRef } from "vue";

export function usePromisifiedQueryWithoutVariables<Query, QueryVariables>(
  useQueryComposable: (
    queryOptions: ReactiveFunction<UseQueryOptions<Query, QueryVariables>>,
  ) => UseQueryReturn<Query, QueryVariables>,
  queryOptions?: UseQueryOptions<Query, QueryVariables>
) {
  const isEnabled = ref(false);
  const { onResult, onError, result } = useQueryComposable(
    () => ({
      enabled: isEnabled.value,
      ...queryOptions,
    }),
  );
  const data = useResult(result);

  return () => {
    return new Promise<typeof data.value>((resolve, reject) => {
      const offOnResult = onResult((result) => {
        offOnResult.off();
        offOnError.off();
        resolve(data.value);
      });
      const offOnError = onError((error) => {
        offOnResult.off();
        offOnError.off();
        reject(error);
      });
      isEnabled.value = true;
    });
  };
}

export function getPromisifiedQueryWithoutVariables<QueryVariables, QueryReturn, QueryOptions>(
  queryComposable: (queryOptions?: QueryOptions) => UseQueryReturn<QueryReturn, QueryVariables>,
  queryOptions?: QueryOptions,
) {
  return function () {
    const response: UseQueryReturn<QueryReturn, QueryVariables> = queryComposable(queryOptions);
    const { onResult, onError, result, error } = response;
    const data = useResult(result);
    return new Promise<typeof data.value>((resolve, reject) => {
      onResult(() => resolve(data.value));
      onError(() => reject(error));
    });
  };
}

/**
 * This function returns a function that can be called to get a promise that either resolves with the query result or rejects with an error
 * @param useQueryFn Input the composable name generted by codegen
 * @param queryOptions
 * @returns a Promise that resolves with the query result or rejects with an error
 */
export function usePromisifiedQuery<Query, QueryVariables>(
  useQueryFn: (
    variables: QueryVariables | Ref<QueryVariables> | ReactiveFunction<QueryVariables>,
    queryOptions?: UseQueryOptions<Query, QueryVariables> | Ref<UseQueryOptions<Query, QueryVariables>> | ReactiveFunction<UseQueryOptions<Query, QueryVariables>>,
  ) => UseQueryReturn<Query, QueryVariables>,
  queryOptions?: UseQueryOptions<Query, QueryVariables>,
) {
  const params = shallowRef<QueryVariables>();
  const isEnabled = ref(false);

  const { onResult, onError, result } = useQueryFn(
    () => params.value!,
    () => ({
      enabled: isEnabled.value,
      ...queryOptions,
    }),
  );
  const data = useResult(result);

  return (variables?: QueryVariables) => {
    return new Promise<typeof data.value>((resolve, reject) => {
      const offOnResult = onResult((result) => {
        offOnResult.off();
        offOnError.off();
        resolve(data.value);
      });
      const offOnError = onError((error) => {
        offOnResult.off();
        offOnError.off();
        reject(error);
      });
      params.value = variables;
      isEnabled.value = true;
    });
  };
}

export function usePromisifiedMutation<Mutation, MutationVariables>(
  useMutationComposable: (options: UseMutationOptions<Mutation, MutationVariables>) => UseMutationReturn<Mutation, MutationVariables>,
  mutationOptions?: UseMutationOptions<Mutation, MutationVariables>,
) {
  // let params:  MutationVariables
  const { mutate } = useMutationComposable({ ...mutationOptions });

  return (variables: MutationVariables) => mutate(variables);
}
interface UseLazyQueryOptions<TResult, TVariables> extends Omit<UseQueryOptions<TResult, TVariables>, "enabled"> {
  /**
   * Initial values for the variables
   */
  variables: TVariables;
  /**
   * Defaults to being enabled by default
   */
  enabled?: Ref<boolean> | ((variables: TVariables) => boolean);
}

// fallback to consider a query always enabled
const alwaysEnabledFn = () => true;

export function useLazyQuery<TResult, TVariables>(
  useQueryFn: (
    variables: TVariables | Ref<TVariables> | ReactiveFunction<TVariables>,
    queryOptions?: UseQueryOptions<TResult, TVariables> | Ref<UseQueryOptions<TResult, TVariables>> | ReactiveFunction<UseQueryOptions<TResult, TVariables>>,
  ) => UseQueryReturn<TResult, TVariables>,
  options: UseLazyQueryOptions<TResult, TVariables>,
) {
  const isReady = ref(false);

  const enabledFn = typeof options.enabled === "function" ? options.enabled : alwaysEnabledFn;
  // reuse any existing ref to give full control
  const isEnabled = isRef(options.enabled)
    ? options.enabled
    : computed<boolean>(() => {
        // enabled must return false initially or
        if (!isReady.value) return false;
        // variables cannot be undefined because they are required in options
        const variables = query.variables.value!;
        return enabledFn(variables);
      });

  const query = useQueryFn(options.variables, () => ({
    enabled: isEnabled.value,
    // TODO:
    // ...options,
    fetchPolicy: "network-only",
  }));

  // expose a more convenient date
  const data = useResult(query.result);

  const isFinished = () => nextTick(() => query.query.value?.result);

  isReady.value = true;

  return {
    ...query,
    isFinished,
    data,
  };
}
