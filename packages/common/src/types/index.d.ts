import { StateTree } from "pinia";

export interface PersistedStateOptions<State> {
    /**
     * Storage key to use.
     * @default $store.id
     */
    key?: string
    /**
     * Dot-notation paths to partially save state.
     * @default undefined
     */
    paths: Array<keyof State>
}

declare module 'pinia' {
    export interface DefineStoreOptions<Id extends string, S extends StateTree, G, A> {
        /**
         * Persist store in localStorage.
         */
        persistInLocalStorage?: PersistedStateOptions<S>
    }
}
