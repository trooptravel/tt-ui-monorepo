import jwtDecode from "jwt-decode";
import type { AccessTokenPayload, SuccessAuthResponse } from "../models";

const ACCESS_TOKEN_KEY = "accessToken";
const REFRESH_TOKEN_KEY = "refreshToken";
const MS_ACCESS_TOKEN_KEY = 'msAccessToken'

export function useAuthTokenHelpers() {
  function setAuthTokensInLocalStorage(tokenData: Pick<SuccessAuthResponse, "accessToken" | "refreshToken">) {
    const { accessToken, refreshToken } = tokenData;

    setAccessToken(accessToken);
    setRefreshToken(refreshToken);
  }

  function setAccessToken(accessToken: string) {
    localStorage.setItem(ACCESS_TOKEN_KEY, accessToken);
  }

  function setMsAccessToken (msAccessToken: string) {
    localStorage.setItem(MS_ACCESS_TOKEN_KEY, msAccessToken)
  }

  function setRefreshToken(refreshToken: string) {
    localStorage.setItem(REFRESH_TOKEN_KEY, refreshToken);
  }

  function removeAuthTokensFromLocalStorage() {
    localStorage.removeItem(ACCESS_TOKEN_KEY);
    localStorage.removeItem(REFRESH_TOKEN_KEY);
  }

  function getAuthTokens() {
    return {
      accessToken: localStorage.getItem(ACCESS_TOKEN_KEY),
      refreshToken: localStorage.getItem(REFRESH_TOKEN_KEY),
      msAccessToken: localStorage.getItem(MS_ACCESS_TOKEN_KEY),
    };
  }

  function getAccessTokenData() {
    const { accessToken } = getAuthTokens();
    if (!accessToken) return null;

    try {
      return jwtDecode<AccessTokenPayload>(accessToken);
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  function getAccessTokenExpiry() {
    const { accessToken } = getAuthTokens();
    if (!accessToken) return null;

    try {
      const { exp } = jwtDecode<AccessTokenPayload>(accessToken);
      return typeof exp === "number" ? new Date(exp * 1000) : null;
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  return {
    setAuthTokensInLocalStorage,
    setAccessToken,
    setMsAccessToken,
    removeAuthTokensFromLocalStorage,
    getAuthTokens,
    getAccessTokenData,
    getAccessTokenExpiry,
  };
}
