import { defineStore } from "pinia"
// import { Maybe, Organization, User } from "../../graphql/generated";
import { Maybe, Organization, User } from "@graphql"

interface AuthState {
  user: Maybe<User>;
  organizations: Maybe<Organization[]>;
  currentOrganization: Maybe<Organization>;
}

export const useAuthStore = defineStore("auth", {
  state: (): AuthState => {
    return { user: { first_name: 'John', last_name: 'Doe' }, organizations: [], currentOrganization: null };
  },
  actions: {
    setUser(user: User) {
      this.user = user;
    },
    removeUser() {
      this.user = null;
    },
    setOrganizations(organizations: Organization[]) {
      this.organizations = organizations;
    },
    setCurrentOrganization(organization: Organization) {
      this.currentOrganization = organization;
    },
  },
  getters: {
    isAuthenticated: (state) => Boolean(state.user),
    currentUser: (state) => state.user,
    organizationId: (state) => state.currentOrganization?.id,
    passports: (state) => state.user?.travelProfile?.passports || [],
  },
  persistInLocalStorage: {
    paths: ["user"],
  },
});
