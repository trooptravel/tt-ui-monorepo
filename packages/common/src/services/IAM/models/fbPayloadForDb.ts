export interface FBPayloadForDb {
    user: FbUser;
    providerId: string | null;
    operationType: string;
}


interface FbUser {
    uid: string;
    email: string | null;
    emailVerified: boolean;
    isAnonymous: boolean;
    providerData: ProviderData[];
    createdAt?: string;
    lastLoginAt?: string;
    photoURL?: string | null;
    phoneNumber?: string | null;
}

interface ProviderData {
    providerId: string;
    uid: string;
    displayName: string | null;
    email: string | null;
    phoneNumber: string | null;
    photoURL: string | null;
}
