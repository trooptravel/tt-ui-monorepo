import { Maybe } from "../../../graphql/generated";

export interface AccessTokenPayload {
    "https://api.trooptravel.com"?: HTTPSAPITrooptravelCOM;
    iss: string;
    aud: string;
    auth_time: number;
    user_id: string;
    sub: string;
    iat: number;
    exp: Maybe<number>;
    email: string;
    email_verified: boolean;
    firebase?: Firebase;
}

interface Firebase {
    identities: Identities;
    sign_in_provider: string;
}

interface Identities {
    "saml.trooptravel-google"?: string[];
    email: string[];
}

interface HTTPSAPITrooptravelCOM {
    oid: string;
    id: string;
    roles: string[];
    permissions: string[];
}
