import { OrganizationSsoAuthQuery } from '../../../graphql/generated'
import { UserBasicDataFragment, AuthUserMutation, AuthSamlMutation, IsUserQuery, OrganizationSso } from '../../../graphql/generated'

export { FBPayloadForDb } from './fbPayloadForDb'
export { AccessTokenPayload } from './accessTokenPayload'

export type User = UserBasicDataFragment

export interface AuthResponse {
    isSuccess: boolean
    user: User | null,
    errorMessage?: string,
    error?: unknown
}

export interface SuccessResponse extends AuthResponse {
    isSuccess: true
    user: User
    accessToken: string
    refreshToken: string
}
export interface FailiureResponse extends AuthResponse {
    isSuccess: false
    user: null
    errorMessage: string
}


export type UserInAuthResponseFromGraph = AuthUserMutation['authUser'] & AuthSamlMutation['authSaml']
export type SuccessAuthResponse = {
  user: User;
  accessToken: string;
  refreshToken: string;
};

export type IsUserQueryResponse = IsUserQuery['isUser']
export type Organization = NonNullable<User["organizations"]>[0];
export type OrganizationSsoAuthResponse = NonNullable<OrganizationSsoAuthQuery>['organizationSsoAuth']
