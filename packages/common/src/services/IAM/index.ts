import { useAuthStore } from "./store";
import { storeToRefs } from "pinia";

export default function AuthService() {
    const store = useAuthStore()
    const { user, passports, organizations, currentOrganization } = storeToRefs(store)

    return { user, passports, organizations, currentOrganization }
}
