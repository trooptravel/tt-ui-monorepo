// import { isString } from "@/modules/common/utilities";
import { acceptHMRUpdate, defineStore } from "pinia"
import { useLazyQuery } from "@@/graphql/functions"
import { Maybe, useMeetingSpaceListQuery, useMeetingSpaceQuery } from "@@/graphql/generated"
import { computed } from "vue"

export const useMeetingSpaceStore = defineStore("meetingSpaces", () => {
  const {
    data: meetingSpaceList,
    variables: meetingSpaceListVars,
    loading: isMeetingSpaceListLoading,
    isFinished: isMeetingSpaceListQueryFinished,
  } = useLazyQuery(useMeetingSpaceListQuery, {
    variables: { organizationId: "" },
    enabled: (vars) => !!vars.organizationId,
    fetchPolicy: "network-only",
  });

  async function fetchMeetingSpaceList(organizationId: Maybe<string>, placeId: Maybe<string>) {
    // if (!isString(organizationId)) return;
    meetingSpaceListVars.value = { organizationId, placeId, perPage: 100, currentPage: 1 };
    return isMeetingSpaceListQueryFinished();
  }

  const {
    data: meetingSpace,
    variables: meetingSpaceVars,
    loading: isMeetingSpaceLoading,
    isFinished: isMeetingSpaceQueryFinished,
  } = useLazyQuery(useMeetingSpaceQuery, {
    variables: { organizationId: "", meetingSpaceId: "" },
    enabled: (vars) => !!vars.organizationId && !!vars.meetingSpaceId,
    fetchPolicy: "network-only",
  });

  async function fetchMeetingSpace(meetingSpaceId: Maybe<string>, organizationId: Maybe<string>) {
    // NOTE: why is this?
    // if (!isString(organizationId)) return;
    // if (!isString(meetingSpaceId)) return;
    meetingSpaceVars.value = { meetingSpaceId, organizationId };
    return isMeetingSpaceQueryFinished();
  }

  const currentMeetingSpaces = computed(() => meetingSpaceList.value?.data?.find((space) => space?.id === meetingSpace.value?.id));

  return {
    meetingSpaceList,
    isMeetingSpaceListLoading,
    fetchMeetingSpaceList,

    meetingSpace,
    currentMeetingSpaces,
    isMeetingSpaceLoading,
    fetchMeetingSpace,

    // TODO: do we need these
    // clearMeetingSpace,
    // clearMeetingSpaceList,
  };
});

// HMR
if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useMeetingSpaceStore, import.meta.hot));
}
