import { UserModule } from "@/types/custom";
import { createPinia, PiniaPluginContext } from "pinia";

const pinia = createPinia()


/**
 * Pinia plugin to persist stores in LocalStorage.
 *
 * Inspired by:
 * 1. https://github.com/robinvdvleuten/vuex-persistedstate
 * 2. https://github.com/prazdevs/pinia-plugin-persistedstate
 * @param context Options
 */
function persistInLocalStorage (context: PiniaPluginContext): void {
    const storage = localStorage
    const {
        options: { persistInLocalStorage },
        store,
    } = context

    if (!persistInLocalStorage) return

    const {
        key = store.$id + "Store",
        paths = null,
    } = persistInLocalStorage

    try {
        const dataFromLS = storage.getItem(key)
        if (dataFromLS) {
            store.$patch(JSON.parse(dataFromLS))
        }
    } catch (error) {
        // console.error(error)
    }

    store.$subscribe((_mutation: unknown, state: Record<string, unknown>) => {
        try {
            if (!Array.isArray(paths)) return
            const objectToStore = paths.reduce((acc, currentPath) => {
                acc[currentPath] = state[currentPath as unknown as string | number]
                return acc
            }, {} as Record<string | number | symbol, unknown>)

            paths.forEach(path => storage.setItem(key, JSON.stringify(objectToStore)))
        } catch (error) { console.error(error) }
    })
}

export const install: UserModule = ({ app }) => {
  pinia.use(persistInLocalStorage);
  app.use(pinia)
}
