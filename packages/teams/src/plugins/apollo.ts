import { UserModule } from "@/types/custom";
import { ApolloClient, ApolloLink, createHttpLink, InMemoryCache } from "@apollo/client/core";
import { ErrorResponse, onError } from "@apollo/client/link/error";
import { ServerError } from "@apollo/client/link/utils";
import { DefaultApolloClient } from "@vue/apollo-composable";
import { useAuthTokenHelpers } from "@tt-ui-mono/common/src/services/IAM/helpers/authTokenHelpers";

export const install: UserModule = ({ app }) => {
  const httpLink = createHttpLink({
    uri: import.meta.env.VITE_TT_GW_FEDERATION as string
  })

  // Retrieve token from Local-storage and set in header
  const authLink = new ApolloLink((operation, forward) => {
    const { accessToken } = useAuthTokenHelpers().getAuthTokens();
    operation.setContext({
      headers: {
        Authorization: accessToken ? `Bearer ${accessToken}` : "",
      },
    });
    return forward(operation);
  });

  // Log any GraphQL errors or network error that occurred
  const errorLink = onError((errorResponse: ErrorResponse) => {
    const { graphQLErrors, networkError, forward, operation } = errorResponse
    if (graphQLErrors) {
      graphQLErrors.forEach(({ message, locations, path }) => console.error(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`))
    }
    if (networkError) {
      if (networkError.name == 'ServerError') {
        const isTokenRevoked = (networkError as ServerError).result?.error?.extensions?.code === 'TOKEN-REVOKED'
        if (isTokenRevoked) {
          useAuthTokenHelpers().removeAuthTokensFromLocalStorage()
          return forward(operation)
        }
      }
      console.error(`[Network error]: ${networkError}`);
    }
  });

  // Cache implementation
  const cache = new InMemoryCache();

  // Create the apollo client
  const apolloClient = new ApolloClient({
    link: authLink.concat(errorLink.concat(httpLink)),
    name: "tt-ui-teams",
    version: "1.0.0",
    cache,
  });

  app.provide(DefaultApolloClient, apolloClient)
}
