import { UserModule } from "@/types/custom";
import { createWebHistory, createRouter } from "vue-router";
import { setupLayouts } from "virtual:generated-layouts";
import generatedRoutes from "virtual:generated-pages";

const router = createRouter({
  history: createWebHistory(),
  routes: setupLayouts(generatedRoutes)
});

export const install: UserModule = ({ app }) => {
    app.use(router);
}

export default router
