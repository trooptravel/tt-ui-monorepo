import { createApp } from 'vue'
import App from './App.vue'
import router from './plugins/router'

const app = createApp(App)
Object.values(import.meta.globEager('./plugins/*.ts')).map((i) => i.install?.({ app, router }))

app.mount('#app')
