import { App } from "vue";
import { Router } from "vue-router";
import { StateTree } from "pinia";

export type UserModule = (ctx: { app: App; router: Router }) => void

export interface PersistedStateOptions<State> {
    /**
     * Storage key to use.
     * @default $store.id
     */
    key?: string
    /**
     * Dot-notation paths to partially save state.
     * @default undefined
     */
    paths: Array<keyof State>
}

declare module 'pinia' {
    export interface DefineStoreOptions<Id extends string, S extends StateTree, G, A> {
        /**
         * Persist store in localStorage.
         */
        persistInLocalStorage?: PersistedStateOptions<S>
    }
}
