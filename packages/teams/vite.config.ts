import { getApplicationConfiguration } from "../../configuration";
import Pages from "vite-plugin-pages";
import Layouts from "vite-plugin-vue-layouts";
import { UserConfigExport } from "vite";
import { resolve } from "path";

export default getApplicationConfiguration({
  resolve: {
    alias: {
      "@": resolve(__dirname, "src")
    },
    extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', ".web.ts"],
    dedupe: ["vue"],
  },

  plugins: [
    Pages(),
    Layouts(),
  ],
} as UserConfigExport);
