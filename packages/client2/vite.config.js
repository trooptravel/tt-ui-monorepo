import { getApplicationConfiguration } from "../../configuration"
import Pages from "vite-plugin-pages"
import Layouts from "vite-plugin-vue-layouts"
import path from "path"

export default getApplicationConfiguration({
  plugins: [Pages(), Layouts()],
  resolve: {
    alias: [
      { find: "@@", replacement: path.resolve(__dirname, "../common/src") },
      { find: "@graphql", replacement: path.resolve(__dirname, "../common/src/graphql/generated") },
    ],
  },
})
