import { createWebHistory, createRouter } from "vue-router";
import { setupLayouts } from "virtual:generated-layouts";
import generatedRoutes from "virtual:generated-pages";

const routes = setupLayouts(generatedRoutes);

/**
 * Creates UI router with all the UI routes
 */
export default createRouter({
  history: createWebHistory(),
  routes,
});
