import { createApp } from 'vue'
import App from './app.vue'
import Router from './routes'
import { createPinia } from 'pinia'

const pinia = createPinia()

createApp(App)
  .use(Router)
  .use(pinia)
  .mount('#app')
