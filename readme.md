## Install
First install [pnpm](https://pnpm.js.org/) globally
- `npm i -g pnpm`

Then run 
- `pnpm initialize`

After that you should be able to lift any of the applications. You can check `package.json` on the root to see available commands

- `pnpm dev:client` 
- `pnpm build:client`
- `pnpm dev:client2`
- `pnpm build:client2`

## Commands

### Running package specific commands

To run a command from a specific package.json, use pnpm's filter commands

- `pnpm add axios --filter @tt-ui-mono/teams`

Another example would be
- `pnpm run serve --filter @tt-ui-mono/teams`

That will run the `serve` command in the `@tt-ui-mono/teams` package which resides in `packages/client/package.json`

### Running commands on all packages

- `pnpm recursive add axios`
- `pnpm recursive remove axios`



## Root Setup
The following structure is used
```
|- package.json
|- .npmrc
|- pnpm-workspace.yaml
|- packages
    |- client
    |- client2
    |- common
    |- utilities
```